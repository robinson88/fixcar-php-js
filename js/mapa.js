function initMap() {
    var address= document.getElementById('address');
  var marcadores = address;


  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 18,
    center: new google.maps.LatLng( 40.0000000, -4.0000000),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  var infowindow = new google.maps.InfoWindow();
  var marker, i;
  for (i = 0; i < marcadores.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(marcadores[i][1], marcadores[i][2]),
      map: map
    });
    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent(marcadores[i][0]);
        infoWindow.open(map,this)
      }
    })(marker, i));
  }

var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
    this.setZoom(18);
    google.maps.event.removeListener(boundsListener);
    geocodeAddress(geocoder, map);
});
var geocoder = new google.maps.Geocoder();

}
function geocodeAddress(geocoder, resultsMap) {
    var address = document.getElementById('address').value;
    geocoder.geocode({'address': address}, function(results, status) {
      if (status === 'OK') {
        resultsMap.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
          map: resultsMap,
          position: results[0].geometry.location
        });
      } else {
        alert('Geocode  sin exito: ' + status);
      }
    });

  }
