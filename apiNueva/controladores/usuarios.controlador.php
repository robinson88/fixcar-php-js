<?php

class ControladorUsuarios
{
	/*=============================================
	Mostrar todos los registros
	=============================================*/

	public function index()
	{

		$usuarios = ModeloUsuarios::index('usuario');

		$json = array(

			"status" => 200,
			"detalle" => count($usuarios),
			"detalle" => $usuarios

		);

		echo json_encode($json, true);

		return;
	}

	public function show($idusuario)
	{


		/*=============================================
			Mostrar solo un usuario
		=============================================*/

		$usuarios = ModeloUsuarios::show("usuario", $idusuario);
		
		if (!empty($usuarios)) {


			$json = json_encode($usuarios);



			echo ($json);
			//echo ($json);

			return;
		} else {

			$json = array(

				"status" => 200,
				"total_registros" => 0,
				"detalles" => "No hay ningún curso registrado"

			);

			echo json_encode($json, true);

			return;
		}
	}

	/*=============================================
	Crear un registro
	=============================================*/

	public function create($datos)
	{

		$tabla = "usuario";
		/*=============================================
		Llevar datos al modelo
		=============================================*/

		$datos = array(
			'nombre' => $datos['nombre'],
			'password' => $datos['password'],
			'direccion' => $datos['direccion'],
			'localidad' => $datos['localidad'],
			'telefono' => $datos['telefono'],
			'email' => $datos['email'],
			'estado' => $datos['estado'],
			'fecha' => date('Y-m-d h:i:s'),
			'imagen' => $datos['imagen']
		);


		$create = ModeloUsuarios::create($tabla, $datos);

		/*=============================================
		Respuesta del modelo
		=============================================*/

		if ($create == "ok") {

			$json = array(
				"status" => 200,
				"detalle" => "Registro exitoso, tome sus credenciales y guárdelas"

			);

			echo json_encode($json, true);

			return;
		}
	}

	/*=============================================
	Editar un usuario
	=============================================*/

	public function update($idusuario, $datos)
	{
		$usuarios = ModeloUsuarios::show("usuario", $idusuario);

		/*=============================================
			Llevar datos al modelo
		=============================================*/

		$datos = array(
			"id" => $idusuario,
			'nombre' => $datos['nombre'],
			'password' => $datos['password'],
			'direccion' => $datos['direccion'],
			'localidad' => $datos['localidad'],
			'telefono' => $datos['telefono'],
			'email' => $datos['email'],
			'estado' => $datos['estado'],
			'fecha' => date('Y-m-d h:i:s'),
			'imagen' => $datos['imagen']
		);

		$update = ModeloUsuarios::update("usuario", $datos);

		/*=============================================
			Respuesta del modelo
		=============================================*/

		if ($update == "ok") {

			$json = array(
				"status" => 200,
				"detalle" => "Registro exitoso, su curso ha sido actualizado"

			);

			echo json_encode($json, true);

			return;
		}
	}

	/*=============================================
	Borrar curso
	=============================================*/

	public function delete($idusuario)
	{
		$delete = ModeloUsuarios::delete("usuario", $idusuario);

		/*=============================================
			Respuesta del modelo
		=============================================*/

		if ($delete == "ok") {

			$json = array(
				"status" => 200,
				"detalle" => "Se ha borrado su curso con éxito"

			);

			echo json_encode($json, true);

			return;
		}
	}
}
