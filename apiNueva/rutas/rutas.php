<?php
$url = parse_url($_SERVER['REQUEST_URI']);
$arrayRuta = $url["path"];
$arrayRutas = explode("/", $arrayRuta);



if (count(array_filter($arrayRutas)) == 1) {

	/*=============================================
		Cuando no se hace ninguna petición a la API
		=============================================*/

	$json = array(

		"detalle" => "no encontrado peticion a la api "

	);

	echo json_encode($json, true);

	return;
} else {

	/*=============================================
		Cuando pasamos solo un índice en el array $arrayRutas
		=============================================*/

	if (count(array_filter($arrayRutas)) == 2) {

		/*=============================================
			Cuando se hace peticiones desde usuarios
		=============================================*/

		if (array_filter($arrayRutas)[2] == "usuarios") {

			/*=============================================
				Peticiones GET para mostrar todos los usuarios
				=============================================*/

			if (isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "GET") {

				$usuario = new ControladorUsuarios();
				$usuario->index();
			}
			/*=============================================
				Peticiones POST para guardar todos los usuarios
				=============================================*/

			if (isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST") {

				/*=============================================
					Capturar datos
					=============================================*/

				$datos = array(

					'nombre' => $_POST['nombre'],
					'password' => $_POST['password'],
					'direccion' => $_POST['direccion'],
					'localidad' => $_POST['localidad'],
					'telefono' => $_POST['telefono'],
					'email' => $_POST['email'],
					'estado' => $_POST['estado'],
					'fecha' => $_POST['fecha'],
					'imagen' => $_POST['imagen']
				);


				$usuario = new ControladorUsuarios();
				$usuario->create($datos);
			} else {

				$json = array(

					"detalle" => "no encontrado para crear usuario"

				);

				echo json_encode($json, true);

				return;
			}
		}
	} else {
		/*=============================================
			Cuando se hace peticiones desde un solo usuario
		=============================================*/

		if (array_filter($arrayRutas)[2] == "usuarios" && is_numeric(array_filter($arrayRutas)[3])) {

			/*=============================================
			Peticiones GET
			=============================================*/

			if (isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "GET") {

				$usuario = new ControladorUsuarios();
				$usuario->show(array_filter($arrayRutas)[3]);
			}

			/*=============================================
			Peticiones PUT
			=============================================*/ else if (isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "PUT") {


				/*=============================================
			Capturar datos
			=============================================*/

				$datos = array();

				parse_str(file_get_contents('php://input'), $datos);
				$editarUsuario = new ControladorUsuarios();
				$editarUsuario->update(array_filter($arrayRutas)[3], $datos);
			}

			/*=============================================
			Peticiones DELETE
			=============================================*/ else if (isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "DELETE") {

				$borrarUsuario = new ControladorUsuarios();
				$borrarUsuario->delete(array_filter($arrayRutas)[3]);
			} else {

				$json = array(

					"detalle" => "no encontrado"

				);

				echo json_encode($json, true);

				return;
			}
		} else {

			$json = array(

				"detalle" => "no encontrado"

			);

			echo json_encode($json, true);

			return;
		}
	}
}
