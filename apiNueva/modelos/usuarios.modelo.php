<?php
//REQUIERO LA CONEXION DE LA DB
require_once "conexion.php";

class ModeloUsuarios
{

	/*=============================================
	Mostrar todos los registros
	=============================================*/

	static public function index($tabla)
	{

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

		$stmt->execute();

		return $stmt->fetchAll(PDO::FETCH_CLASS);

		$stmt->close();

		$stmt -= null;
	}

	/*=============================================
	Crear un registro
	=============================================*/


	/* REGISTRO
        Recibe los siguiente parametros:
        - Nombre de la tabla
        - Datos (Vienen en un arry)
        
    */

	static public function create($tabla, $datos)
	{
		#prepare() Prepra una sentencia SQL para ser ejecutada por el método PDOStatement::execute(). La sentencia SQL puede contener cero o más marcadores de parámetros con nombre (:name) o signos de interrogación (?) por los cuales los valores reales serán sustituidos cuando la sentencia sea ejecutada. Ayuda a prevenir inyecciones SQL eliminando la necesidad de entrecomillar manualmente los parámetros.

		//statement : declaracion
		$con = Conexion::conectar();
		$sql = "INSERT INTO $tabla (nombre,password,direccion,localidad,telefono,email,estado,fecha,imagen) VALUES (:nombre,:password,:direccion,:localidad,:telefono,:email,:estado,:fecha,:imagen)";
		$stmt = $con->prepare($sql);

		#binParam()Vincula una variable de PHP a un parámetro de sustitución con nombre o de signo de interrogación correspondiente de la sentencia SQL que fue usada para preparar la sentencia.

		$stmt->bindParam(':nombre', $datos['nombre'], PDO::PARAM_STR);
		$stmt->bindParam(':password', $datos['password'], PDO::PARAM_STR);
		$stmt->bindParam(':direccion', $datos['direccion'], PDO::PARAM_STR);
		$stmt->bindParam(':localidad', $datos['localidad'], PDO::PARAM_STR);
		$stmt->bindParam(':telefono', $datos['telefono'], PDO::PARAM_STR);
		$stmt->bindParam(':email', $datos['email'], PDO::PARAM_STR);
		$stmt->bindParam(':estado', $datos['estado'], PDO::PARAM_STR);
		$stmt->bindParam(':fecha', $datos['fecha'], PDO::PARAM_STR);
		$stmt->bindParam(':imagen', $datos['imagen'], PDO::PARAM_STR);

		if ($stmt->execute()) {

			return "ok";
		} else {

			print_r(Conexion::conectar()->errorInfo());
		}

		//echo "New record added";

		$stmt->close(); //Cerramos la conexión

		$stmt = null; //forma de reforzar la seguridad del sistema

	}

	/*=============================================
	Mostrar un solo curso
	=============================================*/

	static public function show($tabla, $idusuario)
	{

		// $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id=:id");

		$con = Conexion::conectar();
		$sql = "SELECT $tabla.idusuario,$tabla.nombre,$tabla.password,$tabla.direccion,$tabla.localidad,$tabla.telefono,$tabla.email,$tabla.estado,$tabla.fecha,$tabla.imagen FROM $tabla WHERE $tabla.idusuario=:idusuario ";
		$stmt = $con->prepare($sql);


		$stmt->bindParam(":idusuario", $idusuario, PDO::PARAM_INT);

		$stmt->execute();

		return $stmt->fetchAll(PDO::FETCH_CLASS);

		$stmt->close();

		$stmt -= null;
	}
	/*=============================================
	Actualización de un curso
	=============================================*/

	static public function update($tabla, $datos)
	{

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre=:nombre, password=:password, direccion=:direccion,  localidad=:localidad,telefono=:telefono,email=:email,estado=:estado,fecha=:fecha,imagen=:imagen WHERE idusuario = :idusuario");

		$stmt->bindParam(":idusuario", $datos["id"], PDO::PARAM_INT);
		$stmt->bindParam(':nombre', $datos['nombre'], PDO::PARAM_STR);
		$stmt->bindParam(':password', $datos['password'], PDO::PARAM_STR);
		$stmt->bindParam(':direccion', $datos['direccion'], PDO::PARAM_STR);
		$stmt->bindParam(':localidad', $datos['localidad'], PDO::PARAM_STR);
		$stmt->bindParam(':telefono', $datos['telefono'], PDO::PARAM_STR);
		$stmt->bindParam(':email', $datos['email'], PDO::PARAM_STR);
		$stmt->bindParam(':estado', $datos['estado'], PDO::PARAM_STR);
		$stmt->bindParam(':fecha', $datos['fecha'], PDO::PARAM_STR);
		$stmt->bindParam(':imagen', $datos['imagen'], PDO::PARAM_STR);


		if ($stmt->execute()) {

			return "ok";
		} else {

			print_r(Conexion::conectar()->errorInfo());
		}

		$stmt->close();

		$stmt = null;
	}

	/*=============================================
	Borrar curso
	=============================================*/

	static public function delete($tabla, $idusuario)
	{

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE idusuario = :idusuario");

		$stmt->bindParam(":idusuario", $idusuario, PDO::PARAM_INT);

		if ($stmt->execute()) {

			return "ok";
		} else {

			print_r(Conexion::conectar()->errorInfo());
		}

		$stmt->close();

		$stmt = null;
	}
}
