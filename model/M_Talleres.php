<?php
require_once "M_Conexion.php";
//contiene metodos para crear usuario y devolver usuarios de tabla
class M_Talleres extends Conexion
{
    private $db;
    private $talleres;

    public function __construct()
    {
        $this->db = Conexion::Conectar(); // se llama metodo estatico conectar
        //de clase Conexion
        $this->talleres = array();
    }

    //Getter taller
    public function get_workshop()
    {
        $consulta = $this->db->query("SELECT * FROM workshop");
        return $consulta;
    }

    //metodo que devuelve taller segun idtaller como parametro
    public function get_taller($id)
    {
        $consulta = $this->db->query("SELECT * FROM workshop WHERE idworkshop ='$id'");
        return $consulta;
    }
    // metodo para direcciones en mapa

   public function get_tallermapa($search)
    {
        $consulta = $this->db->query("SELECT * FROM workshop, workshop_type WHERE workshop.location='$search' AND workshop.idworkshop=workshop_type.workshop_id");
        while ($row = mysqli_fetch_array($consulta)) {
           if ($row['mechanics'] == 1) {
             $row['mechanics'] = "Mecánica";
           }else{
             $row['mechanics'] = "";
           }
           if ($row['repairs'] == 1) {
             $row['repairs'] = "Reparaciones";
           }else{
             $row['repairs'] = "";
           }
           if ($row['electricity'] == 1) {
             $row['electricity'] = "Electricidad";
           }else{
             $row['electricity'] = "";
           }
           if ($row['bodywork'] == 1) {
             $row['bodywork'] = "Chapa y pintura";
           }else{
             $row['bodywork'] = "";
           }
           if ($row['review'] == 1) {
             $row['review'] = "Revisión";
           }else{
             $row['review'] = "";
           }
           if ($row['creditcard'] == 1) {
             $row['creditcard'] = "Pago con tarjeta";
           }else{
             $row['creditcard'] = "";
           }
           if($row['state'] != "Premium") {
             $row['state'] = "";
           }
            $direcciones = '["' . $row['name'] . '", "'. $row['description'] . '", ' . $row['lat'] . ', ' . $row['workshop_long'] . ', "'. $row['image'] . '", ' . $row['idworkshop'] . ' , "'. $row['adress'] . '" , "'. $row['mechanics'] . '" , "'. $row['repairs'] . '" , "'. $row['electricity'] . '" , "'. $row['bodywork'] . '" , "'. $row['review'] . '" , "'. $row['creditcard'] . '" , "'. $row['state'] . '"],';
            echo $direcciones;
       }

    }
    // metodo para ventanas de mapa
    public function get_tallerventanas()
    {
        $consulta = $this->db->query("SELECT * FROM workshop");
        while ($row = mysqli_fetch_array($consulta)) {
            echo '["'. $row['name'] .'",'.'"'. $row['description'] .'"],';
       }

    }

    // metodo para comprobar correo en DB

    public function get_correo_t($email)
    {
        $consulta = $this->db->query("SELECT * FROM workshop WHERE email = '$email'");
        return $consulta;
    }

    //  metodo para crear un usuario
    public function crea_taller($nombre, $cif, $password, $email)
    {
        $consulta = $this->db->query("INSERT INTO workshop(name, cif, password, email, state) VALUES ('$nombre', '$cif','$password', '$email', 'Freemium')");

        return $consulta;
    }
    // metodo para actualizar tipo de taller
    public function set_workshoptype($id){
        $consulta2=$this->db->query("INSERT INTO workshop_type(mechanics, repairs, electricity, bodywork, review, creditcard, workshop_id) VALUES (0, 0, 0, 0, 0, 0, '$id')");
        return $consulta2;
    }

    // metodo para actualizar taller
    public function update_taller($name, $telefono, $direccion, $lat, $long, $ciudad, $descripcion, $id, $destino, $video){
        $consulta=$this->db->query("UPDATE workshop SET name='$name', adress='$direccion', lat='$lat', workshop_long='$long', description='$descripcion', location='$ciudad', phone='$telefono', image='$destino', video='$video' WHERE idworkshop='$id'");
        return $consulta;
    }

    // metodo para actualizar tipo de taller
    public function update_workshoptype($mech, $repair, $elec, $carbody, $overhaul, $credit, $id){
        $consulta2=$this->db->query("UPDATE workshop_type SET mechanics='$mech', repairs='$repair', electricity='$elec', bodywork='$carbody', review='$overhaul', creditcard='$credit' WHERE workshop_id='$id'");
        return $consulta2;
    }


    // método para hacer Login
    public function login($email, $password)
    {
        $consulta = $this->db->query("SELECT * FROM workshop WHERE email='$email' AND password='$password'");
        return $consulta;
    }

    // metodo para sacar datos según sesión
    public function datos_talleres($idtaller){
       $consulta=$this->db->query("SELECT * FROM workshop WHERE idworkshop = '$idtaller'");
       return $consulta;
    }

    // metodo para sacar todos los datos de todos los tipos de talleres
    public function get_allworkshoptype(){
       $consulta=$this->db->query("SELECT * FROM workshop_type");
       return $consulta;
    }

    // metodo para sacar datos según sesión
    public function get_workshoptype($idtaller){
       $consulta=$this->db->query("SELECT * FROM workshop_type WHERE workshop_id = '$idtaller'");
       return $consulta;
    }

    //Búsuqeda home-in por calles y ciudades
    public function search_taller($search){
      $consulta=$this->db->query("SELECT * FROM workshop WHERE adress LIKE '%".$search."%' OR location LIKE '%".$search."%' ORDER BY state DESC, name");
      return $consulta;
    }

    public function add_premium($id){
        $consulta=$this->db->query("UPDATE workshop SET state='Premium' WHERE idworkshop='$id'");
        return $consulta;
    }

    public function drop_premium($id){
        $consulta=$this->db->query("UPDATE workshop SET state='Freemium' WHERE idworkshop='$id'");
        return $consulta;
    }

    //Búsqueda de talleres por filtros
    public function search_filter($search, $mechanics, $repairs, $electricity, $bodywork, $review, $creditcard, $premium) {
        $arraytypes=array();
        if ($mechanics == 1) {
            array_push($arraytypes, 1);
        }
        if ($repairs == 1) {
            array_push($arraytypes, 1);
        }
        if ($electricity == 1) {
            array_push($arraytypes, 1);
        }
        if ($bodywork == 1) {
            array_push($arraytypes, 1);
        }
        if ($review == 1) {
            array_push($arraytypes, 1);
        }
        if ($creditcard == 1) {
            array_push($arraytypes, 1);
        }
        if ($premium == "Premium") {
            array_push($arraytypes, 1);
        }
        $length = count($arraytypes);
         $sql= "SELECT DISTINCT * FROM workshop_type, workshop WHERE";
         $i=0;
         if ($mechanics == 1) {
             $i++;
             $sql.=" workshop_type.mechanics='$mechanics'";
             if ($i < $length) {
             $sql.=" AND";
             }
         }
         if ($repairs == 1) {
             $i++;
             $sql.=" workshop_type.repairs='$repairs'";
             if ($i < $length) {
             $sql.=" AND";
             }
         }
         if ($electricity == 1) {
             $i++;
             $sql.=" workshop_type.electricity='$electricity'";
             if ($i < $length) {
             $sql.=" AND";
             }
         }
         if ($bodywork == 1) {
             $i++;
             $sql.=" workshop_type.bodywork='$bodywork'";
             if ($i < $length) {
             $sql.=" AND";
             }
         }
         if ($review == 1) {
             $i++;
             $sql.=" workshop_type.review='$review'";
             if ($i < $length) {
             $sql.=" AND";
             }
         }
         if ($creditcard == 1) {
             $i++;
             $sql.=" workshop_type.creditcard='$creditcard'";
             if ($i < $length) {
             $sql.=" AND";
             }
         }
         if ($premium == "Premium") {
             $i++;
             $sql.=" workshop.state='$premium'";
             if ($i < $length) {
             $sql.=" AND";
             }
         }
         if ($search != "") {
             $sql.=" AND workshop.location='$search'";
         }
         $sql.=" AND workshop.idworkshop=workshop_type.workshop_id ORDER BY workshop.state DESC";
         $consulta=$this->db->query($sql);
         if ($consulta != null) {
           return $consulta;
         }
    }

    //Búsqueda de talleres por filtros para el mapa
    public function get_filtrosmapa($search, $mechanics, $repairs, $electricity, $bodywork, $review, $creditcard, $premium) {
        $arraytypes=array();
        if ($mechanics == 1) {
            array_push($arraytypes, 1);
        }
        if ($repairs == 1) {
            array_push($arraytypes, 1);
        }
        if ($electricity == 1) {
            array_push($arraytypes, 1);
        }
        if ($bodywork == 1) {
            array_push($arraytypes, 1);
        }
        if ($review == 1) {
            array_push($arraytypes, 1);
        }
        if ($creditcard == 1) {
            array_push($arraytypes, 1);
        }
        if ($premium == "Premium") {
            array_push($arraytypes, 1);
        }
        $length = count($arraytypes);

         $sql= "SELECT DISTINCT * FROM workshop_type, workshop WHERE";
         $i=0;
         if ($mechanics == 1) {
             $i++;
             $sql.=" workshop_type.mechanics='$mechanics'";
             if ($i < $length) {
             $sql.=" AND";
             }
         }
         if ($repairs == 1) {
             $i++;
             $sql.=" workshop_type.repairs='$repairs'";
             if ($i < $length) {
             $sql.=" AND";
             }
         }
         if ($electricity == 1) {
             $i++;
             $sql.=" workshop_type.electricity='$electricity'";
             if ($i < $length) {
             $sql.=" AND";
             }
         }
         if ($bodywork == 1) {
             $i++;
             $sql.=" workshop_type.bodywork='$bodywork'";
             if ($i < $length) {
             $sql.=" AND";
             }
         }
         if ($review == 1) {
             $i++;
             $sql.=" workshop_type.review='$review'";
             if ($i < $length) {
             $sql.=" AND";
             }
         }
         if ($creditcard == 1) {
             $i++;
             $sql.=" workshop_type.creditcard='$creditcard'";
             if ($i < $length) {
             $sql.=" AND";
             }
         }
         if ($premium == "Premium") {
             $i++;
             $sql.=" workshop.state='$premium'";
             if ($i < $length) {
             $sql.=" AND";
             }
         }
         if ($search != "") {
             $sql.=" AND workshop.location='$search'";
         }
         $sql.=" AND workshop.idworkshop=workshop_type.workshop_id ORDER BY workshop.state DESC";
         $consulta=$this->db->query($sql);
         while ($row = mysqli_fetch_array($consulta)) {
            if ($row['mechanics'] == 1) {
              $row['mechanics'] = "Mecánica";
            }else{
              $row['mechanics'] = "";
            }
            if ($row['repairs'] == 1) {
              $row['repairs'] = "Reparaciones";
            }else{
              $row['repairs'] = "";
            }
            if ($row['electricity'] == 1) {
              $row['electricity'] = "Electricidad";
            }else{
              $row['electricity'] = "";
            }
            if ($row['bodywork'] == 1) {
              $row['bodywork'] = "Chapa y pintura";
            }else{
              $row['bodywork'] = "";
            }
            if ($row['review'] == 1) {
              $row['review'] = "Revisión";
            }else{
              $row['review'] = "";
            }
            if ($row['creditcard'] == 1) {
              $row['creditcard'] = "Pago con tarjeta";
            }else{
              $row['creditcard'] = "";
            }
            if($row['state'] != "Premium") {
              $row['state'] = "";
            }
             $direcciones = '["' . $row['name'] . '", "'. $row['description'] . '", ' . $row['lat'] . ', ' . $row['workshop_long'] . ', "'. $row['image'] . '", ' . $row['idworkshop'] . ' , "'. $row['adress'] . '" , "'. $row['mechanics'] . '" , "'. $row['repairs'] . '" , "'. $row['electricity'] . '" , "'. $row['bodywork'] . '" , "'. $row['review'] . '" , "'. $row['creditcard'] . '" , "'. $row['state'] . '"],';
             echo $direcciones;
        }
    }
}
