<?php
require_once "M_Conexion.php";
//contiene metodos para crear usuario y devolver usuarios de tabla

class M_Vehiculos extends Conexion
{

    private $db;
    private $vehiculos;

    public function __construct(){
        $this->db=Conexion::Conectar();// se llama metodo estatico conectar
        //de clase Conexion
        $this->vehiculos=array();
    }


     //  metodo para crear un vehículo
     public function crea_vehiculos($iduser, $marca, $modelo, $destino, $matricula, $vehicle_engine, $vehicle_km){
         $consulta = $this->db->query("INSERT INTO vehicle(uIdUser,vehicle_band,vehicle_model,image,vehicle_registration,vehicle_engine,vehicle_km)
                                       VALUES ('$iduser', '$marca', '$modelo','$destino', '$matricula', '$vehicle_engine', '$vehicle_km')");
         return $consulta;
     }


     // Edita vehículo desde edit-profile
     public function edit_vehicle($idvehicle, $marca, $modelo, $destino, $matricula, $vehicle_engine, $vehicle_km){
       $consulta=$this->db->query("UPDATE vehicle SET vehicle_band='$marca', vehicle_model='$modelo', vehicle_engine='$vehicle_engine', image='$destino', vehicle_registration='$matricula', vehicle_km='$vehicle_km' WHERE idvehicle='$idvehicle'");
        return $consulta;
     }

     // Elimina vehículo
     public function delete_vehicle($idvehicle){
       $consulta=$this->db->query("DELETE FROM vehicle WHERE idvehicle='$idvehicle'");
        return $consulta;
     }


     // Método para sacar datos vehículo según usuario sesión
      public function datos_vehiculos($idusuario){
          $consulta=$this->db->query("SELECT * FROM vehicle WHERE uIdUser = '$idusuario'");
          return $consulta;
      }

      // Método para sacar datos vehículo según id vehículo
      public function datos_vehiculosid($idvehicle){
          $consulta=$this->db->query("SELECT * FROM vehicle WHERE idvehicle = '$idvehicle'");
          return $consulta;
      }


      /*
      ---------------
      RECORDATORIOS
      ---------------
      */

      //Recordatorio ITV
      public function itvreminder($idvehicle, $reminderdate, $note){
          $consulta=$this->db->query("UPDATE vehicle SET date_itv='$reminderdate', itv_note='$note' WHERE idvehicle ='$idvehicle'");
          return $consulta;
      }

      //Recordatorio seguro
      public function insurancereminder($idvehicle, $reminderdate, $note){
          $consulta=$this->db->query("UPDATE vehicle SET vehicle_insurance='$reminderdate', vehicle_note='$note' WHERE idvehicle ='$idvehicle'");
          return $consulta;
      }

      //Recordatorio ruedas
      public function wheelsreminder($idvehicle, $reminderdate, $note){
          $consulta=$this->db->query("UPDATE vehicle SET wheels_date='$reminderdate', wheels_note='$note' WHERE idvehicle ='$idvehicle'");
          return $consulta;
      }

      //Recordatorio aceite
      public function oilreminder($idvehicle, $reminderdate, $note){
          $consulta=$this->db->query("UPDATE vehicle SET oil_date='$reminderdate', oil_note='$note' WHERE idvehicle ='$idvehicle'");
          return $consulta;
      }

      //Recordatorio revisión
      public function reviewreminder($idvehicle, $reminderdate, $note){
          $consulta=$this->db->query("UPDATE vehicle SET review_date='$reminderdate', review_note='$note' WHERE idvehicle ='$idvehicle'");
          return $consulta;
      }

      //Eliminar recordatorios
      public function delete_reminder($idvehicle, $date_itv, $itv_note, $wheels_date, $wheels_note, $oil_date, $oil_note, $review_date, $review_note, $vehicle_insurance, $vehicle_note){
          $consulta=$this->db->query("UPDATE vehicle SET date_itv='$date_itv', itv_note='$itv_note', wheels_date='$wheels_date', oil_date='$oil_date', review_date='$review_date', vehicle_insurance='$vehicle_insurance', wheels_note='$wheels_note', oil_note='$oil_note', review_note='$review_note', vehicle_note='$vehicle_note' WHERE idvehicle='$idvehicle'");
          return $consulta;
      }



}
