<?php
require_once "M_Conexion.php";
//contiene metodos para crear usuario y devolver usuarios de tabla
class M_Documentos extends Conexion
{
    private $db;
    private $documentos;

    public function __construct()
    {
        $this->db = Conexion::Conectar(); // se llama metodo estatico conectar
        //de clase Conexion
        $this->documentos = array();
    }

    //Método que devuelve documento segun idusuario
    public function get_document($id)
    {
        $consulta = $this->db->query("SELECT * FROM document WHERE idUser ='$id'");
        return $consulta;
    }

    //Método para crear un documento
    public function new_document($type, $notes, $iduser, $idvehicle, $destino)
    {
        $consulta = $this->db->query("INSERT INTO document(type_document, documents, notes, idUser, idVehicle) VALUES ('$type', '$destino', '$notes', '$iduser', '$idvehicle')");
        return $consulta;
    }

    //Método para eliminar documentos
    public function delete_document($iddocument){
       $consulta=$this->db->query("DELETE FROM document WHERE iddocument = '$iddocument'");
       return $consulta;
    }

}
