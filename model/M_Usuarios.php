<?php
require "M_Conexion.php";
//contiene metodos para crear usuario y devolver usuarios de tabla
class M_Usuarios extends Conexion {
    private $db;
    private $usuarios;

    public function __construct(){
        $this->db=Conexion::Conectar();// se llama metodo estatico conectar
        //de clase Conexion
        $this->usuarios=array();
    }

    // metodo getter para usuarios

         public function get_usuarios(){
             $consulta=$this->db->query("SELECT * FROM user");
             return $consulta;
         }


     // metodo para actualizar usuario segun formulario profile-user.php

     public function update_usuario($direccion, $ciudad, $telefono, $fecha, $destino, $id){
         $consulta=$this->db->query("UPDATE user SET adress='$direccion', location='$ciudad', phone='$telefono', birth_date='$fecha', image='$destino' WHERE iduser='$id'");
         return $consulta;
     }

     // metodo para comprobar correo en DB

          public function get_correo($email){
              $consulta=$this->db->query("SELECT * FROM user WHERE email = '$email'");
              return $consulta;
          }

    // metodo para sacar datos según sesión

         public function datos_usuarios($idusuario){
             $consulta=$this->db->query("SELECT * FROM user WHERE iduser = '$idusuario'");
             return $consulta;
         }


    //  metodo para crear un usuario
         public function crea_usuario($nombre, $password, $email){
           $consulta=$this->db->query("INSERT INTO user(name, pass, email, state) VALUES ('$nombre', '$password', '$email', '1')");
           return $consulta;
         }


    // método para hacer Login
         public function login($email, $password){
            $consulta=$this->db->query("SELECT * FROM user WHERE email='$email' AND pass='$password'");
            return $consulta;
         }
}
?>
