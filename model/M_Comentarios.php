<?php

require_once "M_Conexion.php";

class M_Comentarios extends Conexion
{
    private $db;
    private $comentarios;

    public function __construct()
    {
        $this->db = Conexion::Conectar(); // se llama metodo estatico conectar
        //de clase Conexion
        $this->comentarios = array();
    }

    //metodo que devuelve comentarios según el id del comentario
    public function getid_comment($id)
    {
        $consulta = $this->db->query("SELECT * FROM commentary WHERE idcommentary ='$id'");
        return $consulta;
    }

    //metodo que devuelve comentarios según el id del taller
    public function workshopid_comment($id)
    {
        $consulta = $this->db->query("SELECT * FROM commentary WHERE idworkshops ='$id' AND response='0' ORDER BY idcommentary DESC");
        return $consulta;
    }

    //  metodo para postear una reseña
    public function post_comment($commentary, $idusers, $idworkshops)
    {
        $consulta = $this->db->query("INSERT INTO commentary (commentary, idusers, idworkshops, create_date) VALUES ('$commentary', '$idusers', '$idworkshops', NOW())");
        return $consulta;
    }

    //  metodo para responder a una reseña
    public function post_reply($commentary, $idusers, $workshopname, $idworkshops, $response)
    {
        $consulta = $this->db->query("INSERT INTO commentary (commentary, idusers, workshopname, idworkshops, response, create_date) VALUES ('$commentary', '$idusers', '$workshopname', '$idworkshops', '$response', NOW())");
        return $consulta;
    }

    // metodo para actualizar usuario segun formulario profile-user.php
    public function edit_comment($idcommentary, $commentary, $idusers, $idworkshops){
        $consulta=$this->db->query("UPDATE commentary SET commentary='$commentary', update_date=NOW() WHERE idusers='$idusers' AND idcommentary='$idcommentary'");
        return $consulta;
    }

    //Método para eliminar comentarios
    public function del_comment($idcommentary){
        $consulta=$this->db->query("DELETE FROM commentary WHERE idcommentary='$idcommentary'");
        return $consulta;
    }

    //Método para mostrar respuestas
    public function workshopid_reply($id)
    {
        $consulta = $this->db->query("SELECT * FROM commentary WHERE response='$id' ORDER BY idcommentary DESC");
        return $consulta;
    }

    // método para comprobar número de comentarios de usuario
         public function count_comments($iduser, $idworkshop){
            $consulta=$this->db->query("SELECT COUNT(*) FROM commentary WHERE response=0 AND idusers='$iduser' AND idworkshops='$idworkshop'");
            return $consulta;
         }

     //metodo que devuelve los rating de un usuario según su sesión y el taller en el que se encuentre
     public function get_rating_user($iduser, $idworkshop) {
         $consulta = $this->db->query("SELECT * FROM ranking WHERE id_users='$iduser' AND id_workshops='$idworkshop'");
         return $consulta;
     }

     //metodo que devuelve todos los ratings
     public function get_allrating() {
         $consulta = $this->db->query("SELECT * FROM ranking");
         return $consulta;
     }


     //metodo que devuelve los rating de un usuario según su sesión y el taller en el que se encuentre
     public function get_rating($idworkshop) {
         $consulta = $this->db->query("SELECT * FROM ranking WHERE id_workshops='$idworkshop'");
         return $consulta;
     }

     //metodo que añade el rating de un usuario a un taller
     public function post_rating($iduser, $idworkshop, $rating) {
         $consulta = $this->db->query("INSERT INTO ranking (ranking, id_users, id_workshops) VALUES ('$rating', '$iduser', '$idworkshop')");
         return $consulta;
     }

     //update del rating de un usuario a un taller
     public function update_rating($iduser, $idworkshop, $rating) {
         $consulta = $this->db->query("UPDATE ranking SET ranking='$rating' WHERE id_users ='$iduser' AND id_workshops='$idworkshop'");
         return $consulta;
     }

     //Media de las puntuaciones de un taller
     public function getavg_rating($idworkshop) {
         $consulta = $this->db->query("SELECT AVG(ranking) FROM ranking WHERE id_workshops='$idworkshop'");
         return $consulta;
     }

}

 ?>
