<?php

require_once "M_Conexion.php";

class M_Favoritos extends Conexion
{
    private $db;
    private $favoritos;

    public function __construct()
    {
        $this->db = Conexion::Conectar(); // se llama metodo estatico conectar
        //de clase Conexion
        $this->favoritos = array();
    }
    // metodo para añadir un favorito
    public function set_favorites($iduser, $idworkshop)
    {
        $consulta = $this->db->query("INSERT INTO favorites (idsuser, idsworkshop) VALUES ('$iduser', '$idworkshop')");
        return $consulta;
    }

    //Método para comprobar favoritos
    public function getfav($iduser, $idworkshop)
    {
        $consulta = $this->db->query("SELECT * FROM favorites WHERE idsuser ='$iduser' AND idsworkshop ='$idworkshop'");
        return $consulta;
    }

    //Método para eliminar favoritos
    public function drop_fav($iduser, $idworkshop){
        $consulta=$this->db->query("DELETE FROM favorites WHERE idsuser ='$iduser' AND idsworkshop ='$idworkshop'");
        return $consulta;
    }

    //Mostrar todos los talleres faavoritos de un usuario
    public function getallfavs($id)
    {
        $consulta = $this->db->query("SELECT * FROM favorites, workshop, workshop_type WHERE favorites.idsuser ='$id' AND favorites.idsworkshop=workshop.idworkshop AND workshop.idworkshop=workshop_type.workshop_id ORDER BY workshop.state DESC");
        return $consulta;
    }

}

 ?>
