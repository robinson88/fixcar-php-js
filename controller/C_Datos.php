<?php

include "../../model/M_Usuarios.php";
$usuario2= new M_Usuarios;

include "../../model/M_Vehiculos.php";
$vehiculo2= new M_Vehiculos;

include "../../model/M_Talleres.php";
$taller2= new M_Talleres;

include "../../model/M_Documentos.php";
$documento2= new M_Documentos;

include "../../model/M_Comentarios.php";
$comentario2= new M_Comentarios;

include "../../model/M_Favoritos.php";
$fav2= new M_Favoritos;

/*---------------------------------------------
Recibir datos para las sesiones de los usuarios
---------------------------------------------*/
if (isset($_SESSION['idusuario'])) {
    //Consulta para mostrar los datos de usuarios
    $consulta1=$usuario2->datos_usuarios($_SESSION['idusuario']);
    $row=mysqli_fetch_array($consulta1);
    //Consulta para mostrar los documentos
    $consultad1=$documento2->get_document($_SESSION['idusuario']);
    //Consulta para contar el número de comentarios de un usuario en un taller
    if (isset($_GET['id'])) {
      $consultancuser=$comentario2->count_comments($_SESSION['idusuario'], $_GET['id']);
      $rownc=mysqli_fetch_array($consultancuser);
      //Datos para los favoritos
      $consultafav=$fav2->getfav($_SESSION['idusuario'], $_GET['id']);
      $countfav=mysqli_num_rows($consultafav);
    }
    //Consulta para recibir los vehículos de un usuario
    $consultav=$vehiculo2->datos_vehiculos($_SESSION['idusuario']);
    //Recibir los favoritos del usuario
    $consultaallfav=$fav2->getallfavs($_SESSION['idusuario']);
    $rowallfav=mysqli_fetch_array($consultaallfav);
    //Formato fecha
    $hoy = date("d-m-Y");
    $arraymedia=array();
    foreach ($consultaallfav as $rowt) {
    $consultaratuser=$comentario2->get_rating($rowt['idworkshop']);
    $rowratuser=mysqli_fetch_array($consultaratuser);
      //Consulta para mostrar la media de puntuación del taller y redondearla
      $consultarat=$comentario2->getavg_rating($rowt['idworkshop']);
      $rowrat=mysqli_fetch_array($consultarat);
      $media=round($rowrat['AVG(ranking)']);
      array_push ($arraymedia, $media);
      }
}

/*---------------------------------------------
Recibir datos para las sesiones de los talleres
---------------------------------------------*/

if (isset($_SESSION['idtaller'])) {
    //Consulta para mostrar los comentarios de los talleres
    $consultacw=$comentario2->workshopid_comment($_SESSION['idtaller']);
    $rowcw=mysqli_fetch_array($consultacw);
    //Consulta para mostrar los datos de usuarios en los comentarios
    $consultauc=$usuario2->get_usuarios();
    $rowuc=mysqli_fetch_array($consultauc);
    //Consulta para mostrar los datos de talleres en los comentarios
    $consultawc=$taller2->get_workshop();
    $rowwc=mysqli_fetch_array($consultawc);
    //Consulta para recibir el rating del taller en los comentarios, y para mostrar el número de votos recibidos
    $consultaratuser=$comentario2->get_rating($_SESSION['idtaller']);
    $countratingsn=mysqli_num_rows($consultaratuser);
    $rowratuser=mysqli_fetch_array($consultaratuser);
    //Consulta para mostrar la media de puntuación del taller y redondearla
    $consultarat=$comentario2->getavg_rating($_SESSION['idtaller']);
    $rowrat=mysqli_fetch_array($consultarat);
    $media=round($rowrat['AVG(ranking)']);
    $mediadec=number_format($rowrat['AVG(ranking)'], 1);
    //Consulta para mostrar los datos de los talleres
    $consultat=$taller2->datos_talleres($_SESSION['idtaller']);
    $rowt=mysqli_fetch_array($consultat);
    //Consulta para mostrar los datos del tipo de taller
    $consultatt=$taller2->get_workshoptype($_SESSION['idtaller']);
    $rowtt=mysqli_fetch_array($consultatt);
}

/*--------------------------------------------------------------
Recibir datos para las páginas de los talleres a través del GET
--------------------------------------------------------------*/

if (isset($_GET['id'])) {
    //Consulta para mostrar los datos de los talleres
    $consultadt=$taller2->datos_talleres($_GET['id']);
    $rowdt=mysqli_fetch_array($consultadt);
    //Consulta para mostrar los datos de los tipos de talleres
    $consultatt=$taller2->get_workshoptype($_GET['id']);
    $rowtt=mysqli_fetch_array($consultatt);
    //Consulta para mostrar los comentarios de un taller
    $consultacw=$comentario2->workshopid_comment($_GET['id']);
    $rowcw=mysqli_fetch_array($consultacw);
    //Consulta para mostrar los datos de los usuarios
    $consultauc=$usuario2->get_usuarios();
    $rowuc=mysqli_fetch_array($consultauc);
    //Consulta para mostrar los datos de talleres
    $consultawc=$taller2->get_workshop();
    $rowwc=mysqli_fetch_array($consultawc);
    //Consulta para recibir el rating del taller en los comentarios, y para mostrar el número de votos recibidos
    $consultaratuser=$comentario2->get_rating($_GET['id']);
    $countratingsn=mysqli_num_rows($consultaratuser);
    $rowratuser=mysqli_fetch_array($consultaratuser);
    //Consulta para mostrar la media de puntuación del taller y redondearla
    $consultarat=$comentario2->getavg_rating($_GET['id']);
    $rowrat=mysqli_fetch_array($consultarat);
    $media=round($rowrat['AVG(ranking)']);
    $mediadec=number_format($rowrat['AVG(ranking)'], 1);
}

/*------------------------------------------
Datos específicos para situaciones concretas
------------------------------------------*/

//Recibir datos para las búsquedas
if (isset($_GET['search-in']) || isset($_GET['search']) && !isset($_GET['pr'])) {
  if (isset($_GET['search'])) {
    $consultasearch=$taller2->search_taller($_GET['search']);
  }elseif(isset($_GET['search-in'])){
    $consultasearch=$taller2->search_taller($_GET['search-in']);
  }
  $rowt=mysqli_fetch_array($consultasearch);
  $consultast=$taller2->get_allworkshoptype();
  $rowst=mysqli_fetch_array($consultast);
  $arraymedia=array();
  foreach ($consultasearch as $rowt) {
  $consultaratuser=$comentario2->get_rating($rowt['idworkshop']);
  $rowratuser=mysqli_fetch_array($consultaratuser);
    //Consulta para mostrar la media de puntuación del taller y redondearla
    $consultarat=$comentario2->getavg_rating($rowt['idworkshop']);
    $rowrat=mysqli_fetch_array($consultarat);
    $media=round($rowrat['AVG(ranking)']);
    array_push ($arraymedia, $media);
    }
}


//Recibir datos para la búsqueda por filtros
if (isset($_GET['pr'])) {
  if (isset($_GET['search'])) {
    $consultafiltersearch=$taller2->search_filter($_GET['search'], $_GET['mech'], $_GET['rep'], $_GET['el'], $_GET['bo'], $_GET['ov'], $_GET['cr'], $_GET['pr']);
  }elseif(isset($_GET['search-in'])){
    $consultafiltersearch=$taller2->search_filter($_GET['search-in'], $_GET['mech'], $_GET['rep'], $_GET['el'], $_GET['bo'], $_GET['ov'], $_GET['cr'], $_GET['pr']);
  }
  if ($consultafiltersearch != null) {
  $rowfilt=mysqli_fetch_array($consultafiltersearch);
  $consultast=$taller2->get_allworkshoptype();
  $rowst=mysqli_fetch_array($consultast);
  $arraymedia=array();
  foreach ($consultafiltersearch as $rowfilt) {
  $consultaratuser=$comentario2->get_rating($rowfilt['idworkshop']);
  $rowratuser=mysqli_fetch_array($consultaratuser);
    //Consulta para mostrar la media de puntuación del taller y redondearla
    $consultarat=$comentario2->getavg_rating($rowfilt['idworkshop']);
    $rowrat=mysqli_fetch_array($consultarat);
    $media=round($rowrat['AVG(ranking)']);
    array_push ($arraymedia, $media);
    }
  }
}

//Recibir datos para editar comentarios
if (isset($_GET['edit'])) {
  $consultaed=$comentario2->getid_comment($_GET['edit']);
  $rowed=mysqli_fetch_array($consultaed);
}

?>
