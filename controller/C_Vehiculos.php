<?php

session_start();

require "../model/M_Vehiculos.php";
$vehiculo = new M_Vehiculos;

//Añadir vehículo
if (isset($_POST['addvehicle'])) {
    $iduser = $_SESSION['idusuario'];
    $marca = $_POST['vehicle_band'];
    $modelo = $_POST['vehicle_model'];
    $matricula = $_POST['vehicle_registration'];
    $vehicle_engine = $_POST['vehicle_engine'];
    $vehicle_km = $_POST['vehicle_km'];
    $tips = 'jpg';
    $type = array('image/jpeg' => 'jpg');
    $imagename=$_FILES['file']['name'];
    $ruta=$_FILES['file']['tmp_name'];
    $name = $iduser.'Vehic_'.round(microtime(true) * 1000).'.'.$tips;
    $destino="../view/assets/images/vehicle-pics/".$name;
    copy($ruta, $destino);
    $vehiculo->crea_vehiculos($iduser, $marca, $modelo, $destino, $matricula, $vehicle_engine, $vehicle_km);
    echo '<script type="text/javascript">
          alert("Vehículo añadido");
          window.location.href="../view/profiles/profile-vehicle.php";
          </script>';

}


//Editar vehículo
if (isset($_POST['editvehicle'])) {
    $idvehicle=$_GET['id'];
    $consulta=$vehiculo->datos_vehiculosid($idvehicle);
    $row = mysqli_fetch_array($consulta);
    $marca = $_POST['vehicle_band'];
    $modelo = $_POST['vehicle_model'];
    $matricula = $_POST['vehicle_registration'];
    $vehicle_engine = $_POST['vehicle_engine'];
    $vehicle_km = $_POST['vehicle_km'];
    $idvehicle = $_GET['id'];
    $tips = 'jpg';
    $type = array('image/jpeg' => 'jpg');
    $id = $row['idvehicle'];
    $imagename=$_FILES['file']['name'];
    $ruta=$_FILES['file']['tmp_name'];
    $name = $id.'_'.round(microtime(true) * 1000).'.'.$tips;
    $destino="";
    if(is_uploaded_file($ruta)) {
      $destino="../view/assets/images/vehicle-pics/".$name;
      copy($ruta, $destino);
    }else{
      $destino=$row['image'];
    }
    $vehiculo->edit_vehicle($idvehicle, $marca, $modelo, $destino, $matricula, $vehicle_engine, $vehicle_km);
    echo '<script type="text/javascript">
          alert("Vehículo editado");
          window.location.href="../view/profiles/profile-vehicle.php";
          </script>';

}

//Eliminar vehículo
if (isset($_POST['deletevehicle'])) {
    $idvehicle=$_GET['id'];
    $vehiculo->delete_vehicle($idvehicle);
    echo '<script type="text/javascript">
          alert("Vehículo eliminado");
          window.location.href="../view/profiles/profile-vehicle.php";
          </script>';

}


//Añadir recordatorios
if (isset($_POST['newreminder'])) {
    $idvehicle=$_POST['car'];
    $reminderdate=$_POST['reminder-date'];
    $note=$_POST['note'];
    $consulta=$vehiculo->datos_vehiculosid($idvehicle);
    $row = mysqli_fetch_array($consulta);
      if ($_POST['type'] == 'itv') {
        $vehiculo->itvreminder($idvehicle, $reminderdate, $note);
        echo '<script type="text/javascript">
              alert("¡Reconrdatorio añadido!");
              window.location.href="../view/profiles/edit-reminders.php";
              </script>';
      }elseif($_POST['type'] == 'insurance') {
        $vehiculo->insurancereminder($idvehicle, $reminderdate, $note);
        echo '<script type="text/javascript">
              alert("¡Reconrdatorio añadido!");
              window.location.href="../view/profiles/edit-reminders.php";
              </script>';
      }elseif($_POST['type'] == 'wheels') {
        $vehiculo->wheelsreminder($idvehicle, $reminderdate, $note);
        echo '<script type="text/javascript">
              alert("¡Reconrdatorio añadido!");
              window.location.href="../view/profiles/edit-reminders.php";
              </script>';
      }elseif($_POST['type'] == 'oil') {
        $vehiculo->oilreminder($idvehicle, $reminderdate, $note);
        echo '<script type="text/javascript">
              alert("¡Reconrdatorio añadido!");
              window.location.href="../view/profiles/edit-reminders.php";
              </script>';
      }else{
        $vehiculo->reviewreminder($idvehicle, $reminderdate, $note);
        echo '<script type="text/javascript">
              alert("¡Reconrdatorio añadido!");
              window.location.href="../view/profiles/edit-reminders.php";
              </script>';
      }
}

//Eliminar recordatorio
if (isset($_POST['deletereminder'])) {
    $idvehicle=$_POST['id'];
    $date_itv=$_POST['date_itv'];
    $itv_note=$_POST['itv_note'];
    $wheels_date=$_POST['wheels_date'];
    $wheels_note=$_POST['wheels_note'];
    $oil_date=$_POST['oil_date'];
    $oil_note=$_POST['oil_note'];
    $review_date=$_POST['review_date'];
    $review_note=$_POST['review_note'];
    $vehicle_insurance=$_POST['vehicle_insurance'];
    $vehicle_note=$_POST['vehicle_note'];
    $vehiculo->delete_reminder($idvehicle, $date_itv, $itv_note, $wheels_date, $wheels_note, $oil_date, $oil_note, $review_date, $review_note, $vehicle_insurance, $vehicle_note);
    echo '<script type="text/javascript">
          alert("Recordatorio eliminado");
          window.location.href="../view/profiles/edit-reminders.php";
          </script>';

}
