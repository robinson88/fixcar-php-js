<?php

require "../model/M_Documentos.php";

$documento2 = new M_Documentos;

//Añadir documento
if (isset($_POST['adddoc'])) {
  session_start();
  $iduser=$_SESSION['idusuario'];
  $idvehicle=$_POST['vehicle'];
  $notes=$_POST['notes'];
  $type_document=$_POST['documentradio'];
  $documentpath = '../view/assets/images/document-pics/'.$iduser;
  if (!file_exists($documentpath)) {
      mkdir($documentpath, 0777, true);
  }
  $tips = 'jpg';
  $type = array('image/jpeg' => 'jpg');
  $imagename=$_FILES['file']['name'];
  $ruta=$_FILES['file']['tmp_name'];
  $name = $iduser.'_doc_'.round(microtime(true) * 1000).'.'.$tips;
  $destino="../view/assets/images/document-pics/".$iduser."/".$name;
  copy($ruta, $destino);
  $documento2->new_document($type_document, $notes, $iduser, $idvehicle, $destino);
   echo '<script type="text/javascript">
         alert("Documento añadido.");
         window.location.href="../view/profiles/documents.php";
         </script>';
}

//Eliminar documento
if (isset($_POST['deletedoc'])) {
    $iddocument=$_POST['iddocument'];
    $documento2->delete_document($iddocument);
    echo '<script type="text/javascript">
          alert("Documento eliminado");
          window.location.href="../view/profiles/documents.php";
          </script>';

}

// Función que crea el archivo ZIP con todos los documentos de un usuario
function createZip($zip,$dir){
  if (is_dir($dir)){
    if ($dh = opendir($dir)){
       while (($file = readdir($dh)) !== false){
          if($file != '' && $file != '.' && $file != '..'){

             $zip->addFile($dir.$file);
          }
       }
       closedir($dh);
     }
  }
}

//Crear copia temporal del directorio con los documentos de un usuario
function full_copy( $source, $target ) {
    if ( is_dir( $source ) ) {
        @mkdir( $target );
        $d = dir( $source );
        while ( FALSE !== ( $entry = $d->read() ) ) {
            if ( $entry == '.' || $entry == '..' ) {
                continue;
            }
            $Entry = $source . '/' . $entry;
            if ( is_dir( $Entry ) ) {
                full_copy( $Entry, $target . '/' . $entry );
                continue;
            }
            copy( $Entry, $target . '/' . $entry );
        }

        $d->close();
    }else {
        copy( $source, $target );
    }
}

//Eliminar el directorio temporal
function deleteDirectory($dir) {
    if(!$dh = @opendir($dir)) return;
    while (false !== ($current = readdir($dh))) {
        if($current != '.' && $current != '..') {
            if (!@unlink($dir.'/'.$current))
                deleteDirectory($dir.'/'.$current);
        }
    }
    closedir($dh);
    @rmdir($dir);
}

// Descargar todos los documentos en un zip
if(isset($_POST['downloadall'])){
  session_start();
  $nameuser=$_SESSION['usuario'];
  $iduser=$_SESSION['idusuario'];
  //Asignamos la carpeta que queremos copiar
  $source = '../view/assets/images/document-pics/'.$iduser.'/';
  //El destino donde se guardara la copia
  $destination = $nameuser.'/';
  full_copy($source, $destination);
  $zip = new ZipArchive();
  $filename = "./Mi_documentación.zip";
  if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
    exit("No se puede abrir <$filename>");
  }
  createZip($zip,$destination);
  $zip->close();
  $filename = "Mi_documentación.zip";
  if (file_exists($filename)) {
     header('Content-Type: application/zip');
     header('Content-Disposition: attachment; filename="'.basename($filename).'"');
     header('Content-Length: ' . filesize($filename));
     flush();
     readfile($filename);
     // Borrar zip tras su descarga
     unlink($filename);

   }
   deleteDirectory($destination);
   echo '<script type="text/javascript">
         window.location.href="../view/profiles/documents.php";
         </script>';
}
