<?php

require "../model/M_Comentarios.php";

$comentario2 = new M_Comentarios;

//Hacer una nueva reseña
if (isset($_POST['postcomment'])) {
  session_start();
  $idusers=$_SESSION['idusuario'];
  $idworkshops=$_GET['id'];
  $commentary=$_POST['commentary'];
  $rating=$_POST['rating'];
  $comentario2->post_rating($idusers, $idworkshops, $rating);
  $comentario2->post_comment($commentary, $idusers, $idworkshops);
  echo '<script type="text/javascript">
        alert("Reseña publicada");
        window.location.href="../view/search-in/workshop-view.php?id='.$idworkshops.'#coms";
        </script>';
}

//Responder a una reseña
if (isset($_POST['postreply'])) {
  session_start();
  if (isset($_SESSION['idusuario'])) {
  $idusers=$_SESSION['idusuario'];
  $workshopname='';
  }
  if (isset($_SESSION['idtaller'])) {
  $idusers='';
  $workshopname=$_SESSION['taller'];
  }
  $idworkshops=$_GET['id'];
  $response=$_GET['reply'];
  $commentary=$_POST['commentary'];
  $comentario2->post_reply($commentary, $idusers, $workshopname, $idworkshops, $response);
  echo '<script type="text/javascript">
        alert("Reseña publicada");';
        if (isset($_SESSION['idusuario'])) {
        echo 'window.location.href="../view/search-in/workshop-view.php?id='.$idworkshops.'#comments";';
        }
        if (isset($_SESSION['idtaller'])) {
        echo 'window.location.href="../view/profiles/profile-workshop.php";';
        }
        echo '</script>';
}

//Editar una reseña
if (isset($_POST['editcomment'])) {
  session_start();
  if (isset($_SESSION['idusuario'])) {
  $idusers=$_SESSION['idusuario'];
  }
  if (isset($_SESSION['idtaller'])) {
  $idusers='';
  }
  $idworkshops=$_GET['id'];
  $idcommentary=$_GET['edit'];
  $commentary=$_POST['commentary'];
  $rating=$_POST['rating'];
  $comentario2->update_rating($idusers, $idworkshops, $rating);
  $comentario2->edit_comment($idcommentary, $commentary, $idusers, $idworkshops);
  echo '<script type="text/javascript">
        alert("Comentario editado");';
        if (isset($_SESSION['idusuario'])) {
        echo 'window.location.href="../view/search-in/workshop-view.php?id='.$idworkshops.'#'.$idcommentary.'";';
        }
        if (isset($_SESSION['idtaller'])) {
        echo 'window.location.href="../view/profiles/profile-workshop.php#'.$idcommentary.'";';
        }
        echo '</script>';
}

//Eliminar una reseña
if (isset($_POST['deletecomment'])) {
    $idcommentary=$_POST['idcommentary'];
    $comentario2->del_comment($idcommentary);
    echo '<script type="text/javascript">
          alert("Comentario eliminado");
          window.location.href="../view/search-in/workshop-view.php?id='.$_GET['id'].'";
          </script>';

}

 ?>
