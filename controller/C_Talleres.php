<?php

require "../model/M_Talleres.php";

$taller2 = new M_Talleres;
//$taller2->get_usuario();


//Formulario de registro
if (isset($_POST['registrarTaller'])) {
    $cif = $_POST['cif'];
    $nombre = $_POST['nombre'];
    $email = $_POST['email'];
    $password = md5($_POST['password']);
    $confirm = md5($_POST['confirma']);

    // se consulta email en base de datos y se compara con el ingresado
    $consulta2 = $taller2->get_correo_t($email);
    $count = mysqli_num_rows($consulta2);
    if ($count == 0) {
        //Se asegura de que la contraseña esté bien introducida
        if ($password == $confirm) {
            $taller2->crea_taller($nombre, $cif, $password, $email);
            $getmail=$taller2->get_correo_t($email);
            $grow=mysqli_fetch_array($getmail);
            $taller2->set_workshoptype($grow['idworkshop']);
            echo '<script type="text/javascript">
              alert("Registrado con éxito!");
              window.location.href="../view/login/login-workshop.php";
              </script>';
        } else {
          echo '<script type="text/javascript">
                alert("Las contraseñas no coinciden, vuelve a intentarlo.");
                window.location.href="../view/register-options/register-user.php";
                </script>';
        }
    } else {
      echo '<script type="text/javascript">
            alert("Ya existe un taller registrado con este mail.");
            window.location.href="../view/register-options/register-user.php";
            </script>';
    }
}


// $direccion=$this->db-> real_escape_string($_POST['direccion']);
// $localidad=$this->db-> real_escape_string($_POST['localidad']);
// $telefono=$this->db-> real_escape_string($_POST['telefono']);


//Inicio de sesión
if (isset($_POST['login'])) {
    $email = $_POST['email'];
    $password = md5($_POST['password']);
    $consulta2 = $taller2->login($email, $password);
    $count = mysqli_num_rows($consulta2);
    if ($count != 0) {
        while ($row = mysqli_fetch_array($consulta2)) {
            if ($email == $row['email'] && $password == $row['password']) {
                session_start();
                $_SESSION['taller'] = $row['name'];
                $_SESSION['idtaller'] = $row['idworkshop'];
                echo '<script type="text/javascript">
                      alert("Logueado con éxito!");
                      window.location.href="../view/profiles/profile-workshop.php";
                      </script>';
            }
        }
    } else {
        echo '<script type="text/javascript">
                alert("El email o la contraseña son incorrectos. Inténtalo de nuevo.");
                window.location.href="../view/login/login-workshop.php";
                </script>';
    }
}

//Update del taller

if (isset($_POST['updatetaller'])) {
  session_start();
  $id=$_SESSION['idtaller'];
  $consulta=$taller2->get_taller($id);
  $row = mysqli_fetch_array($consulta);
  $cif=$_POST['cif'];
  $video=substr($_POST['video'], -11);
  $namework=$_POST['namework'];
  $telefono=$_POST['telefono'];
  $direccion=$_POST['direccion'];
  $lat=$_POST['lat'];
  $long=$_POST['long'];
  $ciudad=$_POST['ciudad'];
  $descripcion=$_POST['descripcion'];
  if(isset($_POST['mech'])){
  $mech=1;
  }else{
  $mech=0;
  }
  if(isset($_POST['elec'])){
  $elec=1;
  }else{
  $elec=0;
  }
  if(isset($_POST['repair'])){
  $repair=1;
  }else{
  $repair=0;
  }
  if(isset($_POST['overhaul'])){
  $overhaul=1;
  }else{
  $overhaul=0;
  }
  if(isset($_POST['carbody'])){
  $carbody=1;
  }else{
  $carbody=0;
  }
  if(isset($_POST['credit'])){
  $credit=1;
  }else{
  $credit=0;
  }
  $tips = 'jpg';
  $type = array('image/jpeg' => 'jpg');
  $imagename=$_FILES['workshoppic']['name'];
  $ruta=$_FILES['workshoppic']['tmp_name'];
  $name = $id.'_'.round(microtime(true) * 1000).'.'.$tips;
  $destino="";
  if(is_uploaded_file($ruta)) {
    $destino="../view/assets/images/workshop-pics/".$name;
    copy($ruta, $destino);
  }else{
    $destino=$row['image'];
  }
  //$mech, $elec, $repair, $overhaul, $carbody, $credit,
   $taller2->update_taller($namework, $telefono, $direccion, $lat, $long, $ciudad, $descripcion, $id, $destino, $video);
   $taller2->update_workshoptype($mech, $repair, $elec, $carbody, $overhaul, $credit, $id);
   echo '<script type="text/javascript">
         alert("Perfil actualizado con éxito.");
         window.location.href="../view/profiles/profile-workshop.php";
         </script>';
}


//Búsqueda de talleres home-in
if (isset($_POST['search-in'])) {
    $search = trim($_POST['search-in']);
    if (empty($search)){
      echo "¡No has escrito nada! Introduce tu calle o tu ciudad";
    }else{
      echo '<script type="text/javascript">
            window.location.href="../view/search-in/search.php?search-in='.$search.'";
            </script>';
    }
}

//Búsqueda de talleres home
if (isset($_POST['search'])) {
    $search = trim($_POST['search']);
    if (empty($search)){
      echo "¡No has escrito nada! Introduce tu calle o tu ciudad";
    }else{
      echo '<script type="text/javascript">
            window.location.href="../view/search/search.php?search='.$search.'";
            </script>';
    }
}

//Búsqueda por filtros
if (isset($_POST['filtersearch'])) {
  session_start();
  if (isset($_GET['search'])) {
    $search = $_GET['search'];
  }elseif (isset($_GET['search-in'])) {
    $search = $_GET['search-in'];
  }else{
    $search = "";
  }
  if(isset($_POST['mechanics'])){
  $mech=1;
  }else{
  $mech=0;
  }
  if(isset($_POST['electricity'])){
  $elec=1;
  }else{
  $elec=0;
  }
  if(isset($_POST['repair'])){
  $repair=1;
  }else{
  $repair=0;
  }
  if(isset($_POST['overhaul'])){
  $overhaul=1;
  }else{
  $overhaul=0;
  }
  if(isset($_POST['bodywork'])){
  $carbody=1;
  }else{
  $carbody=0;
  }
  if(isset($_POST['creditcard'])){
  $credit=1;
  }else{
  $credit=0;
  }
  if(isset($_POST['premium'])){
  $premium="Premium";
  }else{
  $premium="0";
  }
  if (isset($_SESSION['idusuario'])) {
    echo '<script type="text/javascript">
          window.location.href="../view/search-in/inline.php?search-in='.$search.'&mech='.$mech.'&el='.$elec.'&rep='.$repair.'&ov='.$overhaul.'&bo='.$carbody.'&cr='.$credit.'&pr='.$premium.'";
          </script>';
  }else{
    echo '<script type="text/javascript">
          window.location.href="../view/search/inline.php?search='.$search.'&mech='.$mech.'&el='.$elec.'&rep='.$repair.'&ov='.$overhaul.'&bo='.$carbody.'&cr='.$credit.'&pr='.$premium.'";
          </script>';
  }

}

//Hacerse Premium
if (isset($_POST['addpremium'])) {
  session_start();
  $id=$_SESSION['idtaller'];
  $taller2->add_premium($id);
  echo '<script type="text/javascript">
        alert("¡Enhorabuena! Ahora eres un taller PREMIUM");
        window.location.href="../view/profiles/profile-workshop.php";
        </script>';
}

//Dejar de ser Premium
if (isset($_POST['droppremium'])) {
  session_start();
  $id=$_SESSION['idtaller'];
  $taller2->drop_premium($id);
  echo '<script type="text/javascript">
        alert("Has dejado de ser PREMIUM.");
        window.location.href="../view/profiles/profile-workshop.php";
        </script>';
}
