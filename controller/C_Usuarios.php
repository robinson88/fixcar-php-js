<?php

session_start();

require "../model/M_Usuarios.php";


$usuario1 = new M_Usuarios;
//$usuario1->get_usuario();

//Formulario de registro
if (isset($_POST['registrar'])) {
  $nombre = $_POST['nombre'];
  $email = $_POST['email'];
  $password = md5($_POST['password']);
  $confirm = md5($_POST['confirma']);

  // se consulta email en base de datos y se compara con el ingresado
  $consulta1 = $usuario1->get_correo($email);
  $count = mysqli_num_rows($consulta1);
  if ($count == 0) {
    if (strlen($_POST['password']) <= 5) {
      echo '<script type="text/javascript">
                    alert("La contraseña debe tener al menos 6 caracteres.");
                    window.location.href="../view/register-options/register-user.php";
                    </script>';
    } else {
      //Se asegura de que la contraseña esté bien introducida
      if ($password == $confirm) {
        $usuario1->crea_usuario($nombre, $password, $email);
        echo '<script type="text/javascript">
                      alert("Registrado con éxito!");
                      window.location.href="../view/login/login-user.php";
                      </script>';
      } else {
        echo '<script type="text/javascript">
                      alert("Las contraseñas no coinciden, vuelve a intentarlo.");
                      window.location.href="../view/register-options/register-user.php";
                      </script>';
      }
    }
  } else {
    echo '<script type="text/javascript">
                    alert("Ya existe un usuario registrado con este mail.");
                    window.location.href="../view/register-options/register-user.php";
                    </script>';
  }
}

//Inicio de sesión
if (isset($_POST['login'])) {
  $email = $_POST['email'];
  $password = md5($_POST['password']);
  $consulta1 = $usuario1->login($email, $password);
  $count = mysqli_num_rows($consulta1);
  if ($count != 0) {
    while ($row = mysqli_fetch_array($consulta1)) {
      if ($email == $row['email'] && $password == $row['pass']) {
        $_SESSION['usuario'] = $row['name'];
        $_SESSION['idusuario'] = $row['iduser'];
        echo '<script type="text/javascript">
                      alert("Logueado con éxito!")
                      window.location.href="../view/home-in/home-in.php";
                      </script>';
      }
    }
  } else {
    echo '<script type="text/javascript">
                  alert("El email o la contraseña son incorrectos. Inténtalo de nuevo.");
                  window.location.href="../view/login/login-user.php";
                  </script>';
  }
}

//update de usuario recoge datos de formulario profile-user.php"
if (isset($_POST['save'])) {
  $id = $_SESSION['idusuario'];
  $consulta = $usuario1->datos_usuarios($id);
  $row = mysqli_fetch_array($consulta);
  $username = $_POST['username'];
  $email = $_POST['email'];
  $direccion = $_POST['user-location'];
  $ciudad = $_POST['city'];
  $telefono = $_POST['telefono'];
  $fecha = $_POST['fecha'];
  $tips = 'jpg';
  $type = array('image/jpeg' => 'jpg');
  $id = $row['iduser'];
  $imagename = $_FILES['image']['name'];
  $ruta = $_FILES['image']['tmp_name'];
  $name = $id . '_userpic.' . $tips;
  $destino = "";
  if (is_uploaded_file($ruta)) {
    $destino = "../view/assets/images/user-pics/" . $name;
    copy($ruta, $destino);
  } else {
    $destino = $row['image'];
  }

  $usuario1->update_usuario($direccion, $ciudad, $telefono, $fecha, $destino, $id);
  echo '<script type="text/javascript">
           alert("Perfil actualizado con éxito.");
           window.location.href="../view/profiles/profile-user.php";
           </script>';
}
