<?php

include '../PHPMailer/PHPMailerAutoload.php';

// metodo para sacar datos según sesión
include "../model/M_Usuarios.php";
$usuario2= new M_Usuarios;

// metodo para sacar datos según sesión
include "../model/M_Vehiculos.php";
$vehiculo2= new M_Vehiculos;

/*
-----------------------------------------------------------------
LA FUNCIÓN QUE ENVIARÍA CORREOS A TODOS LOS USERS ES LA SIGUIENTE
-----------------------------------------------------------------

$hoy = date("d-m-Y");
$consulta=$usuario2->get_usuarios();
foreach ($consulta as $row) {
  $consultav=$vehiculo2->datos_vehiculos($row['iduser']);
  foreach ($consultav as $rowv) {
    if ($rowv['date_itv'] && strtotime($hoy . "+ 5 days") >= strtotime($rowv['date_itv']) && strtotime($hoy) < strtotime($rowv['date_itv'])) {
          sendMail($row['email'], $rowv['vehicle_band'], $rowv['vehicle_model'], $rowv['date_itv'], "ITV", $rowv['itv_note']);
    }
    if ($rowv['wheels_date'] && strtotime($hoy . "+ 5 days") >= strtotime($rowv['wheels_date']) && strtotime($hoy) < strtotime($rowv['wheels_date'])) {
          sendMail($row['email'], $rowv['vehicle_band'], $rowv['vehicle_model'], $rowv['wheels_date'], "fecha para las ruedas", $rowv['wheels_note']);
    }
    if ($rowv['oil_date'] && strtotime($hoy . "+ 5 days") >= strtotime($rowv['oil_date']) && strtotime($hoy) < strtotime($rowv['oil_date'])) {
          sendMail($row['email'], $rowv['vehicle_band'], $rowv['vehicle_model'], $rowv['oil_date'], "fecha para el aceite", $rowv['oil_note']);
    }
    if ($rowv['review_date'] && strtotime($hoy . "+ 5 days") >= strtotime($rowv['review_date']) && strtotime($hoy) < strtotime($rowv['review_date'])) {
          sendMail($row['email'], $rowv['vehicle_band'], $rowv['vehicle_model'], $rowv['review_date'], "fecha de revisión", $rowv['review_note']);
    }
    if ($rowv['vehicle_insurance'] && strtotime($hoy . "+ 5 days") >= strtotime($rowv['vehicle_insurance']) && strtotime($hoy) < strtotime($rowv['vehicle_insurance'])) {
          sendMail($row['email'], $rowv['vehicle_band'], $rowv['vehicle_model'], $rowv['vehicle_insurance'], "seguro", $rowv['vehicle_note']);
    }
  }
}
*/

//Función para enviar mail diario de recordatorio desde 5 días antes hasta que llegue la fecha. Prueba para host gratuito.
$hoy = date("d-m-Y");
$consulta1=$usuario2->datos_usuarios(1);
foreach ($consulta1 as $row) {
  $consultav=$vehiculo2->datos_vehiculos($row['iduser']);
  foreach ($consultav as $rowv) {
    if ($rowv['date_itv'] && strtotime($hoy . "+ 5 days") >= strtotime($rowv['date_itv']) && strtotime($hoy) < strtotime($rowv['date_itv'])) {
          sendMail($row['email'], $rowv['vehicle_band'], $rowv['vehicle_model'], $rowv['date_itv'], "ITV", $rowv['itv_note']);
    }
  }
}



//Función para enviar mails con los recordatorios
function sendMail($email, $marca, $modelo, $fecha, $tipo, $nota) {
  $mail = new PHPMailer;
  $mail->isSMTP();
  $mail->SMTPDebug = 0;
  $mail->Host = 'smtp.gmail.com';
  $mail->Port = 587;
  $mail->SMTPAuth = true;
  $mail->SMTPSecure='tls';
  $mail->Username = 'fixcarproject@gmail.com';
  $mail->Password = 'FixcarCesur12345';
  $mail->setFrom('fixcarproject@gmail.com', 'Fixcar');
  $mail->addAddress($email, 'Usuario');
  $mail->Subject = 'Tienes un recordatorio de Fixcar';
  $mail->Body = '<p>Recordatorio de tu '.$marca.' '.$modelo.'. ¡Tu '.$tipo.' va a caducar! La fecha de caducidad es: '.$fecha.'</p><p>'.$nota.'</p><p>Accede a <a href="https://fixcarcesur.herokuapp.com/">Fixcar</a> para visualizarlo.</p>';
  $mail->IsHTML(true);
  $mail->send();
}

?>
