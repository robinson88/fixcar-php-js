<?php
// para cargar los envios de mail
require "../PHPMailer/PHPMailerAutoload.php";

// recovery para la contraseña del usuario

if (isset($_POST['recovery_pass'])) {

    try {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (isset($_POST['email']) && !empty($_POST['email'])) {
                $email = $_POST['email'];
                //Procederemos a hacer una consulta que buscara el correo del usuario
                $buscarCorreo = "SELECT * from user WHERE email='$email'";
                $buscarCorreowork = "SELECT * from workshop WHERE email='$email'";
                //Conexion con la base
                $conn = new mysqli('eu-cdbr-west-03.cleardb.net', 'bc089039848ae4', '1dd028bb', 'heroku_f8450cd1dd0ceea');
                // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
                $resultado = $conn->query($buscarCorreo);
                $result = $conn->query($buscarCorreowork);

                //Usaremos la funcion mysqli_num_rows en la consulta $resultado,
                //esta funcion nos regresa el numero de filas en el resultado
                $contador = mysqli_num_rows($resultado);
                $cont = mysqli_num_rows($result);
                //SI SI EXISTE una fila, quiere decir QUE SI ESTA EL CORREO EN LA BASE DE DATOS
                if ($contador === 1  || $cont === 1) {
                    senMail($email);
                    echo '<script type="text/javascript">
                     alert("Correo enviado correctamente con éxito, verifique su correo y restablezca la contraseña." );
                     window.location.href="../";
                     </script>';
                } else {
                    echo '<script type="text/javascript">
                     alert("Ingresa un correo válido o Registrate.." );
                     window.location.href="../view/profiles/recovery_pass.php";
                     </script>';
                }
            } else {
                echo '<script type="text/javascript">
                     alert("Ingresa un correo válido o Registrate.." );
                     window.location.href="../view/profiles/recovery_pass.php";
                     </script>';
            }
        } else {
            echo '<script type="text/javascript">
            alert("Ingresa un correo válido o Registrate.." );
            window.location.href="../view/profiles/recovery_pass.php";
            </script>';
        }
    } catch (Exception $e) {
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }
}

// llamado de la funcion mail //
function senMail($email)
{
    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 587;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'tls';
    $mail->Username = 'fixcarproject@gmail.com';
    $mail->Password = 'FixcarCesur12345';
    $mail->setFrom('fixcarproject@gmail.com', 'Fixcar');
    $mail->addAddress($email, 'Usuario');
    $mail->Subject = 'recupera una contraseña de Fixcar';
    // estoy usando local cuando este en el servidor se debe modificar la url.
    $mail->Body = '<p>Recordatorio de tu ' . $email . '</p><p>Accede a <a href="https://fixcarcesur.herokuapp.com/view/profiles/recovery.php">FIXCAR</a> para visualizarlo.</p>';
    $mail->IsHTML(true);
    $mail->send();
}


// reseteo de la contraseña en general para el taller 
if (isset($_POST['resetSubmit'])) {
    try {
        if (isset($_POST['email']) && !empty($_POST['email'])) {
            $pass = md5($_POST['pass']);
            $pass2 = md5($_POST['pass']);
            $email = $_POST['email'];

            //Conexion con la base
            $conn = new mysqli('eu-cdbr-west-03.cleardb.net', 'bc089039848ae4', '1dd028bb', 'heroku_f8450cd1dd0ceea');
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            //Procederemos a hacer una consulta que buscara el correo del usuario
            $buscarCorreo = "SELECT * from user WHERE email='$email'";
            $buscarCorreowork = "SELECT * from workshop WHERE email='$email'";

            $resultado = $conn->query($buscarCorreo);
            $result = $conn->query($buscarCorreowork);

            $sql = "UPDATE user SET pass='$pass'  Where email='$email'";
            $sql2 = "UPDATE workshop SET password='$pass2'  Where email='$email'";

            $resultado1 = $conn->query($sql);
            $result2 = $conn->query($sql2);


            //Usaremos la funcion mysqli_num_rows en la consulta $resultado,
            //esta funcion nos regresa el numero de filas en el resultado
            $contador = mysqli_num_rows($resultado);
            $cont = mysqli_num_rows($result);
            //funcion para guardar en la base de datos de cada usuario o taller.
            $contador1 = mysqli_num_rows($resultado1);
            $cont2 = mysqli_num_rows($result2);
            //SI SI EXISTE una fila, quiere decir QUE SI ESTA EL CORREO EN LA BASE DE DATOS
            if ($contador === 1  || $cont === 1) {
                $contador1;
                $cont2;
                echo '<script type="text/javascript">
                     window.location.href="../";
                     </script>';
            }
        } else
            echo '<script type="text/javascript">
                     alert("Informacion incompleta, vuelve a pedir tu contraseña con tu correo válido." );
                     window.location.href="../";
                     </script>';
    } catch (Exception $e) {
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }
}
