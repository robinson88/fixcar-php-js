<?php
require "api_metodos.php";
$db=Conexionapi::Conectarapi();
Conexionapi::autenticarapi();
$api=new Api;

if (isset($_GET['url'])) {

    /*
    ---------------
    PETICIONES GET
    ---------------
    */

    if ($_SERVER['REQUEST_METHOD']=='GET') {
    $numero=intval(preg_replace('/[^0-9]+/', '', $_GET['url']), 10.);

    switch ($_GET['url']) {

        //Instrucciones
        case "manual":
          ?>
          <header>
            <style>
              * {
                font-family: verdana;
                background: #B5F3D4;
              }

              h3 {
                background: #186F44;
                color: white;
              }

              strong {
                color: #186F44;
              }

              ul {
                list-style: none;
              }


            </style>
          </header>
          <h2>INSTRUCCIONES DE LA API DE FIXCAR</h2>
          <p>Dirección: <strong>https://fixcarcesur.herokuapp.com/api/</strong></p>
          <p>La API tiene una autorización básica, los datos son los siguientes: </p>
          <hr>
          <p>Nombre de usuario: Cesur. Password: FixCar.</p>
          <hr>
          <p>Introducir tras la dirección lo siguiente, según la petición a realizar, sustituyendo la X por el número de id del dato a recibir/modificar/eliminar:</p>
          <h3>PETICIONES GET</h3>
            <ul>
              <li><h4>Usuarios</h4></li>
              <li><strong>usuario</strong> todos los usuarios.</li>
              <li><strong>idusuarioX</strong> usuario en particular.</li>
              <li><strong>idmailusuario</strong> devuelve la id del usuario según el mail (introducir email en parámetros).</li><hr>
              <li><h4>Vehículos</h4></li>
              <li><strong>vehiculo</strong> todos lov vehículos.</li>
              <li><strong>vehiculoidusuarioX</strong> vehículo en particular según el id del usuario que lo posee.</li>
              <li><strong>idvehiculoX</strong> vehículo en particular según su id.</li><hr>
              <li><h4>Talleres</h4></li>
              <li><strong>taller</strong> todos los talleres.</li>
              <li><strong>dirtaller</strong> todos los nombres de talleres y sus direcciones.</li>
              <li><strong>idtallerX</strong> taller en particular.</li>
              <li><strong>idmailtaller</strong> devuelve la id del taller según el mail (introducir email en parámetros).</li><hr>
              <li><strong>workshoptype</strong> todos los tipos de taller.</li><hr>
              <li><strong>idtworkshoptypeX</strong> devuelve los tipos de un taller específico X.</li><hr>
              <li><strong>workshoptypesearch</strong> introducir los siguientes parámetros (mechanics, repairs, electricity, bodywork, review, creditcard, premium). Todos son 1 o 0, excepto premium, que para que devuelva los resultados, debe ser "Premium".</li><hr>
              <li><h4>Documentos</h4></li>
              <li><strong>document</strong> todos los documentos.</li>
              <li><strong>documentidusuarioX</strong> devuelve todos los documentos de un usuario concreto</li><hr>
              <li><h4>Reseñas</h4></li>
              <li><strong>comment</strong> todas las reseñas.</li>
              <li><strong>commentidworkshopX</strong> todas las reseñas de un taller determinado.</li>
              <li><h4>Ranking</h4></li>
              <li><strong>ranking</strong> todos los rankings.</li>
              <li><strong>rankingidworkshopX</strong> todos los ranking de un taller determinado.</li>
              <li><strong>avgrankingX</strong> media de los ranking de un taller.</li>
              <li><h4>Favoritos</h4></li>
              <li><strong>fav</strong> todos los favs.</li>
              <li><strong>faviduserX</strong> todos los favs de un user determinado.</li>
              <li><strong>findfav</strong> devuelve si un taller es favorito o no para un usuario. Introducir como parámetros iduser,idworkshop.</li>
            </ul>
          <br>
          <br>
          <h3>PETICIONES POST</h3>
            <ul>
              <li><h4>Usuarios</h4></li>
              <li><strong>usuariopost</strong> introducir los siguientes parámetros (nombre, direccion, localidad, telefono, email, fecha, imagen).</li>
              <li><strong>userpicX</strong> introducir el id del usuario. Meter la imagen por el body, en form-data.</li><hr>
              <li><h4>Vehículos</h4></li>
              <li><strong>vehiculopost</strong> introducir los siguientes parámetros (idusuario, km_vehiculo, modelo, motor, marca, matricula).</li>
              <li><strong>vehiclepicX</strong> introducir el id del vehículo al que asociar una imagen. Meter la imagen por el body, en form-data.</li><hr>
              <li><h4>Talleres</h4></li>
              <li><strong>tallerpost</strong> introducir los siguientes parámetros (nombre, cif, direccion, email, tipo_taller, descripcion, imagen, video, localidad, telefono).</li>
              <li><strong>workshoppicX</strong> introducir el id del taller al cual subir una foto. Meter la imagen por el body, en form-data.</li><hr>
              <li><h4>Documentos</h4></li>
              <li><strong>documentpost</strong> introducir los siguientes parámetros (type_document, notes, iduser, idvehicle).</li>
              <li><strong>documentpicX</strong> introducir el id del documento donde subir una imagen. Meter la imagen por el body, en form-data.</li><hr>
              <li><h4>Reseñas</h4></li>
              <li><strong>commentpost</strong> introducir los siguientes parámetros (commentary, idusers, idworkshops).</li>
              <li><strong>replypostX</strong> el id del comentario al que se va a responder es X. introducir los siguientes parámetros (commentary, idusers, idworkshops).</li><hr>
              <li><h4>Ranking</h4></li>
              <li><strong>rankingpost</strong> introducir los siguientes parámetros (ranking, id_users, id_workshops).</li>
              <li><h4>Favoritos</h4></li>
              <li><strong>favpost</strong> introducir los siguientes parámetros (iduser, idworkshop).</li>
            </ul>
          <br>
          <br>
          <h3>PETICIONES PUT</h3>
            <ul>
              <li><h4>Usuarios</h4></li>
              <li><strong>usuarioputX</strong> introducir los siguientes parámetros (idusuario, nombre, direccion, localidad, telefono, email, estado, fecha, imagen).</li><hr>
              <li><h4>Vehículos</h4></li>
              <li><strong>vehiculoputX</strong> introducir los siguientes parámetros (km_vehiculo, modelo, motor, marca, matricula). </li>
              <li><strong>reminderX</strong> introducir los siguientes parámetros (itv_fecha, itv_note, fecha_ruedas, wheels_note, fecha_aceite, oil_note, fecha_revision, review_note, seguro, vehicle_note). </li><hr>
              <li><h4>Talleres</h4></li>
              <li><strong>tallerputX</strong> introducir los siguientes parámetros (idtaller, nombre, cif, direccion, email, descripcion, imagen, video, localidad, estado, telefono).</li><hr>
              <li><h4>Reseñas</h4></li>
              <li><strong>commentputX</strong> el id del comentario a editar es X. Introducir los siguientes parámetros (commentary).</li><hr>
              <li><h4>Ranking</h4></li>
              <li><strong>rankingput</strong> Introducir los siguientes parámetros (ranking, id_users, id_workshops).</li><hr>
            </ul>
          <br>
          <br>
          <h3>PETICIONES DELETE</h3>
            <ul>
              <li><h4>Usuarios</h4></li>
              <li><strong>usuariodelX</strong></li><hr>
              <li><h4>Vehículos</h4></li>
              <li><strong>vehiculodelX</strong></li><hr>
              <li><h4>Talleres</h4></li>
              <li><strong>tallerdelX</strong></li><hr>
              <li><h4>Documentos</h4></li>
              <li><strong>documentdelX</strong></li><hr>
              <li><h4>Reseñas</h4></li>
              <li><strong>commentdelX</strong></li><hr>
              <li><h4>Ranking</h4></li>
              <li><strong>rankingdelX</strong></li><hr>
              <li><h4>Favoritos</h4></li>
              <li><strong>favdel</strong> introducir iduser e idworkshop.</li><hr>
            </ul>
          <?php

        break;

        // PETICIONES DE USUARIOS
        //-----------------------
        case "usuario":
          $row = $api-> get_usuariosapi();
          echo json_encode($row);
          break;

        case "idusuario".$numero:
          $row = $api-> get_usuariosapid($numero);
            foreach ($row as $row1) {
                print_r(json_encode($row1));
            }
          break;

        case "idmailusuario":
         $row = $api-> idmailuser($_REQUEST['email']);
         foreach ($row as $index => $value){
             print_r(json_encode($value['iduser']));
         }
         break;

         //PETICIONES DE VEHÍCULOS
         //-----------------------
        case 'vehiculo':
          $row2=$api->get_vehiculosapi();
          echo json_encode($row2);
          break;

        case 'vehiculoidusuario'.$numero:
          $row2=$api->get_vehiculosapidusuario($numero);
           echo json_encode($row2);
          break;

        case 'idvehiculo'.$numero:
          $row = $api-> get_vehiculosapid($numero);
            foreach ($row as $row1) {
                print_r(json_encode($row1));
            }
          break;

        //PETICIONES DE TALLERES
        //-----------------------
        case 'taller':
          $row2=$api->get_tallerapi();
          echo json_encode($row2);

          break;

        case 'dirtaller':
          $row2=$api->direcciontaller();
          echo json_encode($row2);

        break;

        case 'idtaller'.$numero:
           $row2=$api->get_tallerapid($numero);
            foreach ($row2 as $row3) {
                print_r(json_encode($row3));
            }
           break;

        case "idmailtaller":
           $row = $api-> idmailtaller($_REQUEST['email']);
            foreach ($row as $index => $value){
               print_r(json_encode($value['idworkshop']));
            }
           break;

         case 'workshoptype':
           $row2=$api->get_workshoptypeapi();
           echo json_encode($row2);

           break;

         case 'idtworkshoptype'.$numero:
            $row2=$api->get_workshoptypeapid($numero);
             foreach ($row2 as $row3) {
                 print_r(json_encode($row3));
             }
            break;

         case 'workshoptypesearch':
           $row2=$api->get_workshoptypesearch($_REQUEST['mechanics'], $_REQUEST['repairs'], $_REQUEST['electricity'], $_REQUEST['bodywork'], $_REQUEST['review'], $_REQUEST['creditcard'], $_REQUEST['premium']);
           echo json_encode($row2);

           break;



        //PETICIONES DE DOCUMENTOS
        //------------------------
        case 'document':
          $row2=$api->get_documentapi();
                echo json_encode($row2);
           break;

        case 'documentidusuario'.$numero:
          $row2=$api->get_documentid($numero);
               echo json_encode($row2);
          break;

        //PETICIONES DE DOCUMENTOS
        //------------------------
        case 'comment':
          $row2=$api->get_commentapi();
                echo json_encode($row2);
           break;

        case 'commentidworkshop'.$numero:
          $row2=$api->get_commentworkshopid($numero);
               echo json_encode($row2);
          break;

        //PETICIONES DE RANKING
        //------------------------
        case 'ranking':
          $row2=$api->get_ratingapi();
                echo json_encode($row2);
           break;

        case 'rankingidworkshop'.$numero:
          $row2=$api->get_ratingworkshopid($numero);
               echo json_encode($row2);
          break;

        case 'avgranking'.$numero:
          $row2=$api->get_avgrating($numero);
          foreach ($row2 as $index => $value) {
              print_r(json_encode($value['AVG(ranking)']));
          }

          break;

        //PETICIONES DE FAVORITOS
        //------------------------
        case 'fav':
          $row2=$api->get_favapi();
                echo json_encode($row2);
           break;

         case 'faviduser'.$numero:
           $row2=$api->get_favuserid($numero);
                echo json_encode($row2);
           break;

         case 'findfav':
           $row2 = $api->find_fav($_REQUEST['iduser'], $_REQUEST['idworkshop']);
                echo json_encode($row2);
           break;

        default:

          break;
    }

    /*
    ---------------
    PETICIONES POST
    ---------------
    */

    } elseif ($_SERVER['REQUEST_METHOD'] =='POST') {
      $numero=intval(preg_replace('/[^0-9]+/', '', $_GET['url']), 10.);


        switch ($_GET['url']) {


          //PETICIONES DE USUARIOS
          //------------------------
          case 'usuariopost':
            $consulta2 = $api->add_usuario($_REQUEST['nombre'], $_REQUEST['password'], $_REQUEST['direccion'], $_REQUEST['localidad'],
                                           $_REQUEST['telefono'], $_REQUEST['email'], $_REQUEST['fecha'], $_REQUEST['imagen'],);
            echo json_encode($consulta2);
            break;

          case 'userpic'.$numero:

              $response = array();
              if ($_FILES['image']['error'] === UPLOAD_ERR_OK) {

                  $file = $_FILES['image']['tmp_name'];

                  if ($api->saveFileUser($file, getFileExtension($_FILES['image']['name']), $numero)) {
                      $response['error'] = false;
                      $response['message'] = 'Archivo subido correctamente.';
                  }

              } else {
                  $response['error'] = true;
                  $response['message'] = 'Parámetros no disponibles.';
                  echo json_encode($consulta2);
              }

              break;

          //PETICIONES DE VEHÍCULOS
          //------------------------
          case 'vehiculopost':
            $consulta2 = $api->add_vehiculo($_REQUEST['idusuario'], $_REQUEST['km_vehiculo'], $_REQUEST['modelo'], $_REQUEST['motor'],
                                            $_REQUEST['marca'], $_REQUEST['matricula']);
            foreach ($consulta2 as $index => $value) {
                print_r(json_encode($value['idvehicle']));
            }
            break;

          case 'vehiclepic'.$numero:

              $response = array();
              if ($_FILES['image']['error'] === UPLOAD_ERR_OK) {

                  $file = $_FILES['image']['tmp_name'];

                  if ($api->saveFileVehicle($file, getFileExtension($_FILES['image']['name']), $numero)) {
                      $response['error'] = false;
                      $response['message'] = 'Archivo subido correctamente.';
                  }

              } else {
                  $response['error'] = true;
                  $response['message'] = 'Parámetros no disponibles.';
                  echo json_encode($consulta2);
              }

              break;

          //PETICIONES DE TALLERES
          //------------------------
          case 'tallerpost':
            $consulta2 = $api->add_taller($_REQUEST['nombre'], $_REQUEST['cif'], $_REQUEST['direccion'],
                                          $_REQUEST['email'], $_REQUEST['password'], $_REQUEST['tipo_taller'], $_REQUEST['descripcion'], $_REQUEST['imagen'],
                                          $_REQUEST['video'], $_REQUEST['localidad'], $_REQUEST['telefono']);
            echo json_encode($consulta2);
            break;

          case 'workshoppic'.$numero:

              $response = array();
              if ($_FILES['image']['error'] === UPLOAD_ERR_OK) {

                  $file = $_FILES['image']['tmp_name'];

                  if ($api->saveFileWorkShop($file, getFileExtension($_FILES['image']['name']), $numero)) {
                      $response['error'] = false;
                      $response['message'] = 'Archivo subido correctamente.';
                  }

              } else {
                  $response['error'] = true;
                  $response['message'] = 'Parámetros no disponibles.';
                  echo json_encode($consulta2);
              }

              break;

          //PETICIONES DE DOCUMENTOS
          //------------------------
          case 'documentpost':
            $consulta2 = $api->add_document($_REQUEST['type_document'], $_REQUEST['notes'], $_REQUEST['iduser'], $_REQUEST['idvehicle']);
            foreach ($consulta2 as $index => $value) {
                print_r(json_encode($value['iddocument']));
            }
            break;

          case 'documentpic'.$numero:

              $response = array();
              if ($_FILES['image']['error'] === UPLOAD_ERR_OK) {

                  $file = $_FILES['image']['tmp_name'];

                  if ($api->saveDocumentUser($file, getFileExtension($_FILES['image']['name']), $numero)) {
                      $response['error'] = false;
                      $response['message'] = 'Archivo subido correctamente.';
                  }

              } else {
                  $response['error'] = true;
                  $response['message'] = 'Parámetros no disponibles.';
                  echo json_encode($consulta2);
              }

              break;

          //PETICIONES DE COMENTARIOS
          //------------------------
          case 'commentpost':
            $consulta2 = $api->add_comment($_REQUEST['commentary'], $_REQUEST['idusers'], $_REQUEST['idworkshops']);
            foreach ($consulta2 as $index => $value) {
                print_r(json_encode($value['idcommentary']));
            }
            break;

           case 'replypost'.$numero:
             $consulta2 = $api->add_reply($_REQUEST['commentary'], $_REQUEST['idusers'], $_REQUEST['idworkshops'], $numero);
             foreach ($consulta2 as $index => $value) {
                 print_r(json_encode($value['idcommentary']));
             }
            break;

          //PETICIONES DE RANKING
          //------------------------
          case 'rankingpost':
            $consulta2 = $api->add_ranking($_REQUEST['ranking'], $_REQUEST['id_users'], $_REQUEST['id_workshops']);
            foreach ($consulta2 as $index => $value) {
                print_r(json_encode($value['idranking']));
            }
            break;

          //PETICIONES DE FAVORITOS
          //------------------------
          case 'favpost':
            $consulta2 = $api->add_fav($_REQUEST['iduser'], $_REQUEST['idworkshop']);
            foreach ($consulta2 as $index => $value) {
                print_r(json_encode($value['idfavorites']));
            }
            break;

           default:

            break;

      }

      /*
      ---------------
      PETICIONES PUT
      ---------------
      */

    } elseif ($_SERVER['REQUEST_METHOD'] == 'PUT') {
      $numero=intval(preg_replace('/[^0-9]+/', '', $_GET['url']), 10.);

        switch ($_GET['url']) {

          case 'usuarioput'.$numero:
            $consulta1 = $api->update_usuario($numero, $_REQUEST['nombre'], $_REQUEST['direccion'], $_REQUEST['localidad'],
                                              $_REQUEST['telefono'], $_REQUEST['email'], $_REQUEST['estado'], $_REQUEST['fecha'], $_REQUEST['imagen']);
            echo json_encode($consulta1);
            break;

          case 'vehiculoput'.$numero:
            $consulta1 = $api->update_vehiculo($numero, $_REQUEST['km_vehiculo'], $_REQUEST['modelo'], $_REQUEST['motor'], $_REQUEST['marca'], $_REQUEST['matricula']);
            echo json_encode($consulta1);
            break;

          case 'reminder'.$numero:
            $consulta1 = $api->vehicle_reminder($numero, $_REQUEST['itv_fecha'], $_REQUEST['itv_note'], $_REQUEST['fecha_ruedas'], $_REQUEST['wheels_note'], $_REQUEST['fecha_aceite'],
                                                $_REQUEST['oil_note'], $_REQUEST['fecha_revision'], $_REQUEST['review_note'], $_REQUEST['seguro'], $_REQUEST['vehicle_note']);

            echo json_encode($consulta1);
            break;

          case 'tallerput'.$numero:
            $consulta1 = $api->update_taller($numero, $_REQUEST['nombre'], $_REQUEST['cif'], $_REQUEST['direccion'],
                                             $_REQUEST['email'], $_REQUEST['descripcion'], $_REQUEST['imagen'],
                                             $_REQUEST['video'], $_REQUEST['localidad'], $_REQUEST['estado'], $_REQUEST['telefono']);
            echo json_encode($consulta1);
            break;

          case 'commentput'.$numero:
            $consulta1 = $api->update_comment($numero, $_REQUEST['commentary']);
            echo json_encode($consulta1);
            break;

          case 'rankingput':
            $consulta1 = $api->update_ranking($_REQUEST['ranking'], $_REQUEST['id_users'], $_REQUEST['id_workshops']);
            echo json_encode($consulta1);
            break;

          default:

            break;

      }

      /*
      -----------------
      PETICIONES DELETE
      -----------------
      */

      } elseif ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
        $numero=intval(preg_replace('/[^0-9]+/', '', $_GET['url']), 10.);

          switch ($_GET['url']) {

            case "usuariodel".$numero:
              $consulta3 = $api->delete_usuario($numero);
              echo json_encode($consulta3);
              break;

            case "vehiculodel".$numero:
              $consulta3 = $api->delete_vehiculo($numero);
              echo json_encode($consulta3);
              break;

            case "tallerdel".$numero:
              $consulta3 = $api->delete_taller($numero);
              echo json_encode($consulta3);
              break;

            case "documentdel".$numero:
              $consulta3 = $api->delete_document($numero);
              echo json_encode($consulta3);
              break;

            case "commentdel".$numero:
              $consulta3 = $api->delete_comment($numero);
              echo json_encode($consulta3);
              break;

            case "rankingdel".$numero:
              $consulta3 = $api->delete_ranking($numero);
              echo json_encode($consulta3);
              break;

            case "favdel":
              $consulta3 = $api->delete_fav($_REQUEST['iduser'], $_REQUEST['idworkshop']);
              echo json_encode($consulta3);
              break;

            default:

              break;

          }

      } else {

            http_response_code(405);

      }

}


function getFileExtension($file)
{
    $path_parts = pathinfo($file);
    return $path_parts['extension'];
}




?>
