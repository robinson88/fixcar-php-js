<?php
require "Conexionapi.php";
class Api extends Conexionapi{

    //Constructor
    public function __construct(){
      //Conexión del API a la BBDD
      $this->conexion=Conexionapi::Conectarapi();
      $this->usuarios=array();
    }

    /*
    ---------------
    PETICIONES GET
    ---------------
    */

    //Devolver todos los usuarios
    public function get_usuariosapi() {
       $sql = "SELECT * FROM user";
       $resultado = $this->ejecutarConsulta($sql);
       if ($resultado != null) {

            return $resultado;

       }else{

            return null;

       }

    }

    //Devolver todos los vehículos
    public function get_vehiculosapi() {
         $sql = "SELECT * FROM vehicle";
         $resultado = $this->ejecutarConsulta($sql);
         if ($resultado != null) {

              return $resultado;

         }else{

              return null;

         }

    }

    //Devolver todos los talleres
    public function get_tallerapi() {
           $sql = "SELECT * FROM workshop";
           $resultado = $this->ejecutarConsulta($sql);
           if ($resultado != null) {

                return $resultado;

           }else{

                return null;

           }

    }

     //Devolver usuario según su id
     public function get_usuariosapid($id){
         $consulta=$this->conexion->query("SELECT * FROM user WHERE iduser=$id");
         return $consulta;
     }

     //Devolver vehículos según la id del usuario
     public function get_vehiculosapidusuario($id) {
         $sql="SELECT * FROM vehicle WHERE uIdUser=$id";
         $resultado = $this->ejecutarConsulta($sql);
         if ($resultado != null) {

              return $resultado;

         }else{

              return null;

         }
     }

     //Devolver vehículo según la id del vehículo
     public function get_vehiculosapid($id) {
       $consulta=$this->conexion->query("SELECT * FROM vehicle WHERE idvehicle=$id");
       return $consulta;
     }

     //Devolver taller según su id
     public function get_tallerapid($id) {
         $consulta=$this->conexion->query("SELECT * FROM workshop WHERE idworkshop=$id");
         return $consulta;
     }

     //Devolver id del usuario según email
     public function idmailuser($email) {
         $sql="SELECT iduser FROM user WHERE email='$email'";
         $resultado = $this->ejecutarConsulta($sql);
         if ($resultado != null) {
              return $resultado;
         }else{
              return null;
         }
       }

     //Devolver id del taller según email
     public function idmailtaller($email) {
         $sql="SELECT idworkshop FROM workshop WHERE email='$email'";
         $resultado = $this->ejecutarConsulta($sql);
         if ($resultado != null) {
              return $resultado;
         }else{
              return null;
         }
     }

     //Devolver todos los documentos
     public function get_documentapi() {
          $sql = "SELECT * FROM document";
          $resultado = $this->ejecutarConsulta($sql);
          if ($resultado != null) {

               return $resultado;

          }else{

               return null;

          }

     }

     //Devolver todos los documentos de un usuario
     public function get_documentid($id) {
            $sql = "SELECT * FROM document WHERE iduser='$id'";
            $resultado = $this->ejecutarConsulta($sql);
            if ($resultado != null) {

                 return $resultado;

            }else{

                 return null;

            }

     }

     //Devolver todos los comentarios
     public function get_commentapi() {
          $sql = "SELECT * FROM commentary";
          $resultado = $this->ejecutarConsulta($sql);
          if ($resultado != null) {

               return $resultado;

          }else{

               return null;

          }

     }

     //Devolver todos los documentos de un taller
     public function get_commentworkshopid($id) {
            $sql = "SELECT * FROM commentary WHERE idworkshops='$id'";
            $resultado = $this->ejecutarConsulta($sql);
            if ($resultado != null) {

                 return $resultado;

            }else{

                 return null;

            }

      }

      //Devolver todos los rating
      public function get_ratingapi() {
           $sql = "SELECT * FROM ranking";
           $resultado = $this->ejecutarConsulta($sql);
           if ($resultado != null) {

                return $resultado;

           }else{

                return null;

           }

      }

      //Devolver todos los ratkings de un taller
      public function get_ratingworkshopid($id) {
             $sql = "SELECT * FROM ranking WHERE id_workshops='$id'";
             $resultado = $this->ejecutarConsulta($sql);
             if ($resultado != null) {

                  return $resultado;

             }else{

                  return null;

             }

      }

       //Media de los rating de un taller
      public function get_avgrating($id) {
            $sql = "SELECT AVG(ranking) FROM ranking WHERE id_workshops='$id'";
            $resultado = $this->ejecutarConsulta($sql);
            if ($resultado != null) {
                       return $resultado;
                  }else{

                       return null;

                  }

      }

      //Devolver todos los favoritos
      public function get_favapi() {
           $sql = "SELECT * FROM favorites";
           $resultado = $this->ejecutarConsulta($sql);
           if ($resultado != null) {

                return $resultado;

           }else{

                return null;

           }

      }

      //Devolver todos los favoritos de un user
      public function get_favuserid($id) {
             $sql = "SELECT * FROM favorites WHERE idsuser='$id'";
             $resultado = $this->ejecutarConsulta($sql);
             if ($resultado != null) {

                  return $resultado;

             }else{

                  return null;

             }

      }

      //Comprobación favorito
      public function find_fav($iduser, $idworkshop) {
             $sql = $this->conexion->query("SELECT * FROM favorites WHERE idsuser='$iduser' AND idsworkshop='$idworkshop'");
             $count = mysqli_num_rows($sql);
             if ($count == 0) {
               return false;
             }else{
               return true;
             }
      }

      //Devolver todos los tipos de talleres
      public function get_workshoptypeapi() {
           $sql = "SELECT * FROM workshop_type";
           $resultado = $this->ejecutarConsulta($sql);
           if ($resultado != null) {

                return $resultado;

           }else{

                return null;

           }

      }

      //Devolver los tipos de un taller
      public function get_workshoptypeapid($id) {
             $sql = "SELECT * FROM workshop_type WHERE workshop_id='$id'";
             $resultado = $this->ejecutarConsulta($sql);
             if ($resultado != null) {
                  return $resultado;

             }else{

                  return null;

             }

      }

      //Búsqueda por filtros
      public function get_workshoptypesearch($mechanics, $repairs, $electricity, $bodywork, $review, $creditcard, $premium) {
          $arraytypes=array();
          if ($mechanics == 1) {
              array_push($arraytypes, 1);
          }
          if ($repairs == 1) {
              array_push($arraytypes, 1);
          }
          if ($electricity == 1) {
              array_push($arraytypes, 1);
          }
          if ($bodywork == 1) {
              array_push($arraytypes, 1);
          }
          if ($review == 1) {
              array_push($arraytypes, 1);
          }
          if ($creditcard == 1) {
              array_push($arraytypes, 1);
          }
          if ($premium == 1) {
              array_push($arraytypes, 1);
          }
          $length = count($arraytypes);

          if ($mechanics != 1 && $mechanics != 1 && $repairs != 1 && $electricity != 1 && $bodywork != 1 && $review != 1 && $creditcard != 1 && $premium != "Premium") {
           $sql= "SELECT DISTINCT * FROM workshop";
           $resultado = $this->ejecutarConsulta($sql);
           if ($resultado != null) {
                return $resultado;
           }else{
                return null;
           }
          }else{

           $sql= "SELECT DISTINCT * FROM workshop_type, workshop WHERE";
           $i=0;
           if ($mechanics == 1) {
               $i++;
               $sql.=" workshop_type.mechanics='$mechanics'";
               if ($i < $length) {
               $sql.=" AND";
               }
           }
           if ($repairs == 1) {
               $i++;
               $sql.=" workshop_type.repairs='$repairs'";
               if ($i < $length) {
               $sql.=" AND";
               }
           }
           if ($electricity == 1) {
               $i++;
               $sql.=" workshop_type.electricity='$electricity'";
               if ($i < $length) {
               $sql.=" AND";
               }
           }
           if ($bodywork == 1) {
               $i++;
               $sql.=" workshop_type.bodywork='$bodywork'";
               if ($i < $length) {
               $sql.=" AND";
               }
           }
           if ($review == 1) {
               $i++;
               $sql.=" workshop_type.review='$review'";
               if ($i < $length) {
               $sql.=" AND";
               }
           }
           if ($creditcard == 1) {
               $i++;
               $sql.=" workshop_type.creditcard='$creditcard'";
               if ($i < $length) {
               $sql.=" AND";
               }
           }
           if ($premium == "Premium") {
               $i++;
               $sql.=" workshop.state='$premium'";
               if ($i < $length) {
               $sql.=" AND";
               }
           }
           $sql.=" AND workshop.idworkshop=workshop_type.workshop_id";
           $resultado = $this->ejecutarConsulta($sql);
           if ($resultado != null) {
                return $resultado;
           }else{
                return null;
           }
         }
      }



    /*
    ---------------------------------------------------------------
    devuelve nombre de talleres y direcciones
    ---------------------------------------------------------------
    */
       public function direcciontaller() {
         $sql="SELECT name,adress FROM workshop";
         $resultado = $this->ejecutarConsulta($sql);
         if ($resultado != null) {
              return $resultado;

         }else{

              return null;

         }
     }

     /*
     ---------------
     PETICIONES POST
     ---------------
     */

     //Añadir usuarios
     public function add_usuario($nombre, $password, $direccion, $localidad, $telefono, $email, $fecha, $imagen) {

          $consulta = "INSERT INTO user (name, pass, adress, location, phone, email, state, birth_date, image) VALUES ('$nombre', '$password', '$direccion', '$localidad', '$telefono', '$email', '1', '$fecha', '$imagen')";
          $add = $this->ejecutarPeticion($consulta);
          return $add;

     }

     //Añadir vehículos
     public function add_vehiculo($idusuario, $km_vehiculo, $modelo, $motor, $marca, $matricula) {

          $consulta = "INSERT INTO vehicle (uIdUser, vehicle_km, vehicle_model, vehicle_engine, vehicle_band, vehicle_registration) VALUES ('$idusuario', '$km_vehiculo', '$modelo', '$motor', '$marca', '$matricula')";
          $add = $this->ejecutarPeticion($consulta);
          $sql = "SELECT idvehicle FROM vehicle WHERE uIdUser='$idusuario' ORDER BY idvehicle DESC LIMIT 1";
          $resultado = $this->ejecutarConsulta($sql);
          if ($resultado != null) {
               return $resultado;
          }else{
               return null;
          }
     }

     //Añadir talleres
     public function add_taller($nombre, $cif, $direccion, $email, $password, $tipo_taller, $descripcion, $imagen, $video, $localidad, $telefono) {

          $consulta = "INSERT INTO workshop (name, cif, adress, email, password, workshop_type, description, image, video, location, state, phone) VALUES ('$nombre', '$cif', '$direccion', '$email', '$password', '$tipo_taller', '$descripcion', '$imagen', '$video', '$localidad', '1', '$telefono')";
          $add = $this->ejecutarPeticion($consulta);
          return $add;

     }

     //Añadir documentos
     public function add_document($type_document, $notes, $iduser, $idvehicle) {

          $consulta = "INSERT INTO document (type_document, notes, iduser, idvehicle) VALUES ('$type_document', '$notes', '$iduser', '$idvehicle')";
          $add = $this->ejecutarPeticion($consulta);
          $sql = "SELECT iddocument FROM document WHERE iduser='$iduser' ORDER BY iddocument DESC LIMIT 1";
          $resultado = $this->ejecutarConsulta($sql);
          if ($resultado != null) {
               return $resultado;
          }else{
               return null;
          }
     }

     //Añadir comentarios
     public function add_comment($commentary, $idusers, $idworkshops) {

          $consulta = "INSERT INTO commentary (commentary, idusers, idworkshops, create_date) VALUES ('$commentary', '$idusers', '$idworkshops', NOW())";
          $add = $this->ejecutarPeticion($consulta);
          $sql = "SELECT idcommentary FROM commentary WHERE idusers='$idusers' ORDER BY idcommentary DESC LIMIT 1";
          $resultado = $this->ejecutarConsulta($sql);
          if ($resultado != null) {
               return $resultado;
          }else{
               return null;
          }
     }

     //Añadir respuesta
     public function add_reply($commentary, $idusers, $idworkshops, $numero) {

          $consulta = "INSERT INTO commentary (commentary, idusers, idworkshops, response, create_date) VALUES ('$commentary', '$idusers', '$idworkshops', '$numero', NOW())";
          $add = $this->ejecutarPeticion($consulta);
          $sql = "SELECT idcommentary FROM commentary WHERE idusers='$idusers' ORDER BY idcommentary DESC LIMIT 1";
          $resultado = $this->ejecutarConsulta($sql);
          if ($resultado != null) {
               return $resultado;
          }else{
               return null;
          }
     }

     //Añadir rating
     public function add_ranking($ranking, $id_users, $id_workshops) {

          $consulta = "INSERT INTO ranking (ranking, id_users, id_workshops) VALUES ('$ranking', '$id_users', '$id_workshops')";
          $add = $this->ejecutarPeticion($consulta);
          $sql = "SELECT idranking FROM ranking WHERE id_users='$id_users' ORDER BY idranking DESC LIMIT 1";
          $resultado = $this->ejecutarConsulta($sql);
          if ($resultado != null) {
               return $resultado;
          }else{
               return null;
          }
     }

     //Añadir fav
     public function add_fav($iduser, $idworkshop) {

          $consulta = "INSERT INTO favorites (idsuser, idsworkshop) VALUES ('$iduser', '$idworkshop')";
          $add = $this->ejecutarPeticion($consulta);
          $sql = "SELECT idfavorites FROM favorites WHERE idsuser='$iduser' ORDER BY idfavorites DESC LIMIT 1";
          $resultado = $this->ejecutarConsulta($sql);
          if ($resultado != null) {
               return $resultado;
          }else{
               return null;
          }
     }

     /*
     MÉTODOS PARA SUBIR IMÁGENES
     */

     //Método para guardar la imagen de un vehículo y subirla a la BBDD
     public function saveFileVehicle($file, $extension, $numero)
     {
         $name = $numero.'_'.round(microtime(true) * 1000) . '.' . $extension;
         $filedest = '../view/assets/images/vehicle-pics/'.$name;
         move_uploaded_file($file, $filedest);
         $consulta = "UPDATE vehicle SET image='$filedest' WHERE idvehicle='$numero'";
         $this->conexion->query($consulta);
         return $consulta;
     }

     //Añadir imagen al taller
     public function saveFileWorkShop($file, $extension, $numero)
     {
         $name = $numero.'_'.round(microtime(true) * 1000) . '.' . $extension;
         $filedest = '../view/assets/images/workshop-pics/'.$name;
         move_uploaded_file($file, $filedest);
         $consulta = "UPDATE workshop SET image='$filedest' WHERE idworkshop='$numero'";
         $this->conexion->query($consulta);
         return $consulta;
     }

     //Añadir imagen al usuario
     public function saveFileUser($file, $extension, $numero)
     {
         $name = $numero.'_userpic.' . $extension;
         $filedest = '../view/assets/images/user-pics/'.$name;
         move_uploaded_file($file, $filedest);
         $consulta = "UPDATE user SET image='$filedest' WHERE iduser='$numero'";
         $this->conexion->query($consulta);
         return $consulta;
     }

     //Añadir documento por parte de un usuario
     public function saveDocumentUser($file, $extension, $numero)
     {
         $name = $numero.'_doc_'.round(microtime(true) * 1000) . '.' . $extension;
         $documentpath = '../view/assets/images/document-pics/'.$numero;
         if (!file_exists($documentpath)) {
             mkdir($documentpath, 0777, true);
         }
         $filedest = $documentpath.'/'.$name;
         move_uploaded_file($file, $filedest);
         $consulta = "UPDATE document SET documents='$filedest' WHERE iddocument='$numero'";
         $this->conexion->query($consulta);
         return $consulta;
     }




     /*
     ---------------
     PETICIONES PUT
     ---------------
     */

     //Método para actualizazr datos de usuarios
     public function update_usuario($id, $nombre, $direccion, $localidad, $telefono, $email, $estado, $fecha, $imagen) {
           $consulta = "UPDATE user SET name='$nombre', adress='$direccion', location='$localidad', phone='$telefono', email='$email', state='$estado', birth_date='$fecha', image='$imagen' WHERE iduser='$id'";
           $this->conexion->query($consulta);
           return $consulta;
         //idusuario='$idusuario',

      }

      //Método para actualizazr datos de vehiculos
      public function update_vehiculo($id, $km_vehiculo, $modelo, $motor, $marca, $matricula) {
            $consulta = "UPDATE vehicle SET vehicle_km='$km_vehiculo', vehicle_model='$modelo', vehicle_engine='$motor', vehicle_band='$marca', vehicle_registration='$matricula' WHERE idvehicle='$id'";
            $this->conexion->query($consulta);
            return $consulta;

       }

       //Método para añadir recordatorios
       public function vehicle_reminder($id, $itv_fecha, $itv_note, $fecha_ruedas, $wheels_note, $fecha_aceite, $oil_note, $fecha_revision, $review_note, $seguro, $vehicle_note) {
            $consulta = "UPDATE vehicle SET date_itv='$itv_fecha', itv_note='$itv_note', wheels_date='$fecha_ruedas', wheels_note='$wheels_note', oil_date='$fecha_aceite', oil_note='$oil_note', review_date='$fecha_revision', review_note='$review_note', vehicle_insurance='$seguro', vehicle_note='$vehicle_note' WHERE idvehicle='$id'";
            $this->conexion->query($consulta);
            return $consulta;
       }

       //Método para actualizar datos de talleres
       public function update_taller($id, $nombre, $cif, $direccion, $email, $descripcion, $imagen, $video, $localidad, $estado, $telefono) {
            $consulta = "UPDATE workshop SET idworkshop='$idtaller', name='$nombre', cif='$cif', adress='$direccion', email='$email', description='$descripcion', image='$imagen', video='$video', location='$localidad', state='$estado', phone='$telefono' WHERE idworkshop='$id'";
            $this->conexion->query($consulta);
            return $consulta;

        }

        //Método para actualizar comentarios
        public function update_comment($numero, $commentary) {
             $consulta = "UPDATE commentary SET commentary='$commentary', update_date=NOW() WHERE idcommentary='$numero'";
             $this->conexion->query($consulta);
             return $consulta;

         }

         //Método para actualizar rating
         public function update_ranking($ranking, $iduser, $idworkshop) {
              $consulta = "UPDATE ranking SET ranking='$ranking' WHERE id_users='$iduser' AND id_workshops='$idworkshop'";
              $this->conexion->query($consulta);
              return $consulta;

          }

        /*
        -----------------
        PETICIONES DELETE
        -----------------
        */

        //Método para eliminar usuarios
        public function delete_usuario($idusuario) {

              $consulta = "DELETE FROM user WHERE iduser=$idusuario";
              $this->conexion->query($consulta);
              return $consulta;

        }

        //Método para eliminar vehículos
        public function delete_vehiculo($idvehiculo) {

              $consulta = "DELETE FROM vehicle WHERE idvehicle=$idvehiculo";
              $this->conexion->query($consulta);
              return $consulta;

        }

        //Método para eliminar talleres
        public function delete_taller($idtaller) {

              $consulta = "DELETE FROM workshop WHERE idworkshop=$idtaller";
              $this->conexion->query($consulta);
              return $consulta;

        }

        //Método para eliminar documentos
        public function delete_document($id) {

              $consulta = "DELETE FROM document WHERE iddocument=$id";
              $this->conexion->query($consulta);
              return $consulta;

        }

        //Método para eliminar comentarios
        public function delete_comment($id) {

              $consulta = "DELETE FROM commentary WHERE idcommentary=$id";
              $this->conexion->query($consulta);
              return $consulta;

        }

        //Método para eliminar rankings
        public function delete_ranking($id) {

              $consulta = "DELETE FROM ranking WHERE idranking=$id";
              $this->conexion->query($consulta);
              return $consulta;

        }

        //Método para eliminar fav
        public function delete_fav($iduser, $idworkshop) {

              $consulta = "DELETE FROM favorites WHERE idsuser='$iduser' AND idsworkshop='$idworkshop'";
              $this->conexion->query($consulta);
              return $consulta;

        }
}
?>
