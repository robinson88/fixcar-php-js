<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    ?>
    <script type="text/javascript">
        window.location.href = "../../";
    </script>
    <?php
} else {
    require "../../controller/C_Datos.php";
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Proyecto de FixCar"/>
        <meta name="keywords"
              content="taller, comparacion, distancia, precio, cambio de aceite, ruedas, piezas, reparacion, itv"/>
        <meta name="Cesur" content="Equipo">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
              crossorigin="anonymous">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="../assets/css/custom.css">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

        <title>FixCar</title>
    </head>
    <body>
    <!-- NAVBAR O MENU-->
    <div class="container-fluid">

        <nav class="row navbar navbar-expand-lg navbar-light bg-dark">
            <button class="navbar-toggler element" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Logo-->
            <a class="navbar-brand" href="../home-in/home-in.php">
                <img class="logo" src="../assets/images/logos/logo.png" alt="Fixcar logo">
            </a>

            <div class="row collapse navbar-collapse" id="navbarNav">
                <div class="col-7">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <!-- Elemento Inicio -->
                            <a class="nav-link" href="../home-in/home-in.php"><?php echo $_SESSION['usuario']; ?> -
                                Inicio<span class="sr-only"></span></a>
                        </li>
                        <li class="nav-item d-flex active">
                            <!-- Elemento recordatorio -->
                            <a class="nav-link" href="../profiles/edit-reminders.php">Recordatorios
                                <?php $n = 0;
                                while ($rowh = mysqli_fetch_array($consultav)) {
                                    if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['wheels_date']) && $rowh['wheels_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['wheels_date'])) {
                                        $n++;
                                    }
                                    if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['oil_date']) && $rowh['oil_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['oil_date'])) {
                                        $n++;
                                    }
                                    if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['review_date']) && $rowh['review_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['review_date'])) {
                                        $n++;
                                    }
                                    if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['date_itv']) && $rowh['date_itv'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['date_itv'])) {
                                        $n++;
                                    }
                                    if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['vehicle_insurance']) && $rowh['vehicle_insurance'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['vehicle_insurance'])) {
                                        $n++;
                                    }
                                    ?>
                                    <?php
                                }
                                if ($n != 0) {
                                    ?>
                                    <span class="reminder-badge navbar-reminder badge badge-danger badge-pill"> <?php echo $n; ?> </span>
                                <?php }
                                mysqli_data_seek($consultav, 0); ?>
                            </a>
                        </li>
                        <li class="nav-item">
                            <!-- Elemento Contacto -->
                            <a class="nav-link" href="favworkshops.php">Talleres favoritos</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false"> Perfiles </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="../profiles/profile-user.php">Mi perfil</a>
                                <a class="dropdown-item" href="../profiles/profile-vehicle.php">Mis vehículos</a>
                                <a class="dropdown-item" href="../profiles/edit-reminders.php">Mis recordatorios</a>
                                <a class="dropdown-item" href="../profiles/documents.php">Mis documentos</a>

                            </div>
                        </li>
                        <li class="nav-item">
                            <!-- Elemento Contacto -->
                            <a class="nav-link" href="../home-in/home-in.php#meet">Contacto</a>
                        </li>
                        <li class="nav-item">
                            <!-- Elemento log-out -->
                            <a class="nav-link" href="../../controller/logout.php"><i class="fas fa-power-off"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="row justify-content-center">
            <div class="row col-lg-12">
                <h1 class="title-contact col-md-12 mb-5">Mis Recordatorios</h1>
                <hr>
            </div>
            <?php while ($rowv = mysqli_fetch_array($consultav)) {
                if ($rowv['wheels_date'] == '0000-00-00' && $rowv['oil_date'] == '0000-00-00' && $rowv['review_date'] == '0000-00-00' && $rowv['date_itv'] == '0000-00-00' && $rowv['vehicle_insurance'] == '0000-00-00') {
                } else {
                    ?>
                    <div class="row col-10">
                        <h3 class="text-dark text-center font-weight-bold mt-3 mb-2"><?php echo $rowv['vehicle_band'] . ' ' . $rowv['vehicle_model']; ?></h3>
                    </div>
                    <div class="row col-lg-12">
                        <?php if ($rowv['date_itv'] != '0000-00-00') { ?>
                            <div class="col-sm-12 col-md-4 reminder-padding">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">ITV
                                            <?php if (strtotime($hoy . "+ 5 days") >= strtotime($rowv['date_itv']) && strtotime($hoy) <= strtotime($rowv['date_itv'])) {
                                                    echo '<span class="reminder-badge badge badge-danger badge-pill">!</span>';
                                                  }elseif (strtotime($rowv['date_itv']) < strtotime($hoy)) {
                                                    echo '<small>Este recordatorio ha caducado</small>';
                                                  }
                                            ?>
                                        </h5>
                                        <p class="card-text"><?php echo $rowv['date_itv']; ?></p>
                                        <p class="card-text"><small class="text-muted"><?php echo $rowv['itv_note'] ?></small></p>
                                        <form class="" action="../../controller/C_Vehiculos.php" method="post">
                                          <p class="align-self-end row justify-content-end">
                                            <input type="hidden" name="date_itv" value="0000-00-00">
                                            <input type="hidden" name="itv_note" value="">
                                            <input type="hidden" name="wheels_date" value="<?php echo $rowv['wheels_date'] ?>">
                                            <input type="hidden" name="wheels_note" value="<?php echo $rowv['wheels_note'] ?>">
                                            <input type="hidden" name="oil_date" value="<?php echo $rowv['oil_date'] ?>">
                                            <input type="hidden" name="oil_note" value="<?php echo $rowv['oil_note'] ?>">
                                            <input type="hidden" name="review_date" value="<?php echo $rowv['review_date'] ?>">
                                            <input type="hidden" name="review_note" value="<?php echo $rowv['review_note'] ?>">
                                            <input type="hidden" name="vehicle_insurance" value="<?php echo $rowv['vehicle_insurance'] ?>">
                                            <input type="hidden" name="vehicle_note" value="<?php echo $rowv['vehicle_note'] ?>">
                                            <input type="hidden" name="id" value="<?php echo $rowv['idvehicle'] ?>">
                                            <button type="submit" name="deletereminder" class="m-auto col-6 btn btn-danger align-self-end">Borrar</button>
                                          </p>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($rowv['wheels_date'] != '0000-00-00') { ?>

                            <div class="col-sm-12 col-md-4 reminder-padding">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Ruedas
                                            <?php if (strtotime($hoy) >= strtotime($rowv['wheels_date'] . "- 5 days") && strtotime($hoy) <= strtotime($rowv['wheels_date'])) {
                                                      echo '<span class="reminder-badge badge badge-danger badge-pill">!</span>';
                                                  } elseif (strtotime($rowv['wheels_date']) < strtotime($hoy)) {
                                                      echo '<small>Este recordatorio ha caducado</small>';
                                                  }
                                            ?>
                                        </h5>
                                        <p class="card-text"><?php echo $rowv['wheels_date']; ?></p>
                                        <p class="card-text"><small class="text-muted"><?php echo $rowv['wheels_note'] ?></small></p>
                                        <form class="" action="../../controller/C_Vehiculos.php" method="post">
                                          <p class="align-self-end row justify-content-end">
                                            <input type="hidden" name="date_itv" value="<?php echo $rowv['date_itv'] ?>">
                                            <input type="hidden" name="itv_note" value="<?php echo $rowv['itv_note'] ?>">
                                            <input type="hidden" name="wheels_date" value="0000-00-00">
                                            <input type="hidden" name="wheels_note" value="">
                                            <input type="hidden" name="oil_date" value="<?php echo $rowv['oil_date'] ?>">
                                            <input type="hidden" name="oil_note" value="<?php echo $rowv['oil_note'] ?>">
                                            <input type="hidden" name="review_date" value="<?php echo $rowv['review_date'] ?>">
                                            <input type="hidden" name="review_note" value="<?php echo $rowv['review_note'] ?>">
                                            <input type="hidden" name="vehicle_insurance" value="<?php echo $rowv['vehicle_insurance'] ?>">
                                            <input type="hidden" name="vehicle_note" value="<?php echo $rowv['vehicle_note'] ?>">
                                            <input type="hidden" name="id" value="<?php echo $rowv['idvehicle'] ?>">
                                            <button type="submit" name="deletereminder" class="m-auto col-6 btn btn-danger align-self-end">Borrar</button>
                                          </p>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($rowv['oil_date'] != '0000-00-00') { ?>
                            <div class="col-sm-12 col-md-4 reminder-padding">


                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Aceite
                                            <?php if (strtotime($hoy . "+ 5 days") >= strtotime($rowv['oil_date']) && strtotime($hoy) <= strtotime($rowv['oil_date'])) {
                                                      echo '<span class="reminder-badge badge badge-danger badge-pill">!</span>';
                                                  }elseif (strtotime($rowv['oil_date']) < strtotime($hoy)) {
                                                      echo '<small>Este recordatorio ha caducado</small>';
                                                  }
                                            ?>
                                        </h5>
                                        <p class="card-text"><?php echo $rowv['oil_date']; ?></p>
                                        <p class="card-text"><small class="text-muted"><?php echo $rowv['oil_note'] ?></small></p>
                                        <form class="" action="../../controller/C_Vehiculos.php" method="post">
                                          <p class="align-self-end row justify-content-end">
                                            <input type="hidden" name="date_itv" value="<?php echo $rowv['date_itv'] ?>">
                                            <input type="hidden" name="itv_note" value="<?php echo $rowv['itv_note'] ?>">
                                            <input type="hidden" name="wheels_date" value="<?php echo $rowv['wheels_date'] ?>">
                                            <input type="hidden" name="wheels_note" value="<?php echo $rowv['wheels_note'] ?>">
                                            <input type="hidden" name="oil_date" value="0000-00-00">
                                            <input type="hidden" name="oil_note" value="">
                                            <input type="hidden" name="review_date" value="<?php echo $rowv['review_date'] ?>">
                                            <input type="hidden" name="review_note" value="<?php echo $rowv['review_note'] ?>">
                                            <input type="hidden" name="vehicle_insurance" value="<?php echo $rowv['vehicle_insurance'] ?>">
                                            <input type="hidden" name="vehicle_note" value="<?php echo $rowv['vehicle_note'] ?>">
                                            <input type="hidden" name="id" value="<?php echo $rowv['idvehicle'] ?>">
                                            <button type="submit" name="deletereminder" class="m-auto col-6 btn btn-danger align-self-end">Borrar</button>
                                          </p>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        <?php } ?>
                        <?php if ($rowv['review_date'] != '0000-00-00') { ?>

                            <div class="col-sm-12 col-md-4 reminder-padding">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Revisión
                                            <?php if (strtotime($hoy . "+ 5 days") >= strtotime($rowv['review_date']) && strtotime($hoy) <= strtotime($rowv['review_date'])) {
                                                    echo '<span class="reminder-badge badge badge-danger badge-pill">!</span>';
                                                  }elseif (strtotime($rowv['review_date']) < strtotime($hoy)) {
                                                      echo '<small>Este recordatorio ha caducado</small>';
                                                  }
                                            ?>
                                        </h5>
                                        <p class="card-text"><?php echo $rowv['review_date']; ?></p>
                                        <p class="card-text"><small class="text-muted"><?php echo $rowv['review_note'] ?></small></p>
                                        <form class="" action="../../controller/C_Vehiculos.php" method="post">
                                          <p class="align-self-end row justify-content-end">
                                            <input type="hidden" name="date_itv" value="<?php echo $rowv['date_itv'] ?>">
                                            <input type="hidden" name="itv_note" value="<?php echo $rowv['itv_note'] ?>">
                                            <input type="hidden" name="wheels_date" value="<?php echo $rowv['wheels_date'] ?>">
                                            <input type="hidden" name="wheels_note" value="<?php echo $rowv['wheels_note'] ?>">
                                            <input type="hidden" name="oil_date" value="<?php echo $rowv['oil_date'] ?>">
                                            <input type="hidden" name="oil_note" value="<?php echo $rowv['oil_note'] ?>">
                                            <input type="hidden" name="review_date" value="0000-00-00">
                                            <input type="hidden" name="review_note" value="">
                                            <input type="hidden" name="vehicle_insurance" value="<?php echo $rowv['vehicle_insurance'] ?>">
                                            <input type="hidden" name="vehicle_note" value="<?php echo $rowv['vehicle_note'] ?>">
                                            <input type="hidden" name="id" value="<?php echo $rowv['idvehicle'] ?>">
                                            <button type="submit" name="deletereminder" class="m-auto col-6 btn btn-danger align-self-end">Borrar</button>
                                          </p>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        <?php } ?>
                        <?php if ($rowv['vehicle_insurance'] != '0000-00-00') { ?>
                            <div class="col-sm-12 col-md-4 reminder-padding">

                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Seguro
                                            <?php if (strtotime($hoy . "+ 5 days") >= strtotime($rowv['vehicle_insurance']) && strtotime($hoy) <= strtotime($rowv['vehicle_insurance'])) {
                                                    echo '<span class="reminder-badge badge badge-danger badge-pill">!</span>';
                                                  }elseif (strtotime($rowv['vehicle_insurance']) < strtotime($hoy)) {
                                                      echo '<small>Este recordatorio ha caducado</small>';
                                                  }
                                            ?>
                                        </h5>
                                        <p class="card-text"><?php echo $rowv['vehicle_insurance']; ?></p>
                                        <p class="card-text"><small class="text-muted"><?php echo $rowv['vehicle_note'] ?></small></p>
                                        <form class="" action="../../controller/C_Vehiculos.php" method="post">
                                          <p class="align-self-end row justify-content-end">
                                            <input type="hidden" name="date_itv" value="<?php echo $rowv['date_itv'] ?>">
                                            <input type="hidden" name="itv_note" value="<?php echo $rowv['itv_note'] ?>">
                                            <input type="hidden" name="wheels_date" value="<?php echo $rowv['wheels_date'] ?>">
                                            <input type="hidden" name="wheels_note" value="<?php echo $rowv['wheels_note'] ?>">
                                            <input type="hidden" name="oil_date" value="<?php echo $rowv['oil_date'] ?>">
                                            <input type="hidden" name="oil_note" value="<?php echo $rowv['oil_note'] ?>">
                                            <input type="hidden" name="review_date" value="<?php echo $rowv['review_date'] ?>">
                                            <input type="hidden" name="review_note" value="<?php echo $rowv['review_note'] ?>">
                                            <input type="hidden" name="vehicle_insurance" value="0000-00-00">
                                            <input type="hidden" name="vehicle_note" value="">
                                            <input type="hidden" name="id" value="<?php echo $rowv['idvehicle'] ?>">
                                            <button type="submit" name="deletereminder" class="m-auto col-6 btn btn-danger align-self-end">Borrar</button>
                                          </p>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        <?php } ?>

                    </div>
                <?php }
            }
            mysqli_data_seek($consultav, 0); ?>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-12 mt-5">
                <h2 class="text-dark text-center text-uppercase font-weight-bold">Nuevo recordatorio</h2>
            </div>
            <div class="card col-sm-12 col-md-4 mb-5">
                <form class="form-custom" action="../../controller/C_Vehiculos.php" method="post">
                    <div class="form-group">
                        <label for="reminder-class">¿Para qué coche es?</label>
                        <select class="form-control" id="cars" name="car">
                            <?php while ($row2 = mysqli_fetch_array($consultav)) { ?>
                                <option value="<?php echo $row2['idvehicle']; ?>"><?php echo $row2['vehicle_band'] . ' ' . $row2['vehicle_model']; ?></option>
                            <?php } ?>
                        </select>

                        <label for="reminder-class">¿Qué quieres que te recordemos?</label>
                        <select class="form-control" name="type">
                            <option value="itv">Caducidad ITV</option>
                            <option value="insurance">Caducidad del seguro</option>
                            <option value="wheels">Cambio de ruedas</option>
                            <option value="oil">Cambio de aceite</option>
                            <option value="review">Revisión del vehículo</option>
                        </select>

                    </div>
                    <div class="form-group">
                        <label for="reminder-date">¿Para cuándo es? ¡Te avisaremos 5 días antes!</label>
                        <input type="date" class="form-control" name="reminder-date"/>
                    </div>
                    <div class="form-group">
                        <label for="nota">¿Quieres añadir alguna nota?</label>
                        <textarea class="form-control" id="Textarea1" name="note" rows="3" placeholder="Añadir notas..."
                                  maxlength="180"></textarea>
                    </div>
                    <button class="btn btn-lg btn-block btn-dark" name="newreminder" type="submit">
                        Enviar
                    </button>
                </form>
            </div>
        </div>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
                integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
                crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
                integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
                crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
                integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"
                integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>
    </body>
    </html>
    <?php
}
?>
