<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    ?>
    <script type="text/javascript">
        window.location.href = "../../";
    </script>
    <?php
} else {
    require "../../controller/C_Datos.php";
    ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Proyecto de FixCar"/>
    <meta name="keywords"
          content="taller, comparacion, distancia, precio, cambio de aceite, ruedas, piezas, reparacion, itv"/>
    <meta name="Cesur" content="Equipo">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
          crossorigin="anonymous">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../assets/css/custom.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

    <title>FixCar</title>
</head>
<body>
<!-- NAVBAR O MENU-->
<div class="container-fluid">
    <nav class="row navbar navbar-expand-lg navbar-light bg-dark">
        <button class="navbar-toggler element" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Logo-->
        <a class="navbar-brand" href="../home-in/home-in.php">
            <img class="logo" src="../assets/images/logos/logo.png" alt="Fixcar logo">
        </a>

        <div class="row collapse navbar-collapse" id="navbarNav">
            <div class="col-7">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <!-- Elemento Inicio -->
                        <a class="nav-link" href="../home-in/home-in.php"><?php echo $_SESSION['usuario']; ?> -
                            Inicio<span class="sr-only"></span></a>
                    </li>
                    <li class="nav-item d-flex">
                        <!-- Elemento recordatorio -->
                        <a class="nav-link" href="../profiles/edit-reminders.php">Recordatorios
                            <?php $n = 0;
                            while ($rowh = mysqli_fetch_array($consultav)) {
                                if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['wheels_date']) && $rowh['wheels_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['wheels_date'])) {
                                    $n++;
                                }
                                if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['oil_date']) && $rowh['oil_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['oil_date'])) {
                                    $n++;
                                }
                                if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['review_date']) && $rowh['review_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['review_date'])) {
                                    $n++;
                                }
                                if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['date_itv']) && $rowh['date_itv'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['date_itv'])) {
                                    $n++;
                                }
                                if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['vehicle_insurance']) && $rowh['vehicle_insurance'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['vehicle_insurance'])) {
                                    $n++;
                                }
                                ?>
                                <?php
                            }
                            if ($n != 0) {
                                ?>
                                <span class="reminder-badge navbar-reminder badge badge-danger badge-pill"> <?php echo $n; ?> </span>
                            <?php }
                            mysqli_data_seek($consultav, 0); ?>
                        </a>
                    </li>
                    <li class="nav-item active">
                        <!-- Elemento Contacto -->
                        <a class="nav-link" href="">Talleres favoritos</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false"> Perfiles </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="../profiles/profile-user.php">Mi perfil</a>
                            <a class="dropdown-item" href="../profiles/profile-vehicle.php">Mis vehículos</a>
                            <a class="dropdown-item" href="../profiles/edit-reminders.php">Mis recordatorios</a>
                            <a class="dropdown-item" href="../profiles/documents.php">Mis documentos</a>

                        </div>
                    </li>
                    <li class="nav-item">
                        <!-- Elemento Contacto -->
                        <a class="nav-link" href="../home-in/home-in.php#meet">Contacto</a>
                    </li>
                    <li class="nav-item">
                        <!-- Elemento log-out -->
                        <a class="nav-link" href="../../controller/logout.php"><i class="fas fa-power-off"></i></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-12 col-lg-5 d-flex">
              <form class="form-inline ml-auto p-2" action="../../controller/C_Talleres.php?search-in=<?php echo $_GET['search-in'] ?>" method="post">
                <div class="row d-flex">
                  <div class="">
                    <button class="btn btn-car form-control-button search-filter dropdown">
                      <!-- Elemento de búsqueda por filtros-->
                        <a class="nav-link dropdown-toggle filter-text" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Filtrar búsqueda </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <div class="col-md-12">
                              <div class="form-check filter-list">
                                  <input class="form-check-input" type="checkbox" name="mechanics" value="" id="oil-check">
                                  <label class="form-check-label" for="oil-check">
                                      Mecánica
                                  </label>
                              </div>
                              <div class="form-check filter-list">
                                  <input class="form-check-input move-left" type="checkbox" name="electricity" value="" id="itv-check">
                                  <label class="form-check-label" for="itv-check">
                                      Electricidad
                                  </label>
                              </div>
                              <div class="form-check filter-list">
                                  <input class="form-check-input move-left" type="checkbox" name="repair" value="" id="repair-check">
                                  <label class="form-check-label" for="repair-check">
                                      Reparaciones
                                  </label>
                              </div>
                              <div class="form-check filter-list">
                                  <input class="form-check-input move-left" type="checkbox" name="overhaul" value="" id="overhaul-check">
                                  <label class="form-check-label" for="overhaul-check">
                                      Revisión
                                  </label>
                              </div>
                              <div class="form-check filter-list">
                                  <input class="form-check-input move-left" type="checkbox" name="bodywork" value="" id="clean-check">
                                  <label class="form-check-label" for="clean-check">
                                      Chapa y pintura
                                  </label>
                              </div>
                              <div class="form-check filter-list">
                                  <input class="form-check-input move-left" type="checkbox" name="creditcard" value="" id="credit-check">
                                  <label class="form-check-label" for="credit-check">
                                      Pago con tarjeta
                                  </label>
                              </div>
                              <div class="form-check filter-list">
                                  <input class="form-check-input move-left" type="checkbox" name="premium" value="" id="credit-check">
                                  <label class="form-check-label" for="credit-check">
                                      PREMIUM
                                  </label>
                              </div>
                              <!-- Aquí acaba el chechbox de filtros de búsqueda -->
                          </div>
                        </div>
                    </button>
                  </div>
                  <div class="ajust-l">
                    <button class="btn btn-car btn-block my-2 my-sm-0 filter" style="width: 45px;" type="submit" name="filtersearch">
                        <i class="fas fa-car-side"></i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
        </div>
    </nav>
        <div class="col-md-10 mx-auto">
            <div class="row">
                <?php if (mysqli_num_rows($consultaallfav) > 0) : ?>
                    <div class="col-md-12 col-lg-12 col-xl-12 col-sm-12 col-xs-12 mx-auto">
                        <?php
                        $i=0;
                        foreach ($consultaallfav as $rowt) :
                          ?>
                            <div class="row">
                                <div class="col-md-12 p-2">
                                    <div class="card border-light mt-3 pt-2 pb-2 row workshop-card-pointer" <?php if ($rowt['state'] == "Premium") { echo 'style="background: #E4FFEB;"'; } ?> onclick="findWorkshop('../search-in/workshop-view.php?id=<?=$rowt['idworkshop']?>')">
                                        <div class="row p-0 m-0">
                                            <div class="col-md-4 col-lg-3 col-sm-8 col-xs-9 col-xl-2 mx-auto card-img thumbnail">
                                                <img src="<?php if ($rowt['image'] == '') { ?>../assets/images/background/workshop.jpg<?php } else {
                                                    echo '../' . $rowt['image'];
                                                } ?>" class="thumbnail search-workshop-image" alt="...">
                                            </div>
                                            <div class="card-body col-md-8 col-lg-9 col-xl-10 mx-auto col-sm-12 m-0">
                                              <div class="float-right crownlist">
                                                <div class="crowncont">
                                                  <?php if ($rowt['state'] == 'Premium') { ?>
                                                      <i class="fas fa-crown premium-sign iconcrownlist float-right"></i>
                                                  <?php } ?>
                                                </div>
                                                <div class="heartcont">
                                                  <i class="fa fa-heart heart-color heartlist"></i>
                                                </div>
                                                <div class="contactcont">
                                                  <button class="listbut" type="submit" name="contact">
                                                    Contactar
                                                  </button>
                                                </div>
                                              </div>
                                                <div class="row m-1 flex">
                                                    <h5 class="col-md-12 col-sm-12 card-title text-info">
                                                        <?php echo $rowt['name']; ?>
                                                        <!-- Rating -->
                                                        <?php if ($arraymedia[$i] != 0): ?>
                                                            <span class="ml-2 mt-3 rating">
                                                              <i class="fas fa fa-star" <?php if ($arraymedia[$i] >= 5) { echo 'style="color: #ffc107"'; } ?>></i>
                                                              <i class="fas fa fa-star" <?php if ($arraymedia[$i] >= 4) { echo 'style="color: #ffc107"'; } ?>></i>
                                                              <i class="fas fa fa-star" <?php if ($arraymedia[$i] >= 3) { echo 'style="color: #ffc107"'; } ?>></i>
                                                              <i class="fas fa fa-star" <?php if ($arraymedia[$i] >= 2) { echo 'style="color: #ffc107"'; } ?>></i>
                                                              <i class="fas fa fa-star" <?php if ($arraymedia[$i] >= 1) { echo 'style="color: #ffc107"'; } ?>></i>
                                                            </span>
                                                          <?php endif; ?>
                                                    </h5>
                                                    <!-- Tipos de taller -->
                                                    <div class="col-md-12 col-sm-6 card-text text-secondary">
                                                      <?php
                                                          if ($rowt['mechanics'] == 1) { echo '<span class="workshoptypecont">Mecánica</span>'; }
                                                          if ($rowt['repairs'] == 1) { echo '<span class="workshoptypecont">Reparaciones</span>'; }
                                                          if ($rowt['electricity'] == 1) { echo '<span class="workshoptypecont">Electricidad</span>'; }
                                                          if ($rowt['bodywork'] == 1) { echo '<span class="workshoptypecont">Chapa y pintura</span>'; }
                                                          if ($rowt['review'] == 1) { echo '<span class="workshoptypecont">Revisión</span>'; }
                                                          if ($rowt['creditcard'] == 1) { echo '<span class="workshoptypecont">Pago con tarjeta</span>'; }
                                                          if($rowt['state'] == "Premium") { echo '<span class="workshoptypecont">Premium</span>'; }
                                                      ?>
                                                    </div>
                                                    <div class="col-md-12 col-sm-6 card-text text-secondary">Teléfono: <?php echo ' '.$rowt['phone']; ?><br>Email:<?php echo ' '.$rowt['email']; ?><br><?php echo $rowt['adress']; ?>, <?php echo $rowt['location']; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php $i++; endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACXVmPCf6pnuzVYfn4hm9KdVM4rS6_23U&callback=initMap"
         async defer></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"
        integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>
<script>
    function findWorkshop(url) {
        window.location = url;
    }
</script>
</body>
</html>
<?php
}
?>
