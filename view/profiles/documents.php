<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    ?>
    <script type="text/javascript">
        window.location.href = "../../";
    </script>
    <?php
} else {
    require "../../controller/C_Datos.php";
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Proyecto de FixCar"/>
        <meta name="keywords"
              content="taller, comparacion, distancia, precio, cambio de aceite, ruedas, piezas, reparacion, itv"/>
        <meta name="Cesur" content="Equipo">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
              crossorigin="anonymous">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="../assets/css/custom.css">
        <link rel="stylesheet" href="../assets/css/upload-buttom.css">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

        <title>FixCar</title>
    </head>
    <body>
    <!-- NAVBAR O MENU-->
    <div class="container-fluid">
        <nav class="row navbar navbar-expand-lg navbar-light bg-dark">
            <button class="navbar-toggler element" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Logo-->
            <a class="navbar-brand" href="../home-in/home-in.php">
                <img class="logo" src="../assets/images/logos/logo.png" alt="Fixcar logo">
            </a>

            <div class="row collapse navbar-collapse" id="navbarNav">
                <div class="col-7">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <!-- Elemento Inicio -->
                            <a class="nav-link" href="../home-in/home-in.php"><?php echo $_SESSION['usuario']; ?> -
                                Inicio<span class="sr-only"></span></a>
                        </li>
                        <li class="nav-item d-flex">
                            <!-- Elemento recordatorio -->
                            <a class="nav-link" href="../profiles/edit-reminders.php">Recordatorios
                                <?php $n = 0;
                                while ($rowh = mysqli_fetch_array($consultav)) {
                                    if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['wheels_date']) && $rowh['wheels_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['wheels_date'])) {
                                        $n++;
                                    }
                                    if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['oil_date']) && $rowh['oil_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['oil_date'])) {
                                        $n++;
                                    }
                                    if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['review_date']) && $rowh['review_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['review_date'])) {
                                        $n++;
                                    }
                                    if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['date_itv']) && $rowh['date_itv'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['date_itv'])) {
                                        $n++;
                                    }
                                    if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['vehicle_insurance']) && $rowh['vehicle_insurance'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['vehicle_insurance'])) {
                                        $n++;
                                    }
                                    ?>
                                    <?php
                                }
                                if ($n != 0) {
                                    ?>
                                    <span class="reminder-badge navbar-reminder badge badge-danger badge-pill"> <?php echo $n; ?> </span>
                                <?php }
                                mysqli_data_seek($consultav, 0); ?>
                            </a>
                        </li>
                        <li class="nav-item">
                            <!-- Elemento Contacto -->
                            <a class="nav-link" href="favworkshops.php">Talleres favoritos</a>
                        </li>
                        <li class="nav-item active dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false"> Perfiles </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="../profiles/profile-user.php">Mi perfil</a>
                                <a class="dropdown-item" href="../profiles/profile-vehicle.php">Mis vehículos</a>
                                <a class="dropdown-item" href="../profiles/edit-reminders.php">Mis recordatorios</a>
                                <a class="dropdown-item" href="../profiles/documents.php">Mis documentos</a>

                            </div>
                        </li>
                        <li class="nav-item">
                            <!-- Elemento Contacto -->
                            <a class="nav-link" href="../home-in/home-in.php#meet">Contacto</a>
                        </li>
                        <li class="nav-item">
                            <!-- Elemento log-out -->
                            <a class="nav-link" href="../../controller/logout.php"><i class="fas fa-power-off"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="row">
            <div class="col-md-8 mx-auto shadow-lg p-3 mt-5 bg-white rounded" method="post">
                <form class="document-type row" action="../../controller/C_Documentos.php" enctype="multipart/form-data" method="post">
                    <div class="col-md-12 mb-5 mt-5 text-center">
                        <h4 class="text-uppercase font-weight-bold">Adjuntar un nuevo documento</h4>
                    </div>
                    <div class="custom-file border-secondary col-md-4 mb-2 btn btn-light text-dark p-1">
                        <input type="file" id="file" name="file"/>
                        <label for="file" class="btn-2 text-dark">Adjuntar archivo</label>
                    </div>
                    <label class="col-md-12 mt-2 mb-1" for="documents-type"> ¿Quieres relacionar tus documentos con un vehículo?</label><br>
                    <select name="vehicle" class="col-md-12 mb-2 border-secondary text-muted custom-select" id="documents-type">
                      <?php while ($row2 = mysqli_fetch_array($consultav)){ ?>
                       <option value="<?php echo $row2['idvehicle']; ?>"><?php echo $row2['vehicle_band'].' '.$row2['vehicle_model']; ?></option>
                      <?php } mysqli_data_seek($consultav, 0); ?>
                    </select>
                    <textarea class="col-md-12" id="w3review" name="notes" rows="4" cols="50" placeholder="Notas para mis documentos"></textarea>
                    <div class="col-md-12 mx-auto p-2 m-3">
                      <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="indentity" name="documentradio" class="custom-control-input" value="Documentos de identidad">
                          <label class="custom-control-label" for="indentity">Documentos de identidad</label>
                      </div>
                      <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="insurance" name="documentradio" class="custom-control-input" value="Mis seguros">
                          <label class="custom-control-label" for="insurance">Mis seguros</label>
                      </div>
                      <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="repair-info" name="documentradio" class="custom-control-input" value="Mis reparaciones">
                          <label class="custom-control-label" for="repair-info">Mis reparaciones</label>
                      </div>
                      <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="last-itv" name="documentradio" class="custom-control-input" value="Mis datos ITV">
                          <label class="custom-control-label" for="last-itv">Mis datos ITV</label>
                      </div>
                      <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="last-review" name="documentradio" class="custom-control-input" value="Mis revisiones">
                          <label class="custom-control-label" for="last-review">Mis revisiones</label>
                      </div>
                      <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="other" name="documentradio" class="custom-control-input" value="Otros datos">
                          <label class="custom-control-label" for="other">Otros datos</label>
                      </div>
                    </div>
                    <div class="col-md-5 mx-auto mt-1">
                        <button type="submit" name="adddoc" class="btn btn-dark btn-lg btn-block">Guardar cambios</button>
                    </div>
                </form>
            </div>
            <div class="col-md-8 mx-auto shadow-lg p-3 mt-5 mb-5 bg-white rounded">
                <form class="document-type" action="index.html" method="get">
                    <div class="col-md-12 mx-auto mb-5">
                        <h4 class="mt-5 text-center text-uppercase font-weight-bold">Documentos guardados</h4>
                    </div>
                    <br>
                    <label for="documents-type">Tipo de documento</label><br>
                    <select name="documents-type" class="border-secondary col-md-12 mb-2 text-muted custom-select" id="documents-type-filter">
                        <option id="0" value="all">Mostrar todo</option>
                        <option id="1" value="Documentos de identidad">Documentos de identidad</option>
                        <option id="2" value="Mis seguros">Mi seguro</option>
                        <option id="3" value="Mis reparaciones">Mis reparaciones</option>
                        <option id="5" value="Mis datos ITV">Mis datos ITV</option>
                        <option id="4" value="Mis revisiones">Mis revisiones</option>
                        <option id="6" value="Otros datos">Otros datos</option>
                    </select>
                    <div class="row m-2 p-2">
                          <?php foreach ($consultad1 as $rowd1) { ?>
                        <div class="card col-md-12 border-light mb-3 document-card-pointer file-filter" data-type="<?=$rowd1['type_document']?>">
                            <div class="row ">
                                <div class="col-md-4 card-img m-1">
                                    <div class="row p-2">
                                        <img onclick="findDocument('<?php echo '../'.$rowd1['documents']; ?>')" src="<?php echo '../'.$rowd1['documents']; ?>" class="document-img card-img"
                                             alt="...">
                                    </div>
                                </div>
                                <div class="card-body col-md-12">
                                    <div class="row">
                                        <h5 class="card-title text-info text-center col-md-3"><?php foreach ($consultav as $rowv) { if ($rowd1['idVehicle'] == $rowv['idvehicle']){ echo $rowv['vehicle_band'].' '.$rowv['vehicle_model'].' - '.$rowd1['type_document']; } } ?></h5>
                                        <p class="card-text text-secondary col-md-6"><?php echo $rowd1['notes']; ?></p>
                                        <div class="col-md-3">
                                            <div class="row">
                                                <form method="get" class="form-group p-0 col-md-12 col-sm-12 col-lg-12">
                                                    <div class="document-function-buttons">
                                                        <a class="btn btn-info btn-block text-white" href="<?php echo '../'.$rowd1['documents']; ?>" download>
                                                            Descargar
                                                        </a>
                                                    </div>
                                                </form>
                                                <form method="post" action="../../controller/C_Documentos.php" class="form-group p-0 col-md-12 col-sm-12 col-lg-12">
                                                    <div class="document-function-buttons mt-2">
                                                        <input type="hidden" name="iddocument" value="<?php echo $rowd1['iddocument']; ?>">
                                                        <button type="submit" name="deletedoc" class="btn btn-danger btn-block">Eliminar</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      <?php }  ?>
                    <div class="col-md-5 mx-auto mt-1">
                      <form class="post" action="../../controller/C_Documentos.php" method="post">
                        <button type="submit" name="downloadall" class="btn btn-dark btn-lg btn-block">Descargar todo</button>
                      </form>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"
            integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>
    <script>
        function findDocument(url) {
            window.open(url, '_blank');
        }
    </script>
    <script>
        function removeDocument(documentId) {
            var document = document.getElementById(documentId);
            document.parentNode.removeChild(document);
        }
    </script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $( "#documents-type-filter" ).change(function() {
                var filterBy = $(this).val();

                $( ".file-filter" ).each(function() {
                    $( this ).addClass( "d-none" );
                });

                if("all" === filterBy) {
                    $( ".file-filter" ).each(function() {
                        $( this ).removeClass( "d-none" );
                    });
                }

                $( ".file-filter[data-type='"+filterBy+"']" ).each(function() {
                    $( this ).removeClass( "d-none" );
                });
            });
        });
    </script>
    </body>
    </html>
    <?php
}
?>
