<?php
session_start();
if (!isset($_SESSION['usuario'])) {
?>
<script type="text/javascript">
    window.location.href="../../";
</script>
<?php
}else {
  require "../../controller/C_Datos.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Proyecto de FixCar" />
    <meta name="keywords" content="taller, comparacion, distancia, precio, cambio de aceite, ruedas, piezas, reparacion, itv" />
    <meta name="Cesur" content="Equipo">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../assets/css/custom.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

    <title>FixCar</title>
</head>
<body>
<!-- NAVBAR O MENU-->
<div class="container-fluid">

  <nav class="row navbar navbar-expand-lg navbar-light bg-dark">
      <button class="navbar-toggler element" type="button" data-toggle="collapse" data-target="#navbarNav"
              aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Logo-->
      <a class="navbar-brand" href="../home-in/home-in.php">
        <img class="logo" src="../assets/images/logos/logo.png" alt="Fixcar logo">
      </a>

      <div class="row collapse navbar-collapse" id="navbarNav">
        <div class="col-7">
          <ul class="navbar-nav">
              <li class="nav-item">
                  <!-- Elemento Inicio -->
                  <a  class="nav-link" href="../home-in/home-in.php"><?php echo $_SESSION['usuario']; ?> - Inicio<span class="sr-only"></span></a>
              </li>
              <li class="nav-item d-flex">
                  <!-- Elemento recordatorio -->
                  <a class="nav-link" href="../profiles/edit-reminders.php">Recordatorios
                      <?php $n = 0;
                      while ($rowh = mysqli_fetch_array($consultav)) {
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['wheels_date']) && $rowh['wheels_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['wheels_date'])) {
                              $n++;
                          }
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['oil_date']) && $rowh['oil_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['oil_date'])) {
                              $n++;
                          }
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['review_date']) && $rowh['review_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['review_date'])) {
                              $n++;
                          }
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['date_itv']) && $rowh['date_itv'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['date_itv'])) {
                              $n++;
                          }
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['vehicle_insurance']) && $rowh['vehicle_insurance'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['vehicle_insurance'])) {
                              $n++;
                          }
                          ?>
                          <?php
                      }
                      if ($n != 0) {
                          ?>
                          <span class="reminder-badge navbar-reminder badge badge-danger badge-pill"> <?php echo $n; ?> </span>
                      <?php }
                      mysqli_data_seek($consultav, 0); ?>
                  </a>
              </li>
              <li class="nav-item">
                  <!-- Elemento Contacto -->
                  <a class="nav-link" href="favworkshops.php">Talleres favoritos</a>
              </li>
              <li class="nav-item dropdown active">
                  <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Perfiles </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="../profiles/profile-user.php">Mi perfil</a>
                      <a class="dropdown-item" href="../profiles/profile-vehicle.php">Mis vehículos</a>
                      <a class="dropdown-item" href="../profiles/edit-reminders.php">Mis recordatorios</a>
                      <a class="dropdown-item" href="../profiles/documents.php">Mis documentos</a>

                  </div>
              </li>
              <li class="nav-item">
                  <!-- Elemento Contacto -->
                  <a class="nav-link" href="../home-in/home-in.php#meet">Contacto</a>
              </li>
              <li class="nav-item">
                  <!-- Elemento log-out -->
                  <a class="nav-link" href="../../controller/logout.php"><i class="fas fa-power-off"></i></a>
              </li>
          </ul>
        </div>
      </div>
  </nav>

  <div class="row">
      <div class="col-md-9 mx-auto">
          <!-- user-profile -->
          <div class="">
              <div id="user" class=""><br>
                  <h3 class="register-heading">Edición de mi perfil</h3>
                  <form class="row mt-5 register-form" action="../../controller/C_Usuarios.php" method="post" enctype="multipart/form-data">
                      <div class="col-md-12">
                          <div class="form-group ">
                              <input type="image" class="circle row mx-auto" name="user-image" src="<?php if ($row['image'] == '') {?>../assets/images/icons/user-icon.png<?php }else{ echo '../'.$row['image']; } ?>" alt="profile">
                          </div>
                      </div>
                      <div class="col-md-12 mx-auto mb-5">
                          <div class="form-group">
                              <input type="file" class="form-control-file" id="image" name="image"  aria-describedby="image-help">
                          </div>
                          <small id="image-help" class="form-text text-muted">La imagen que subas se mostrará cuando hagas un comentario</small>
                      </div>
                      <div class="col-md-6 col-xl-4">
                          <div class="form-group">
                              <input type="text" class="form-control" name="username" value="<?php echo $_SESSION['usuario']; ?>" readonly/>
                          </div>
                      </div>
                      <div class="col-md-6 col-xl-4">
                          <div class="form-group">
                              <input type="email" class="form-control" name="email" value="<?php echo $row['email']; ?>" readonly/>
                          </div>
                      </div>
                      <div class="col-md-6 col-xl-4">
                          <div class="form-group">
                              <input type="text" class="form-control" name="user-location" value="<?php if ($row['adress'] == '') { ?>Dirección <?php }else{ echo $row['adress']; } ?>"/>
                          </div>
                      </div>
                      <div class="col-md-6 col-xl-4">
                          <div class="form-group">
                              <input type="text" class="form-control" name="city" value="<?php if ($row['location'] == '') { ?>Localidad <?php }else{ echo $row['location']; } ?>"/>
                          </div>
                      </div>
                      <div class="col-md-6 col-xl-4">
                          <div class="form-group">
                              <input type="text" class="form-control" name="telefono" value="<?php if ($row['phone'] == '') { ?>Teléfono <?php }else{ echo $row['phone']; } ?>"/>
                          </div>
                      </div>
                      <div class="col-md-6  col-xl-4 mb-5">
                          <div class="form-group">
                              <input type="text" class="form-control" name="fecha" value="<?php if ($row['birth_date'] == '') { ?>Fecha de nacimiento <?php }else{ echo $row['birth_date']; } ?>"/>
                          </div>
                      </div>
                      <div class="col-md-5 mx-auto mt-5">
                          <button type="submit" class="btn btn-dark btn-lg btn-block" name="save">Guardar cambios</button>
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
</div>



<!-- Latest compiled and minified JavaScript -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>
</body>
</html>
<?php
}
 ?>
