<?php
session_start();
if (!isset($_SESSION['taller'])) {
    ?>
    <script type="text/javascript">
        window.location.href = "../../";
    </script>
    <?php
} else {
    require "../../controller/C_Datos.php";
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Proyecto de FixCar"/>
        <meta name="keywords"
              content="taller, comparacion, distancia, precio, cambio de aceite, ruedas, piezas, reparacion, itv"/>
        <meta name="Cesur" content="Equipo">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
              crossorigin="anonymous">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="../assets/css/custom.css">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

        <title>FixCar</title>
    </head>
    <body>
    <!-- NAVBAR O MENU-->
    <div class="container-fluid">
        <nav class="row navbar navbar-expand-lg navbar-light bg-dark">
            <button class="navbar-toggler element" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Logo-->
            <a class="navbar-brand" href="../profiles/profile-workshop.php">
                <img class="logo" src="../assets/images/logos/logo.png" alt="Fixcar logo">
            </a>

            <div class="row collapse navbar-collapse" id="navbarNav">
                <div class="col-7">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <!-- Elemento Inicio -->
                            <a class="nav-link"
                               href="../profiles/profile-workshop.php"><?php echo $_SESSION['taller']; ?> - Inicio<span
                                    class="sr-only"></span></a>
                        </li>
                        <li class="nav-item">
                            <!-- Elemento log-out -->
                            <a class="nav-link" href="../../controller/logout.php"><i class="fas fa-power-off"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="row">
            <div class="col-md-10 col-sm-12 mx-auto">
                <div class="shadow-lg p-3 mt-3 mb-5 bg-white rounded">
                    <div class="jumbotron">
                        <h1 class="display-5 title-box">Hola taller!</h1>
                        <p class="lead text-justify">¿Estás interesado en hacerte Premium? Disfruta de nuestras ventajas por <span class="premium-price text-info font-weight-bold font-size-larger">34'95</span> al año. <span class="text-success font-weight-bold">¿A qué esperas?</span></p>
                        <hr class="my-4">
                        <p> <i class="far fa-heart heart-color"></i> Serás más visible a los usuarios</p>
                        <p> <i class="far fa-heart heart-color"></i> Tendrás más opciones de personalización</p>
                        <p> <i class="far fa-heart heart-color"></i> Podrás hacer uso de las subastas</p>
                        <p> <i class="far fa-heart heart-color"></i> ...</p>

                    </div>
                   <div class="col-md-6 mx-auto">
                       <form action="" method="post" action="../../controller/C_Talleres.php">
                           <div class="form-group text-muted">
                               <label for="credit-car ">Número de tu tarjeta</label>
                               <input type="text" class="form-control" id="credit-car">
                           </div>

                           <label class="text-muted" for="Expirity">Caducidad</label>
                           <div class="form-row text-muted" id="expirity">
                               <div class="col-md-6 col-sm-4">
                                   <input type="text" class="form-control mt-1" id="input-month" placeholder="Mes">
                               </div>
                               <div class="col-md-6 col-sm-4">
                                   <input type="text" class="form-control mt-1" id="input-year" placeholder="Año">
                               </div>
                           </div>

                           <div class="form-group text-muted mt-3">
                               <label for="phone">Número de teléfono</label>
                               <input type="text" class="form-control" id="phone">
                           </div>

                           <label class="text-muted" for="CVC">CVC</label>
                           <div class="form-row text-muted" id="">
                               <div class="col-md-6">
                                   <input type="password" class="form-control mt-2" id="CVC" placeholder="">
                               </div>
                               <buttom name="addpremium"  class="col-md-6 mb-5 mt-2 btn btn-block btn-info">
                                   Pagar ahora
                               </buttom>
                           </div>

                       </form>
                   </div>
                </div>
            </div>
        </div>

    </div>


    <!-- Latest compiled and minified JavaScript -->
    <script src="../../js/mapa.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACXVmPCf6pnuzVYfn4hm9KdVM4rS6_23U&callback=initMap"
            async defer></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"
            integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>
    </body>
    </html>
    <?php
}
?>
