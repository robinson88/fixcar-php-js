<?php
session_start();
if (!isset($_SESSION['usuario'])) {
?>
<script type="text/javascript">
    window.location.href="../../";
</script>
<?php
}else {
  require "../../controller/C_Datos.php";
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Proyecto de FixCar" />
    <meta name="keywords" content="taller, comparacion, distancia, precio, cambio de aceite, ruedas, piezas, reparacion, itv" />
    <meta name="Cesur" content="Equipo">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../assets/css/custom.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <title>FixCar</title>
</head>
<body>
<!-- NAVBAR O MENU-->
<div class="container-fluid">
  <nav class="row navbar navbar-expand-lg navbar-light bg-dark">
      <button class="navbar-toggler element" type="button" data-toggle="collapse" data-target="#navbarNav"
              aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Logo-->
      <a class="navbar-brand" href="../home-in/home-in.php">
        <img class="logo" src="../assets/images/logos/logo.png" alt="Fixcar logo">
      </a>

      <div class="row collapse navbar-collapse" id="navbarNav">
        <div class="col-7">
          <ul class="navbar-nav">
              <li class="nav-item">
                  <!-- Elemento Inicio -->
                  <a  class="nav-link" href="../home-in/home-in.php"><?php echo $_SESSION['usuario']; ?> - Inicio<span class="sr-only"></span></a>
              </li>
              <li class="nav-item d-flex">
                  <!-- Elemento recordatorio -->
                  <a class="nav-link" href="../profiles/edit-reminders.php">Recordatorios
                      <?php $n = 0;
                      while ($rowh = mysqli_fetch_array($consultav)) {
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['wheels_date']) && $rowh['wheels_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['wheels_date'])) {
                              $n++;
                          }
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['oil_date']) && $rowh['oil_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['oil_date'])) {
                              $n++;
                          }
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['review_date']) && $rowh['review_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['review_date'])) {
                              $n++;
                          }
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['date_itv']) && $rowh['date_itv'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['date_itv'])) {
                              $n++;
                          }
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['vehicle_insurance']) && $rowh['vehicle_insurance'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['vehicle_insurance'])) {
                              $n++;
                          }
                          ?>
                          <?php
                      }
                      if ($n != 0) {
                          ?>
                          <span class="reminder-badge navbar-reminder badge badge-danger badge-pill"> <?php echo $n; ?> </span>
                      <?php }
                      mysqli_data_seek($consultav, 0); ?>
                  </a>
              </li>
              <li class="nav-item">
                  <!-- Elemento Contacto -->
                  <a class="nav-link" href="favworkshops.php">Talleres favoritos</a>
              </li>
              <li class="nav-item active dropdown">
                  <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Perfiles </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="../profiles/profile-user.php">Mi perfil</a>
                      <a class="dropdown-item" href="../profiles/profile-vehicle.php">Mis vehículos</a>
                      <a class="dropdown-item" href="../profiles/edit-reminders.php">Mis recordatorios</a>
                      <a class="dropdown-item" href="../profiles/documents.php">Mis documentos</a>

                  </div>
              </li>
              <li class="nav-item">
                  <!-- Elemento Contacto -->
                  <a class="nav-link" href="../home-in/home-in.php#meet">Contacto</a>
              </li>
              <li class="nav-item">
                  <!-- Elemento log-out -->
                  <a class="nav-link" href="../../controller/logout.php"><i class="fas fa-power-off"></i></a>
              </li>
          </ul>
        </div>
      </div>
  </nav>


  <div class="row">
      <div id="vehicle" class="col-md-8 mx-auto"><br>
          <h3 class="register-heading text-uppercase mt-5 text-dark font-weight-bold">Mis vehículos</h3>
            <div id="carouselvehicles" class="carousel slide" interval="false">
              <div class="carousel-inner bg-dark">
                <?php  $i=1; while ($row = mysqli_fetch_array($consultav)){ ?>
                <div class="carousel-item <?php if ($i==1) { ?> active <?php } ?> ">
                  <div class="card-body">
                      <img class="d-block w-100 vehicle-profile-image" src="<?php if ($row['image'] == '') { echo "../assets/images/icons/car.png"; }else{ echo '../'.$row['image']; } ?>" alt="Foto vehículo">
                      <div class="carousel-caption col-lg-4 mx-auto bg-dark opacity-image">
                        <h5 class="card-title"> <?php echo $row['vehicle_band'].' '.$row['vehicle_model']; ?> </h5>
                        <p class="card-text"> <?php echo $row['vehicle_registration']; ?> </p>
                        <p class="card-text"><small class="text-muted"></small></p>
                        <a href="edit-profile-vehicle.php?id=<?php echo $row['idvehicle'] ?>" name="edit-little-vehicle" class="btn btn-md btn-secondary">Detalles</a>
                      </div>
                  </div>
                </div>
              <?php $i++; } ?>
              </div>
              <a class="carousel-control-prev" href="#carouselvehicles" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselvehicles" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>

            <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12 mx-auto mt-3 mb-3">
                <button type="submit" class="btn btn-dark btn-lg btn-block"><a href="./add-profile-vehicle.php">Añadir un vehículo</a></button>
            </div>
      </div>
  </div>
</div>



<!-- Latest compiled and minified JavaScript -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>
</body>
</html>
<?php
}
 ?>
