<?php
session_start();
if (!isset($_SESSION['taller'])) {
    ?>
    <script type="text/javascript">
        window.location.href = "../../";
    </script>
    <?php
} else {
    require "../../controller/C_Datos.php";
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Proyecto de FixCar"/>
        <meta name="keywords"
              content="taller, comparacion, distancia, precio, cambio de aceite, ruedas, piezas, reparacion, itv"/>
        <meta name="Cesur" content="Equipo">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
              crossorigin="anonymous">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="../assets/css/custom.css">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

        <title>FixCar</title>
    </head>
    <body>
    <!-- NAVBAR O MENU-->
    <div class="container-fluid">
        <nav class="row navbar navbar-expand-lg navbar-light bg-dark">
            <button class="navbar-toggler element" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Logo-->
            <a class="navbar-brand" href="../profiles/profile-workshop.php">
                <img class="logo" src="../assets/images/logos/logo.png" alt="Fixcar logo">
            </a>

            <div class="row collapse navbar-collapse" id="navbarNav">
                <div class="col-7">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <!-- Elemento Inicio -->
                            <a class="nav-link"
                               href="../profiles/profile-workshop.php"><?php echo $_SESSION['taller']; ?> - Inicio<span
                                        class="sr-only"></span></a>
                        </li>
                        <li class="nav-item d-flex">
                            <!-- Elemento comentarios -->
                            <a class="nav-link" href="../profiles/profile-workshop.php#comments">Comentarios</a>
                        </li>
                        <li class="nav-item d-flex">
                            <!-- Elemento subasta -->
                            <a class="nav-link" href="../profiles/auction-workshop.php">Subastas</a>
                        </li>
                        <!-- Elemento perfiles -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false"> Perfiles </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="../profiles/edit-profile-workshop.php">Mi perfil</a>
                                <?php if ($rowt['state'] == 'Premium') { ?>
                                    <a class="dropdown-item" href="">
                                        <form class="" action="../../controller/C_Talleres.php" method="post">
                                            <button type="submit" name="droppremium" class="btn btn-danger btn-block">
                                                Quiero dejar de ser PREMIUM
                                            </button>
                                        </form>
                                    </a>
                                <?php } else { ?>
                                    <a class="dropdown-item" href="../profiles/premium-way.php">
                                        <form class="btn btn-light btn-block a-styles-">Quiero ser PREMIUM
                                    </a>
                                <?php } ?>
                            </div>
                        </li>
                        <li class="nav-item d-flex">
                            <!-- Elemento recordatorio -->
                            <a class="nav-link" href="../profiles/profile-workshop.php#meet">Ayuda</a>
                        </li>
                        <li class="nav-item">
                            <!-- Elemento log-out -->
                            <a class="nav-link" href="../../controller/logout.php"><i class="fas fa-power-off"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Taller perfil, si tiene un readonly no se puede modificar-->
        <div class="row">
            <div class="col-md-8 mx-auto shadow-lg p-3 mt-5 bg-white rounded">
                <div class="row">
                    <img class="workshop-cover" width="100%" height="400px" src="../<?php echo $rowt['image']; ?>"
                         alt="portada">
                </div>
                <div class="row">
                    <div class="col-xl-6 col-md-12 mt-3">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="title-box mt-3"><?php echo $rowt['name']; ?>
                                    <?php if ($rowt['state'] == 'Premium') { ?>
                                        <i class="fas fa-crown premium-sign float-right"></i>
                                    <?php } ?>
                                </h1>
                                <div>
                                    <i class="workshop-stars rating-star fas fa-star"></i><i
                                            class="workshop-stars rating-star fas fa-star"></i><i
                                            class="workshop-stars  rating-star fas fa-star"></i><i
                                            class="workshop-stars rating-star fas fa-star"></i><i
                                            class="workshop-stars rating-star fas fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="row d-flex">
                            <?php if ($rowtt['mechanics'] == '1') { ?>
                                <div class="m-1 ">
                                    <span class="app-check"><i class="fas fa-check"></i></span> Mecánica
                                </div>
                            <?php } ?>
                            <?php if ($rowtt['repairs'] == '1') { ?>
                                <div class="m-1 ">
                                    <span class="app-check"><i class="fas fa-check"></i></span> Reparaciones
                                </div>
                            <?php } ?>
                            <?php if ($rowtt['review'] == '1') { ?>
                                <div class="m-1 ">
                                    <span class="app-check"><i class="fas fa-check"></i></span> Revisión
                                </div>
                            <?php } ?>
                            <?php if ($rowtt['bodywork'] == '1') { ?>
                                <div class="m-1 ">
                                    <span class="app-check"><i class="fas fa-check"></i></span> Chapa y pintura
                                </div>
                            <?php } ?>
                            <?php if ($rowtt['electricity'] == '1') { ?>
                                <div class="m-1 ">
                                    <span class="app-check"><i class="fas fa-check"></i></span> Electricidad
                                </div>
                            <?php } ?>
                            <?php if ($rowtt['creditcard'] == '1') { ?>
                                <div class="m-1 ">
                                    <span class="app-check"><i class="fas fa-check"></i></span> Pago con Tarjeta
                                </div>
                            <?php } ?>
                        </div>
                        <div class="row mt-3 mx-auto">
                            <div class="col-md-12">
                                <div class="address">
                                    <input id="address" class="address" type="text" size="25"
                                           value="<?php if ($rowt['adress'] == '') { ?>Dirección del taller <?php } else {
                                               echo $rowt['adress'];
                                           } ?>, <?php if ($rowt['location'] == '') { ?>Ciudad <?php } else {
                                               echo $rowt['location'];
                                           } ?>">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <?php if ($rowt['phone'] == '') { ?>Teléfono de tu taller <?php } else {
                                    echo $rowt['phone'];
                                } ?>
                            </div>
                            <div class="col-md-12">
                                <?php echo $rowt['email']; ?>
                            </div>
                            <div class="col-md-12 mt-2 mb-2 text-justify">
                                <?php if ($rowt['description'] == '') { ?>Acude a tu <a
                                        href="edit-profile-workshop.php">configuración del
                                    perfil</a> para que aparezca aquí la descripción <?php } else {
                                    echo $rowt['description'];
                                } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-12 mt-3" style="align-items: center;display: flex;">
                        <?php if ($rowt['state'] == 'Premium') { ?>
                            <!-- 4:3 aspect ratio -->
                            <div class="embed-responsive embed-responsive-4by3" width="100%">
                                <iframe width="560" height="315"
                                        src="<?php echo 'https://www.youtube.com/embed/' . $rowt['video']; ?>"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                            </div>
                        <?php } ?>
                    </div>
                    <?php if ($rowt['state'] == 'Premium') { ?>
                        <div class="col-md-12 col-xl-12 col-lg-12 mx-auto mapa mt-3" width="100%">
                            <div class="container" id="map">
                                <!--<a href=""><img src="../assets/images/workshop-images/map-example.jpg" alt=""></a>-->
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 mx-auto shadow-lg p-3 mt-5 bg-white">
                <div class="row p-3" id="comments">
                    <h1 class="title-box ml-5 mt-3">Reseñas</h1>
                    <?php if (isset($_GET['reply'])) { ?>
                        <form action="../../controller/C_Comentarios.php?id=<?php echo $_GET['id'];
                        if (isset($_GET['reply'])) { ?>&reply=<?php echo $_GET['reply'];
                        } ?>" method="post">
                            <p>
                                <textarea name="commentary" rows="5" cols="80"></textarea>
                            </p>
                            <p>
                                <input type="submit" <?php if (isset($_GET['reply'])) { ?> name="postreply"  value="Responder" <?php } else { ?>name="postcomment" value="Comentar"<?php } ?>>
                            </p>
                        </form>
                    <?php } ?>

                    <?php foreach ($consultacw as $rowcw) : ?>
                        <?php
                        $NuevaFecha = strtotime('+2 hour', strtotime($rowcw['create_date']));
                        $comdate = date('Y-m-d H:i:s', $NuevaFecha);
                        $NuevaFechaEd = strtotime('+2 hour', strtotime($rowcw['update_date']));
                        $edcomdate = date('Y-m-d H:i:s', $NuevaFechaEd);
                        ?>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 card card-white post p-1 mb-3">
                                    <div class="post-heading">
                                        <div class="float-left image">
                                            <img src="<?php
                                            //Condiciones para mostrar avatar
                                            foreach ($consultauc as $rowuc) {
                                                if ($rowuc['iduser'] == $rowcw['idusers']) {
                                                    if ($rowuc['image'] != '') {
                                                        echo '../' . $rowuc['image'];
                                                    }
                                                    if ($rowuc['image'] == '') {
                                                        echo '../assets/images/icons/user-icon.png';
                                                    }
                                                }
                                            }
                                            ?>" class="img-circle avatar" alt="Avatar">
                                        </div>
                                        <div class="float-left meta">
                                            <div class="title h5">
                                                <a href="#"><b>
                                                        <?php
                                                        foreach ($consultauc as $rowuc) {
                                                            if ($rowuc['iduser'] == $rowcw['idusers']) {
                                                                echo $rowuc['name'];
                                                            }
                                                            if ($rowwc['idworkshop'] == $rowcw['idusers'] && $rowcw['workshopname'] != '') {
                                                                echo $rowwc['name'];
                                                            }
                                                        } ?>
                                                    </b></a>
                                            </div>
                                            <h6 class="text-muted time">Publicado: <?php echo $comdate ?></h6>

                                            <h6 class="text-muted time">
                                                Editado: <?php if ($edcomdate != '-0001-11-30 02:00:00') echo $edcomdate; ?></h6>
                                        </div>
                                    </div>
                                    <div class="post-description">
                                        <p> <?php echo $rowcw['commentary']; ?></p>
                                    </div>
                                    <div class="row">
                                        <div class="col-8 ml-auto">
                                            <form class="float-right ml-2"
                                                  action="../../controller/C_Comentarios.php?id=<?php echo $_GET['id']; ?>"
                                                  method="post">
                                                <input type="hidden" name="idcommentary"
                                                       value="<?php echo $rowcw['idcommentary']; ?>">
                                                <button class="btn btn-danger" type="submit" name="deletecomment"
                                                        value="Eliminar reseña">Eliminar reseña
                                                </button>
                                            </form>
                                            <a class="btn btn-warning float-right ml-2 text-white"
                                               href="editreply.php?id=<?php echo $_GET['id'] ?>&edit=<?php echo $rowcw['idcommentary']; ?>">Editar
                                                reseña</a>

                                            <a class="btn btn-info float-right"
                                               href="?id=<?php echo $_SESSION['idtaller']; ?>&reply=<?php echo $rowcw['idcommentary']; ?>#comments">Responder</a>
                                        </div>
                                    </div>

                                    <!-- respuesta -->

                                    <?php
                                    $countrw = $comentario2->workshopid_reply($rowcw['idcommentary']);
                                    $replycount = mysqli_num_rows($countrw);
                                    if ($replycount != '0') : ?>
                                        <?php $consultarw = $comentario2->workshopid_reply($rowcw['idcommentary']);
                                        while ($rowrw = mysqli_fetch_array($consultarw)) : ?>
                                            <?php $ResFecha = strtotime('+2 hour', strtotime($rowrw['create_date']));
                                            $repdate = date('Y-m-d H:i:s', $ResFecha);
                                            $EdResFecha = strtotime('+2 hour', strtotime($rowrw['update_date']));
                                            $edrepdate = date('Y-m-d H:i:s', $EdResFecha);
                                            ?>
                                            <div class="row" style="padding: 30px;">
                                            <div class="col-12 card card-white post p-1 mb-3"
                                                 style="background: #E3E3E3;">
                                                <div class="post-heading">
                                                    <div class="float-left image">
                                                        <?php
                                                        //Condiciones para mostrar avatar, ya sea de usuario o taller
                                                        foreach ($consultauc as $rowuc) {
                                                            if ($rowuc['iduser'] == $rowrw['idusers']) {
                                                                if ($rowuc['image'] != '') {
                                                                    $imageSrc = '../' . $rowuc['image'];
                                                                }
                                                                if ($rowuc['image'] == '') {
                                                                    $imageSrc = '../assets/images/icons/user-icon.png';
                                                                }
                                                            }
                                                        }
                                                        foreach ($consultawc as $rowwc) {
                                                            if ($rowwc['name'] == $rowrw['workshopname']) {
                                                                if ($rowwc['image'] != '') {
                                                                    $imageSrc = '../' . $rowwc['image'];
                                                                }
                                                                if ($rowwc['image'] == '') {
                                                                    $imageSrc = '../assets/images/background/workshop.jpg';
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                        <img src="<?= $imageSrc ?>"
                                                             class="img-circle avatar" alt="Avatar">
                                                    </div>
                                                    <div class="float-left meta">
                                                        <div class="title h5">
                                                            <a href="#"><b>
                                                                    <?php foreach ($consultauc as $rowuc) {
                                                                        if ($rowuc['iduser'] == $rowrw['idusers']) {
                                                                            echo $rowuc['name'];
                                                                        }
                                                                    } ?>
                                                                    <?php if ($rowrw['workshopname'] != '') {
                                                                        echo 'Comentario del taller';
                                                                    } ?>
                                                                    <?php foreach ($consultawc as $rowwc) {
                                                                        if ($rowwc['name'] == $rowrw['workshopname']) {
                                                                            echo $rowwc['name'];
                                                                        }
                                                                    } ?>
                                                                </b></a>
                                                        </div>
                                                        <h6 class="text-muted time">
                                                            Publicado: <?php echo $comdate ?></h6>

                                                        <h6 class="text-muted time">
                                                            Editado: <?php if ($edcomdate != '-0001-11-30 02:00:00') echo $edcomdate; ?></h6>
                                                    </div>
                                                </div>
                                                <div class="post-description">
                                                    <p> <?php echo $rowrw['commentary'] ?></p>
                                                </div>

                                                <?php /* aquí están todas las condiciones para que se muestre la edición, dependiendo de quién esté entrando */
                                                if (isset($_SESSION['idusuario']) && $_SESSION['idusuario'] == $rowrw['idusers'] || isset($_SESSION['idtaller']) && $_SESSION['taller'] == $rowrw['workshopname']) :
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-8 ml-auto">
                                                            <form class="float-right ml-2"
                                                                  action="../../controller/C_Comentarios.php?id=<?php echo $_SESSION['idtaller']; ?>"
                                                                  method="post">
                                                                <input type="hidden" name="idcommentary"
                                                                       value="<?php echo $rowrw['idcommentary']; ?>">
                                                                <button class="btn btn-danger" type="submit"
                                                                        name="deletecomment"
                                                                        value="Eliminar reseña">Eliminar respuesta
                                                                </button>
                                                            </form>
                                                            <a class="btn btn-warning float-right ml-2 text-white"
                                                               href="../search-in/editreply.php?id=<?php echo $_SESSION['idtaller'] ?>&edit=<?php echo $rowrw['idcommentary'] ?>">Editar
                                                                respuesta</a>

                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            </div>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>


    <!-- Latest compiled and minified JavaScript -->
    <script src="../../js/mapa.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACXVmPCf6pnuzVYfn4hm9KdVM4rS6_23U&callback=initMap"
            async defer></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"
            integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>
    </body>
    </html>
    <?php
}
?>
