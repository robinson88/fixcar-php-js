<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Proyecto de FixCar"/>
    <meta name="keywords" content="taller, comparacion, distancia, precio, cambio de aceite, ruedas, piezas, reparacion, itv"/>
    <meta name="Cesur" content="Equipo">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../assets/css/custom.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

    <title>FixCar</title>
</head>
<body>
<!-- NAVBAR O MENU-->

<div class="container-fluid">
    <nav class="row navbar navbar-expand-lg navbar-light bg-dark">
        <!-- Logo-->
        <a class="navbar-brand" href="../../">
            <img class="logo" src="../assets/images/logos/logo.png" alt="Fixcar logo">
        </a>
        <button class="navbar-toggler element" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <!-- Elemento Inicio -->
                    <a  class="nav-link" href="../../">Inicio <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <!-- Elemento Nosotros -->
                    <a class="nav-link" href="../../#about">Nosotros</a>
                </li>
                <li class="nav-item">
                    <!-- Elemento Descarga la App -->
                    <a class="nav-link" href="../../#download">App</a>
                </li>
                <li class="nav-item">
                    <!-- Elemento Contacto -->
                    <a class="nav-link" href="../../#meet">Contacto</a>
                </li>
                <!-- Elemento Registro -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Registro </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../register-options/register-user.php">Registro de usuario</a>
                        <a class="dropdown-item" href="../register-options/register-workshop.php">Registro de taller</a>
                    </div>
                </li>
                <li class="nav-item dropdown active">
                    <!-- Elemento Login -->
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Login </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../login/login-user.php">Soy un usuario</a>
                        <a class="dropdown-item" href="../login/login-workshop.php">Soy un taller</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <div class="row">
        <div class="col-md-5 col-sm-6 col-lg-3 mx-auto mt-5">
            <form class="shadow-lg p-3 mb-7 bg-white rounded" action="../../controller/C_RecuperacionPass.php" method="post">
                <div class="login-box">
                    <img class="login-image" src="../assets/images/icons/gear.png" alt="login img">
                </div>
                <div class="form-group">
                    <h3 class="text-justify m-1">He olvidado mi contraseña</h3>
                    <label for="email">Introduce tu correo</label>
                    <input type="email" class="form-control" name="email" id="email">
                    <small id="emailHelp" class="form-text text-muted">Introduce tu email para reestablecer tu contraseña</small>
                </div>
                <button type="submit" class="btn btn-dark btn-lg btn-block" name="recovery_pass">Enviar</button>
            </form>
        </div>
    </div>
</div>

<!-- Latest compiled and minified JavaScript -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


</body>

</html>