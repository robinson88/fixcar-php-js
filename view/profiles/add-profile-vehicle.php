<?php
session_start();
if (!isset($_SESSION['usuario'])) {
?>
<script type="text/javascript">
    window.location.href="../../";
</script>
<?php
}else {
  require "../../controller/C_Datos.php";
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Proyecto de FixCar" />
    <meta name="keywords" content="taller, comparacion, distancia, precio, cambio de aceite, ruedas, piezas, reparacion, itv" />
    <meta name="Cesur" content="Equipo">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../assets/css/profile-user.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

    <title>FixCar</title>
</head>
<body>
<!-- NAVBAR O MENU-->
<div class="container-fluid">
  <nav class="row navbar navbar-expand-lg navbar-light bg-dark">
      <button class="navbar-toggler element" type="button" data-toggle="collapse" data-target="#navbarNav"
              aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Logo-->
      <a class="navbar-brand" href="../home-in/home-in.php">
        <img class="logo" src="../assets/images/logos/logo.png" alt="Fixcar logo">
      </a>

      <div class="row collapse navbar-collapse" id="navbarNav">
        <div class="col-7">
          <ul class="navbar-nav">
              <li class="nav-item">
                  <!-- Elemento Inicio -->
                  <a  class="nav-link" href="../home-in/home-in.php"><?php echo $_SESSION['usuario']; ?> - Inicio<span class="sr-only"></span></a>
              </li>

              <li class="nav-item">
                  <!-- Elemento Vehículo -->
                  <a class="nav-link" href="../profiles/profile-vehicle.php">Volver a mis vehículos</a>
              </li>
              <li class="nav-item active">
                  <!-- Elemento Vehículo -->
                  <a class="nav-link " href="../profiles/add-profile-vehicle.php">Añadir vehículos</a>
              </li>
          </ul>
        </div>
      </div>
  </nav>

  <div class="row">
      <div class="col-md-8 mx-auto shadow-lg p-3 mt-5 bg-white rounded">

        <form class="row mt-5 register-form" action="../../controller/C_Vehiculos.php" method="post" enctype="multipart/form-data">
            <div class="col-md-12 mx-auto mb-5">
                <h3 class="text-uppercase text-dark font-weight-bold">Añade un vehículo</h3>
            </div><br>

              <div class="col-md-6">
                  <div class="form-group">
                      <input type="text" class="form-control" name="vehicle_band" placeholder="Marca del vehículo"/>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                      <input type="text" class="form-control" name="vehicle_model" placeholder="Modelo del vehículo"/>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                      <input type="text" class="form-control" name="vehicle_registration" placeholder="Matrícula"/>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                      <input type="text" class="form-control" name="vehicle_engine" placeholder="Motor"/>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                      <input type="text" class="form-control" name="vehicle_km" placeholder="Kilómetros"/>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="">
                      <div class="custom-file border-light btn btn-light text-dark">
                          <input type="file" name="file"/>
                          <label for="file" class="btn-2 text-dark">Adjuntar foto</label>
                      </div>
                  </div>
              </div>
          <div class="col-md-6 mx-auto mt-5 d-flex">
              <button type="submit" name="addvehicle" class="btn btn-dark btn-lg btn-block">Guardar Cambios</button>
          </div>
      </form>
    </div>
  </div>
</div>



<!-- Latest compiled and minified JavaScript -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>
</body>
</html>
<?php
}
?>
