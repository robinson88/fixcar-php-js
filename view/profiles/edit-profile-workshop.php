<?php
session_start();
if (!isset($_SESSION['taller'])) {
?>
<script type="text/javascript">
    window.location.href="../../";
</script>
<?php
}else {
require "../../controller/C_Datos.php";
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Proyecto de FixCar" />
    <meta name="keywords" content="taller, comparacion, distancia, precio, cambio de aceite, ruedas, piezas, reparacion, itv" />
    <meta name="Cesur" content="Equipo">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../assets/css/custom.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

    <title>FixCar</title>
</head>
<body>
<!-- NAVBAR O MENU-->
<div class="container-fluid">
  <nav class="row navbar navbar-expand-lg navbar-light bg-dark">
      <button class="navbar-toggler element" type="button" data-toggle="collapse" data-target="#navbarNav"
              aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Logo-->
      <a class="navbar-brand" href="../profiles/profile-workshop.php">
        <img class="logo" src="../assets/images/logos/logo.png" alt="Fixcar logo">
      </a>

      <div class="row collapse navbar-collapse" id="navbarNav">
        <div class="col-7">
          <ul class="navbar-nav">
              <li class="nav-item">
                  <!-- Elemento Inicio -->
                  <a  class="nav-link" href="../profiles/profile-workshop.php"><?php echo $_SESSION['taller']; ?> - Inicio<span class="sr-only"></span></a>
              </li>
              <li class="nav-item d-flex">
                  <!-- Elemento recordatorio -->
                  <a class="nav-link" href="../profiles/profile-workshop.php#comments">Comentarios</a>
              </li>
              <li class="nav-item d-flex">
                  <!-- Elemento recordatorio -->
                  <a class="nav-link" href="../profiles/auction-workshop.php">Subastas</a>
              </li>
              <li class="nav-item dropdown active">
                  <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown"
                     aria-haspopup="true" aria-expanded="false"> Perfiles </a>
                     <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                         <a class="dropdown-item" href="../profiles/edit-profile-workshop.php">Mi perfil</a>
                         <?php if ($rowt['state'] == 'Premium') {?>
                         <a class="dropdown-item" href=""><form class="" action="../../controller/C_Talleres.php" method="post">
                           <button type="submit" name="droppremium" class="btn btn-dark btn-block">Quiero dejar de ser PREMIUM</button>
                         </form></a>
                       <?php }else{ ?>
                         <a class="dropdown-item" href=""><form class="" action="../../controller/C_Talleres.php" method="post">
                           <button type="submit" name="addpremium" class="btn btn-dark btn-block">Quiero ser PREMIUM</button>
                         </form></a>
                       <?php } ?>
                     </div>
              </li>
              <li class="nav-item d-flex">
                  <!-- Elemento recordatorio -->
                  <a class="nav-link" href="../profiles/profile-workshop.php#meet">Ayuda</a>
              </li>
              <li class="nav-item">
                  <!-- Elemento log-out -->
                  <a class="nav-link" href="../../controller/logout.php"><i class="fas fa-power-off"></i></a>
              </li>
          </ul>
        </div>
      </div>
  </nav>

    <!-- Taller perfil, si tiene un readonly no se puede modificar-->
    <div class="row">
        <div class="col-md-8 mx-auto shadow-lg p-3 mt-5 bg-white rounded">
            <form class="row mt-5 register-form" action="../../controller/C_Talleres.php" method="post" enctype="multipart/form-data">
                <div class="col-md-12 mx-auto mb-5">
                    <h3>Mi perfil</h3>
                </div><br>
                <div class="col-md-12">
                    <div class="form-group">
                      <img class="workshop-cover" width="100%" height="400px" src="../<?php echo $rowt['image']; ?>">
                    </div>
                <div class="col-md-12 mb-2 mt-1">
                  <input type="file" name="workshoppic">
                  <!--
                  <div class="col-md-12">
                      <div class="custom-file">
                          <input type="file" class="upload-input" name="file">
                          <label class="custom-file-label" for="upload-input">Adjuntar archivos</label>
                      </div>
                  </div>
                  <div class="col-md-1">
                      <button class="btn btn-lg btn-file" type="submit">
                          <i class="fas fa-upload"></i>
                      </button>
                  </div>
                  -->
                </div>
                </div>
                <div class="col-md-12 mb-2 mt-1">
                  <?php if ($rowt['state'] == 'Premium') { ?>
                    <input class="form-control" type="text" name="video" placeholder="Inserta aquí el código de un vídeo de youtube" value="<?php if ($rowt['video'] == '') { ?><?php }else{ echo $rowt['video']; } ?>"/>
                  <?php } ?>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" class="form-control" name="cif" value="<?php if ($rowt['cif'] == '') { ?>CIF <?php }else{ echo $rowt['cif']; } ?>" readonly/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" class="form-control" name="namework" value="<?php if ($rowt['name'] == '') { ?>Nombre del taller <?php }else{ echo $rowt['name']; } ?>" readonly/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" class="form-control" name="telefono" value="<?php if ($rowt['phone'] == '') { ?>Teléfono de tu taller <?php }else{ echo $rowt['phone']; } ?>"/>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" class="form-control" name="direccion" value="<?php if ($rowt['adress'] == '') { ?>Dirección <?php }else{ echo $rowt['adress']; } ?>"/>

                        <!--
                        .............................
                        INPUT HIDDEN PARA COORDENADAS
                        .............................
                        -->
                        <input id ="address4" type="hidden" class="form-control" name="nada" value="<?php echo $rowt['adress'].', '.$rowt['location']; ?>" readonly/>
                        <div class="row" id="map"></div>
                        <input type="hidden" id="lat" type="" name="lat" value=""/>
                        <input type="hidden" id="long" type="" name="long" value=""/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" class="form-control" name="cp" value="<?php if ($rowt['cp'] == '') { ?>Código postal <?php }else{ echo $rowt['cp']; } ?>"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" class="form-control" name="ciudad" value="<?php if ($rowt['location'] == '') { ?>Ciudad <?php }else{ echo $rowt['location']; } ?>"/>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <textarea class="form-control" id="description" name="descripcion" placeholder="Describe tu taller brevemente" cols="30" rows="3"><?php if ($rowt['description'] != '') { echo $rowt['description']; } ?></textarea>
                    </div>
                </div>
                <!-- Aquí empieza el chechbox de filtros de búsqueda -->
                <div class="col-md-12 mb-2">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="mech" value="Mecánica " id="oil-check" <?php if ($rowtt['mechanics'] == '1') { echo "checked"; } ?>>
                        <label class="form-check-label" for="oil-check">
                          Mecánica
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="elec" value="Electricidad " id="itv-check" <?php if ($rowtt['electricity'] == '1') { echo "checked"; } ?>>
                        <label class="form-check-label" for="itv-check">
                          Electricidad
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="repair" value="Reparaciones " id="repair-check" <?php if ($rowtt['repairs'] == '1') { echo "checked"; } ?>>
                        <label class="form-check-label" for="repair-check">
                          Reparaciones
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="overhaul" value="Revisión " id="overhaul-check" <?php if ($rowtt['review'] == '1') { echo "checked"; } ?>>
                        <label class="form-check-label" for="overhaul-check">
                          Revisión
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="carbody" value="Chapa y pintura " id="clean-check" <?php if ($rowtt['bodywork'] == '1') { echo "checked"; } ?>>
                        <label class="form-check-label" for="clean-check">
                          Chapa y pintura
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="credit" value="Pago con tarjeta " id="credit-check" <?php if ($rowtt['creditcard'] == '1') { echo "checked"; } ?>>
                        <label class="form-check-label" for="credit-check">
                          Pago con tarjeta
                        </label>
                    </div>
                    <!-- Aquí acaba el chechbox de filtros de búsqueda -->
                </div>
                <div class="col-md-3 mx-auto mt-5">
                    <button type="submit" name="updatetaller" class="btn btn-dark btn-lg btn-block">Actualizar</button>
                </div>

            </form>
        </div>
    </div>
</div>
     <script>

    function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 8,
      center: {lat: 40.0000000, lng: -4.0000000}
    });
    var geocoder = new google.maps.Geocoder();
    geocodeAddress(geocoder, map);
    /*document.getElementById('submit').addEventListener('click', function() {
      geocodeAddress(geocoder, map);
    });*/
  }

  function geocodeAddress(geocoder, resultsMap) {
    var address = document.getElementById('address4').value;
    geocoder.geocode({'address': address}, function(results, status) {
      if (status === 'OK') {
        resultsMap.setCenter(results[0].geometry.location);

  //--------------------------------------------------------------------------------------
        direc=results[0].formatted_address;//nombre de calle
                latlong=results[0].geometry.location;// coordenadas lat y long
                info=[direc,latlong];// direccion mas coordenadas
                //se establece el atributo value del input con las coordenadas
                console.log(info);
                lat=results[0].geometry.location.lat();
                long=results[0].geometry.location.lng();
                var elemento1 = document.querySelector('#lat');
                var elemento2 = document.querySelector('#long');
                elemento1.setAttribute("value", lat);
                elemento2.setAttribute("value", long);
// ---------------------------------------------------------------------------------------

        var marker = new google.maps.Marker({
          map: resultsMap,
          position: results[0].geometry.location
        });
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
  }

    </script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACXVmPCf6pnuzVYfn4hm9KdVM4rS6_23U&callback=initMap"
              async defer></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>
</body>
</html>
<?php
}
 ?>
