<?php
session_start();
if (!isset($_SESSION['usuario'])) {
?>
<script type="text/javascript">
    window.location.href="../../";
</script>
<?php
}else {
  require "../../controller/C_Datos.php";
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Proyecto de FixCar" />
    <meta name="keywords" content="taller, comparacion, distancia, precio, cambio de aceite, ruedas, piezas, reparacion, itv" />
    <meta name="Cesur" content="Equipo">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../assets/css/custom.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

    <title>FixCar</title>
</head>
<body>
<!-- NAVBAR O MENU-->
<div class="container-fluid">
  <nav class="row navbar navbar-expand-lg navbar-light bg-dark">
      <button class="navbar-toggler element" type="button" data-toggle="collapse" data-target="#navbarNav"
              aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Logo-->
      <a class="navbar-brand" href="../home-in/home-in.php">
        <img class="logo" src="../assets/images/logos/logo.png" alt="Fixcar logo">
      </a>

      <div class="row collapse navbar-collapse" id="navbarNav">
        <div class="col-7">
          <ul class="navbar-nav">
              <li class="nav-item">
                  <!-- Elemento Inicio -->
                  <a  class="nav-link" href="../home-in/home-in.php"><?php echo $_SESSION['usuario']; ?> - Inicio<span class="sr-only"></span></a>
              </li>
              <li class="nav-item d-flex">
                  <!-- Elemento recordatorio -->
                  <a class="nav-link" href="../profiles/edit-reminders.php">Recordatorios
                      <?php $n = 0;
                      while ($rowh = mysqli_fetch_array($consultav)) {
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['wheels_date']) && $rowh['wheels_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['wheels_date'])) {
                              $n++;
                          }
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['oil_date']) && $rowh['oil_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['oil_date'])) {
                              $n++;
                          }
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['review_date']) && $rowh['review_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['review_date'])) {
                              $n++;
                          }
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['date_itv']) && $rowh['date_itv'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['date_itv'])) {
                              $n++;
                          }
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['vehicle_insurance']) && $rowh['vehicle_insurance'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['vehicle_insurance'])) {
                              $n++;
                          }
                          ?>
                          <?php
                      }
                      if ($n != 0) {
                          ?>
                          <span class="reminder-badge navbar-reminder badge badge-danger badge-pill"> <?php echo $n; ?> </span>
                      <?php }
                      mysqli_data_seek($consultav, 0); ?>
                  </a>
              </li>
              <li class="nav-item">
                  <!-- Elemento Contacto -->
                  <a class="nav-link" href="../profiles/favworkshops.php">Talleres favoritos</a>
              </li>
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Perfiles </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="../profiles/profile-user.php">Mi perfil</a>
                      <a class="dropdown-item" href="../profiles/profile-vehicle.php">Mis vehículos</a>
                      <a class="dropdown-item" href="../profiles/edit-reminders.php">Mis recordatorios</a>
                      <a class="dropdown-item" href="../profiles/documents.php">Mis documentos</a>

                  </div>
              </li>
              <li class="nav-item">
                  <!-- Elemento Contacto -->
                  <a class="nav-link" href="../home-in/home-in.php#meet">Contacto</a>
              </li>
              <li class="nav-item">
                  <!-- Elemento log-out -->
                  <a class="nav-link" href="../../controller/logout.php"><i class="fas fa-power-off"></i></a>
              </li>
          </ul>
        </div>
          <div class="col-md-12 col-lg-5 d-flex">
              <form class="form-inline ml-auto p-2">
                <div class="row d-flex">
                  <div class="">
                    <button class="btn btn-car form-control-button search-filter dropdown">
                      <!-- Elemento de búsqueda por filtros-->
                        <a class="nav-link dropdown-toggle filter-text" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Filtrar búsqueda </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <div class="col-md-12">
                              <div class="form-check filter-list">
                                  <input class="form-check-input" type="checkbox" name="oil" value="" id="oil-check">
                                  <label class="form-check-label" for="oil-check">
                                      Mecánica
                                  </label>
                              </div>
                              <div class="form-check filter-list">
                                  <input class="form-check-input move-left" type="checkbox" name="itv" value="" id="itv-check">
                                  <label class="form-check-label" for="itv-check">
                                      Electricidad
                                  </label>
                              </div>
                              <div class="form-check filter-list">
                                  <input class="form-check-input move-left" type="checkbox" name="repair" value="" id="repair-check">
                                  <label class="form-check-label" for="repair-check">
                                      Reparaciones
                                  </label>
                              </div>
                              <div class="form-check filter-list">
                                  <input class="form-check-input move-left" type="checkbox" name="overhaul" value="" id="overhaul-check">
                                  <label class="form-check-label" for="overhaul-check">
                                      Revisión
                                  </label>
                              </div>
                              <div class="form-check filter-list">
                                  <input class="form-check-input move-left" type="checkbox" name="clean" value="" id="clean-check">
                                  <label class="form-check-label" for="clean-check">
                                      Chapa y pintura
                                  </label>
                              </div>
                              <div class="form-check filter-list">
                                  <input class="form-check-input move-left" type="checkbox" name="credit" value="" id="credit-check">
                                  <label class="form-check-label" for="credit-check">
                                      Pago con tarjeta
                                  </label>
                              </div>
                              <div class="form-check filter-list">
                                  <input class="form-check-input move-left" type="checkbox" name="credit" value="" id="credit-check">
                                  <label class="form-check-label" for="credit-check">
                                      PREMIUM
                                  </label>
                              </div>
                              <!-- Aquí acaba el chechbox de filtros de búsqueda -->
                          </div>
                        </div>
                    </button>
                  </div>
                  <div class="ajust-l">
                    <a class="btn btn-car filter" href="./inline.php">
                      <i class="fas fa-car-side"></i>
                    </a>
                  </div>
                </div>
              </form>
          </div>
      </div>
  </nav>
  <div class="row">
      <div class="col-md-8 mx-auto shadow-lg p-3 mt-5 bg-white rounded">
        <div class="row">
          <img class="workshop-cover" width="100%" height="300px" src="../<?php echo $rowdt['image']; ?>" alt="portada">
        </div>
        <div class="row d-flex">
            <div class="col-xl-6 mt-3">
              <div class="d-flex">
                <div>
                  <h1 class="title-box mt-3"><?php echo $rowdt['name']; ?>
                      <?php if ($rowdt['state'] == 'Premium') {?>
                           <i class="fas fa-crown premium-sign pl-2"></i>
                      <?php } ?>
                  </h1>
                  <p>
                    <div class="ml-2 mt-3 rating">
                      <i class="fas fa-2x fa-star" <?php if ($media >= 5) { echo 'style="color: #ffc107"'; } ?>></i>
                      <i class="fas fa-2x fa-star" <?php if ($media >= 4) { echo 'style="color: #ffc107"'; } ?>></i>
                      <i class="fas fa-2x fa-star" <?php if ($media >= 3) { echo 'style="color: #ffc107"'; } ?>></i>
                      <i class="fas fa-2x fa-star" <?php if ($media >= 2) { echo 'style="color: #ffc107"'; } ?>></i>
                      <i class="fas fa-2x fa-star" <?php if ($media >= 1) { echo 'style="color: #ffc107"'; } ?>></i>
                    </div>
                    <?php if ($countratingsn > 0) {
                      echo " Votos: ".$countratingsn." (".$mediadec.")";
                     } ?>
                  </p>
                  <!-- Favoritos -->
                  <form action="../../controller/C_Favoritos.php" method="post">
                      <input type="hidden" name="idusuario" value=" <?php echo $_SESSION['idusuario']?>">
                      <input type="hidden" name="id" value="<?php echo $_GET['id']?>">
                    <?php if ($countfav == 1) {?>
                        <button class="botonfavoritos" type="submit" name="dropfav"><i class="fa fa-heart heart-color"></i> Eliminar de favoritos </button>
                    <?php } else {?>
                       <button class="botonfavoritos" type="submit" name="addfav"><i class="far fa-heart heart-color"></i> Añadir a favoritos </button>
                    <?php }?>
                  </form>
                    <!--FIN FAVORITOS -->
                </div>
              </div>
              <div class="row d-flex">
                <?php if ($rowtt['mechanics'] == '1') { ?>
                  <div class="m-1 ">
                      <span class="app-check"><i class="fas fa-check"></i></span> Mecánica
                  </div>
                <?php } ?>
                <?php if ($rowtt['repairs'] == '1') { ?>
                  <div class="m-1 ">
                      <span class="app-check"><i class="fas fa-check"></i></span> Reparaciones
                  </div>
                <?php } ?>
                <?php if ($rowtt['review'] == '1') { ?>
                  <div class="m-1 ">
                      <span class="app-check"><i class="fas fa-check"></i></span> Revisión
                  </div>
                <?php } ?>
                <?php if ($rowtt['bodywork'] == '1') { ?>
                  <div class="m-1 ">
                      <span class="app-check"><i class="fas fa-check"></i></span> Chapa y pintura
                  </div>
                <?php } ?>
                <?php if ($rowtt['electricity'] == '1') { ?>
                  <div class="m-1 ">
                      <span class="app-check"><i class="fas fa-check"></i></span> Electricidad
                  </div>
                <?php } ?>
                <?php if ($rowtt['creditcard'] == '1') { ?>
                  <div class="m-1 ">
                      <span class="app-check"><i class="fas fa-check"></i></span> Pago con Tarjeta
                  </div>
                <?php } ?>
              </div>

                <!-- favoritos
                  <div >
                  <form action="../../controller/C_Favoritos.php" method="post">
                        <input type="hidden" name="idusuario" value=" <?php echo $_SESSION['idusuario']?>">
                        <input type="hidden" name="id" value="<?php echo $_GET['id']?>">
                       <button class="botonfavoritos" type="submit" name="añadirfav"> </button>
                  </form>

                  </div>
          fin favoritos------------------------------------------------------------------------------------------------- -->
              <div class="row mt-3 mx-auto">
                <div class="col-md-10">
                 <div class="address">
                   <input id="address" class="address" type="text" size="50" value="<?php if ($rowdt['adress'] == '') { ?>Dirección del taller <?php }else{ echo $rowdt['adress']; } ?>, <?php if ($rowdt['location'] == '') { ?>Ciudad <?php }else{ echo $rowdt['location']; } ?>">
                 </div>
                </div>
                <div class="col-md-10">
                  <?php if ($rowdt['phone'] == '') { ?>Teléfono de tu taller <?php }else{ echo $rowdt['phone']; } ?>
                </div>
                <div class="col-md-10">
                  <?php echo $rowdt['email']; ?>
                </div>
                <div class="col-md-10 mt-4 mb-4">
                  <?php echo $rowdt['description']; ?>
                  
                </div>
            </div>
          </div>
          <div class="col-xl-6 col-md-12 mt-3" style="align-items: center;display: flex;">
              <?php if ($rowdt['state'] == 'Premium') { ?>
              <!-- 4:3 aspect ratio -->
              <div class="embed-responsive embed-responsive-4by3" width="100%">
                  <iframe width="560" height="315" src="<?php echo 'https://www.youtube.com/embed/'.$rowdt['video']; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
              <?php } ?>
          </div>
          <?php if ($rowdt['state'] == 'Premium') { ?>
              <div class="col-md-12 col-xl-12 col-lg-12 mx-auto Mapa3 mt-3" width="100%">
                  <div class="container" id="map3">
                      <!--<a href=""><img src="../assets/images/workshop-images/map-example.jpg" alt=""></a>-->
                  </div>
              </div>
          <?php } ?>
        </div>
      </div>
  </div>
  
  <div class="row">
    <div class="col-md-8 mx-auto shadow-lg p-3 mt-5 bg-white">
      <div class="row" id="comments">
          <h1 class="title-box ml-5 mt-3">Reseñas</h1>
          <!-- Esta condición impide que un usuario pueda publicar más de una reseña en un taller -->
          <?php if ($rownc['COUNT(*)'] == 0 || isset($_GET['edit'])) {?>
            <form action="../../controller/C_Comentarios.php?id=<?php echo $_GET['id']; if (isset($_GET['reply'])) {?>&reply=<?php echo $_GET['reply']; }elseif (isset($_GET['edit'])) {?>&edit=<?php echo $_GET['edit']; } ?>" method="post">
              <!-- Formulario de rating -->
              <div class="rating" style="width: 20rem">
              <input id="rating-5" type="radio" name="rating" value="5"/><label for="rating-5"><i class="fas fa-2x fa-star"></i></label>
              <input id="rating-4" type="radio" name="rating" value="4"/><label for="rating-4"><i class="fas fa-2x fa-star"></i></label>
              <input id="rating-3" type="radio" name="rating" value="3"/><label for="rating-3"><i class="fas fa-2x fa-star"></i></label>
              <input id="rating-2" type="radio" name="rating" value="2"/><label for="rating-2"><i class="fas fa-2x fa-star"></i></label>
              <input id="rating-1" type="radio" name="rating" value="1"/><label for="rating-1"><i class="fas fa-2x fa-star"></i></label>
              </div>
              <p>
                <textarea name="commentary" rows="5" cols="80"><?php if (isset($_GET['edit'])) { echo $rowed['commentary']; } ?></textarea>
              </p>
              <p>
                <input type="submit" <?php if (isset($_GET['reply'])) { ?> name="postreply"  value="Responder" <?php }elseif (isset($_GET['edit'])) { ?> name="editcomment"  value="Editar" <?php }else{ ?>name="postcomment" value="Comentar"<?php } ?>>
              </p>
            </form>
          <?php } ?>
        <div id="coms" class="row col-12">
        <ul class="list-group">
          <!-- AQUÍ ESTÁ LA CAJA DE COMENTARIOS, METIDA EN UN FOREACH PARA QUE MUESTRE TODOS LOS DEL TALLER -->
          <?php
          foreach ($consultacw as $rowcw) {
          //Formateo de fechas, se pone dentro del foreach, para que no de problemas en caso de que no haya comentarios en un taller (en ese caso la consulta da null y se muestran errores)
          $NuevaFecha = strtotime ('+2 hour' , strtotime ($rowcw['create_date']));
          $comdate = date ( 'Y-m-d H:i:s' , $NuevaFecha);
          $NuevaFechaEd = strtotime ('+2 hour' , strtotime ($rowcw['update_date']));
          $edcomdate = date ( 'Y-m-d H:i:s' , $NuevaFechaEd);
          ?>
          <li id="<?php echo $rowcw['idcommentary']; ?>" class="list-group-item m-5 shadow-sm">
                <div class="">
                  <!-- Cuerpo del comentario -->
                  <div class="p-3">
                    <div class="row d-flex">
                      <div>
                        <img style="width: 80px;" src="<?php
                        //Condiciones para mostrar avatar
                        foreach ($consultauc as $rowuc) {
                          if ($rowuc['iduser'] == $rowcw['idusers']) {
                            if ($rowuc['image'] != '') {
                              echo '../'.$rowuc['image'];
                            }
                            if ($rowuc['image'] == '') {
                              echo '../assets/images/icons/user-icon.png';
                            }
                          }
                        }
                        ?>" alt="Avatar">
                        <?php foreach ($consultaratuser as $rowratuser) { if ($rowratuser['id_users'] == $rowcw['idusers']) { ?>
                        <div class="ml-2 mt-3 rating">
                          <i class="fas fa-2x fa-star" <?php if ($rowratuser['ranking'] >= 5) { echo 'style="color: #ffc107"'; } ?>></i>
                          <i class="fas fa-2x fa-star" <?php if ($rowratuser['ranking'] >= 4) { echo 'style="color: #ffc107"'; } ?>></i>
                          <i class="fas fa-2x fa-star" <?php if ($rowratuser['ranking'] >= 3) { echo 'style="color: #ffc107"'; } ?>></i>
                          <i class="fas fa-2x fa-star" <?php if ($rowratuser['ranking'] >= 2) { echo 'style="color: #ffc107"'; } ?>></i>
                          <i class="fas fa-2x fa-star" <?php if ($rowratuser['ranking'] = 1) { echo 'style="color: #ffc107"'; } ?>></i>
                        </div>
                        <?php } } ?>
                        <p><?php  foreach ($consultauc as $rowuc) { if ($rowuc['iduser'] == $rowcw['idusers']) { echo $rowuc['name']; } } ?></p>
                        <p>Reseña publicada con fecha <?php echo $comdate ?></p>
                        <?php if($edcomdate != '-0001-11-30 02:00:00') { echo '<p>Reseña editada con fecha '.$edcomdate.'</p>'; } ?>
                      </div>
                      <?php if (isset($_SESSION['idusuario']) && $_SESSION['idusuario'] == $rowcw['idusers']) { ?>
                      <div class="ml-auto m-3">
                        <a href="?id=<?php echo $_GET['id'] ?>&edit=<?php echo $rowcw['idcommentary']; ?>#comments">Editar reseña</a>
                        <form class="" action="../../controller/C_Comentarios.php?id=<?php echo $_GET['id']; ?>" method="post">
                          <input type="hidden" name="idcommentary" value="<?php echo $rowcw['idcommentary']; ?>">
                          <input type="submit" name="deletecomment" value="Eliminar reseña">
                        </form>
                      </div>
                    <?php } ?>
                    </div>
                    <div class="row m-2">
                      <p>
                      <?php echo $rowcw['commentary']; ?>
                      </p>
                    </div>
                  </div>
                  <div class="row d-flex mt-3">
                    <span class="m-auto">
                      <a href="?id=<?php echo $_GET['id']; ?>&reply=<?php echo $rowcw['idcommentary']; ?>#comments">Responder a este comentario</a>
                    </span>
                  </div>
                    <div class="p-3">
                      <!-- RESPUESTAS -->
                      <?php
                      $countrw=$comentario2->workshopid_reply($rowcw['idcommentary']);
                      $replycount=mysqli_num_rows($countrw);
                      if ($replycount != '0') {
                        $consultarw=$comentario2->workshopid_reply($rowcw['idcommentary']);
                        while ($rowrw=mysqli_fetch_array($consultarw)){
                          $ResFecha = strtotime ('+2 hour' , strtotime ($rowrw['create_date']));
                          $repdate = date ( 'Y-m-d H:i:s' , $ResFecha);
                          $EdResFecha = strtotime ('+2 hour' , strtotime ($rowrw['update_date']));
                          $edrepdate = date ( 'Y-m-d H:i:s' , $EdResFecha);
                      ?>
                      <ul class="ml-5 list-group">
                        <li id="<?php echo $rowrw['idcommentary']; ?>" class="list-group">
                          <div class="row d-flex">
                            <div>
                              <img style="width: 80px;" src="
                              <?php
                              //Condiciones para mostrar avatar, ya sea de usuario o taller
                              foreach ($consultauc as $rowuc) {
                                if ($rowuc['iduser'] == $rowrw['idusers']) {
                                  if ($rowuc['image'] != '') {
                                    echo '../'.$rowuc['image'];
                                  }
                                  if ($rowuc['image'] == '') {
                                    echo '../assets/images/icons/user-icon.png';
                                  }
                                }
                              }
                              foreach ($consultawc as $rowwc) {
                                if ($rowwc['name'] == $rowrw['workshopname']) {
                                  if ($rowwc['image'] != '') {
                                    echo '../'.$rowwc['image'];
                                  }
                                  if ($rowwc['image'] == '') {
                                    echo '../assets/images/background/workshop.jpg';
                                  }
                                }
                              }
                               ?>" alt="Avatar">
                              <p><?php  foreach ($consultauc as $rowuc) { if ($rowuc['iduser'] == $rowrw['idusers']) { echo $rowuc['name']; } } ?></p>
                              <p><?php  foreach ($consultawc as $rowwc) { if ($rowwc['name'] == $rowrw['workshopname']) { echo $rowwc['name']; } } ?></p>
                              <p>Comentario publicado con fecha <?php  echo $repdate ?></p>
                              <?php if($edrepdate != '-0001-11-30 02:00:00') { echo '<p>Comentario editado con fecha '.$edrepdate.'</p>'; } ?>
                            </div>
                            <?php if (isset($_SESSION['idusuario']) && $_SESSION['idusuario'] == $rowrw['idusers'] && $rowrw['workshopname'] == '' || isset($_SESSION['idtaller']) && $_SESSION['taller'] == $rowrw['workshopname']) { ?>
                            <div class="ml-auto m-3">
                              <a href="?id=<?php echo $_GET['id'] ?>&edit=<?php echo $rowrw['idcommentary'] ?>#comments">Editar respuesta</a>
                                <form class="" action="../../controller/C_Comentarios.php?id=<?php echo $_GET['id']; ?>" method="post">
                                  <input type="hidden" name="idcommentary" value="<?php echo $rowrw['idcommentary']; ?>">
                                  <input type="submit" name="deletecomment" value="Eliminar respuesta">
                                </form>
                            </div>
                          <?php } ?>
                          </div>
                          <div class="row m-2">
                            <p>
                            <?php echo $rowrw['commentary'] ?>
                            </p>
                          </div>
                        </li>
                      </ul>
                    <?php } } ?>
                    </div>
                </div>
          </li>
          <?php } ?>
            </ul>
            </div>
          </div>
      </div>
    </div>
  </div>
  
</div>
<!-- Latest compiled and minified JavaScript -->
    <script>
function initMap() {
    var address= document.getElementById('address');
  var marcadores = address;


  var map = new google.maps.Map(document.getElementById('map3'), {
    zoom: 18,
    center: new google.maps.LatLng( 40.0000000, -4.0000000),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  var infowindow = new google.maps.InfoWindow();
  var marker, i;
  for (i = 0; i < marcadores.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(marcadores[i][1], marcadores[i][2]),
      map: map
    });
    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent(marcadores[i][0]);
        infowindow.open(map, marker);
      }
    })(marker, i));
  }

var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
    this.setZoom(18);
    google.maps.event.removeListener(boundsListener);
    geocodeAddress(geocoder, map);
});
var geocoder = new google.maps.Geocoder();

}
function geocodeAddress(geocoder, resultsMap) {
    var address = document.getElementById('address').value;
    geocoder.geocode({'address': address}, function(results, status) {
      if (status === 'OK') {
        resultsMap.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
          map: resultsMap,
          position: results[0].geometry.location
        });
      } else {
        alert('Geocode  sin exito: ' + status);
      }
    });

  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACXVmPCf6pnuzVYfn4hm9KdVM4rS6_23U&callback=initMap" async defer></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>
</body>
</html>

<?php
}
 ?>
