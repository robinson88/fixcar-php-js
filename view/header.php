<!-- NAVBAR O MENU-->
<div class="background">
    <nav class="navbar navbar-expand-lg navbar-light bg-dark">
        <!-- Logo-->
        <a class="navbar-brand" href="#">
            <img class="logo" src="assets/images/logos/logo.png" alt="Fixcar logo">
        </a>
        <button class="navbar-toggler element" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <!-- Elemento Inicio -->
                    <a  class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <!-- Elemento Nosotros -->
                    <a class="nav-link" href="#about">Nosotros</a>
                </li>
                <li class="nav-item">
                    <!-- Elemento Descarga la App -->
                    <a class="nav-link" href="#download">App</a>
                </li>
                <li class="nav-item">
                    <!-- Elemento Contacto -->
                    <a class="nav-link" href="#meet">Contacto</a>
                </li>
                <li class="nav-item">
                    <!-- Elemento Registro -->
                    <a class="nav-link" href="http://localhost/FixCar/FRONTEND/views/registerOptions/register-view.php">Registro</a>
                </li>
                <li class="nav-item">
                    <!-- Elemento Login -->
                    <a class="nav-link" href="#">Login</a>
                </li>
            </ul>
        </div>
        <!-- Elemento de Búsqueda -->
        <form class="form-inline">
            <input class="form-control mr-sm-2" type="search" placeholder="Filtrar búsqueda" aria-label="Search">
            <button class="btn btn-car my-2 my-sm-0 filter" type="submit">
                <i class="fas fa-car-side"></i>
            </button>
        </form>
    </nav>
</div>