<?php
session_start();
if (!isset($_SESSION['usuario'])) {
?>
<script type="text/javascript">
    window.location.href="../../";
</script>
<?php
}else {
  require "../../controller/C_Datos.php";
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Proyecto de FixCar" />
    <meta name="keywords" content="taller, comparacion, distancia, precio, cambio de aceite, ruedas, piezas, reparacion, itv" />
    <meta name="Cesur" content="Equipo">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../assets/css/custom.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

    <title>FixCar</title>
</head>
<body>
<!-- NAVBAR O MENU-->
<div class="container-fluid">
  <nav class="row navbar navbar-expand-lg navbar-light bg-dark">
      <button class="navbar-toggler element" type="button" data-toggle="collapse" data-target="#navbarNav"
              aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Logo-->
      <a class="navbar-brand" href="../home-in/home-in.php">
        <img class="logo" src="../assets/images/logos/logo.png" alt="Fixcar logo">
      </a>

      <div class="row collapse navbar-collapse" id="navbarNav">
        <div class="col-7">
          <ul class="navbar-nav">
              <li class="nav-item active">
                  <!-- Elemento Inicio -->
                  <a  class="nav-link" href="../home-in/home-in.php"><?php echo $_SESSION['usuario']; ?> - Inicio<span class="sr-only"></span></a>
              </li>
              <li class="nav-item d-flex">
                  <!-- Elemento recordatorio -->
                  <a class="nav-link" href="../profiles/edit-reminders.php">Recordatorios
                      <?php $n = 0;
                      while ($rowh = mysqli_fetch_array($consultav)) {
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['wheels_date']) && $rowh['wheels_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['wheels_date'])) {
                              $n++;
                          }
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['oil_date']) && $rowh['oil_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['oil_date'])) {
                              $n++;
                          }
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['review_date']) && $rowh['review_date'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['review_date'])) {
                              $n++;
                          }
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['date_itv']) && $rowh['date_itv'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['date_itv'])) {
                              $n++;
                          }
                          if (strtotime($hoy . "+ 5 days") >= strtotime($rowh['vehicle_insurance']) && $rowh['vehicle_insurance'] != '0000-00-00' && strtotime($hoy) <= strtotime($rowh['vehicle_insurance'])) {
                              $n++;
                          }
                          ?>
                          <?php
                      }
                      if ($n != 0) {
                          ?>
                          <span class="reminder-badge navbar-reminder badge badge-danger badge-pill"> <?php echo $n; ?> </span>
                      <?php }
                      mysqli_data_seek($consultav, 0); ?>
                  </a>
              </li>
              <li class="nav-item">
                  <!-- Elemento Contacto -->
                  <a class="nav-link" href="../profiles/favworkshops.php">Talleres favoritos</a>
              </li>
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Perfiles </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="../profiles/profile-user.php">Mi perfil</a>
                      <a class="dropdown-item" href="../profiles/profile-vehicle.php">Mis vehículos</a>
                      <a class="dropdown-item" href="../profiles/edit-reminders.php">Mis recordatorios</a>
                      <a class="dropdown-item" href="../profiles/documents.php">Mis documentos</a>

                  </div>
              </li>
              <li class="nav-item">
                  <!-- Elemento Contacto -->
                  <a class="nav-link" href="../home-in/home-in.php#meet">Contacto</a>
              </li>
              <li class="nav-item">
                  <!-- Elemento log-out -->
                  <a class="nav-link" href="../../controller/logout.php"><i class="fas fa-power-off"></i></a>
              </li>
          </ul>
        </div>
          <div class="col-md-12 col-lg-5 d-flex">
            <form class="form-inline ml-auto p-2" action="../../controller/C_Talleres.php" method="post">
              <div class="row d-flex">
                <div class="">
                  <button class="btn btn-car form-control-button search-filter dropdown">
                    <!-- Elemento de búsqueda por filtros-->
                      <a class="nav-link dropdown-toggle filter-text" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Filtrar búsqueda </a>
                      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <div class="col-md-12">
                            <div class="form-check filter-list">
                                <input class="form-check-input" type="checkbox" name="mechanics" value="" id="oil-check">
                                <label class="form-check-label" for="oil-check">
                                    Mecánica
                                </label>
                            </div>
                            <div class="form-check filter-list">
                                <input class="form-check-input move-left" type="checkbox" name="electricity" value="" id="itv-check">
                                <label class="form-check-label" for="itv-check">
                                    Electricidad
                                </label>
                            </div>
                            <div class="form-check filter-list">
                                <input class="form-check-input move-left" type="checkbox" name="repair" value="" id="repair-check">
                                <label class="form-check-label" for="repair-check">
                                    Reparaciones
                                </label>
                            </div>
                            <div class="form-check filter-list">
                                <input class="form-check-input move-left" type="checkbox" name="overhaul" value="" id="overhaul-check">
                                <label class="form-check-label" for="overhaul-check">
                                    Revisión
                                </label>
                            </div>
                            <div class="form-check filter-list">
                                <input class="form-check-input move-left" type="checkbox" name="bodywork" value="" id="clean-check">
                                <label class="form-check-label" for="clean-check">
                                    Chapa y pintura
                                </label>
                            </div>
                            <div class="form-check filter-list">
                                <input class="form-check-input move-left" type="checkbox" name="creditcard" value="" id="credit-check">
                                <label class="form-check-label" for="credit-check">
                                    Pago con tarjeta
                                </label>
                            </div>
                            <div class="form-check filter-list">
                                <input class="form-check-input move-left" type="checkbox" name="premium" value="" id="credit-check">
                                <label class="form-check-label" for="credit-check">
                                    PREMIUM
                                </label>
                            </div>
                            <!-- Aquí acaba el chechbox de filtros de búsqueda -->
                        </div>
                      </div>
                  </button>
                </div>
                <div class="ajust-l">
                  <button class="btn btn-car btn-block my-2 my-sm-0 filter" style="width: 45px;" type="submit" name="filtersearch">
                      <i class="fas fa-car-side"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
      </div>
  </nav>

  <div class="row back-gradient">
      <div class="mx-auto col-md-10 col-lg-8 col-xl-6 mt-5">
          <div class="shadow-lg mb-5 bg-white rounded m-0">
              <div class="row">
                  <div class="col-md-10 mx-auto box">
                      <h2 class="text-uppercase font-weight-bold title-box">Filtra talleres a partir de tu calle</h2>
                      <h4 class="description-box"> Encuentra tu taller entre los cercanos a ti, descubre tu alrededor y
                          compara ofertas </h4>
                  </div>
              </div>
              <form class="row" action="../../controller/C_Talleres.php" method="post">
                  <div class="col-md-9 col-sm-10 ajust-r">
                      <div class="form-group">
                          <input type="text" name="search-in" class="form-control form-control-button" placeholder="Introduce tu calle o ciudad">
                      </div>
                  </div>
                  <div class="col-md-3 col-sm-2 ajust-l">
                      <button class="btn btn-car btn-block my-2 my-sm-0 filter" type="submit">
                          <i class="fas fa-car-side"></i>
                      </button>
                  </div>
              </form>
          </div>
      </div>
  </div>

  <div class="row background2">
    <div class="mx-auto col-md-10 col-lg-8 col-xl-6 mt-5 mb-5">
          <div class="shadow-lg bg-white contact-box rounded" id="meet">
              <h3 class="text-uppercase font-weight-bold title-contact">¿Necesitas ayuda o tienes alguna sugerencia? ¡Escríbenos!</h3>
              <form class="form-custom">
                  <div class="form-group">
                      <label for="staticEmail">Nuestra dirección:</label>
                      <input type="text" readonly class="form-control-plaintext" id="staticEmail"
                             value="fixcarproject@gmail.com">
                  </div>
                  <div class="form-group">
                      <textarea class="form-control" id="Textarea1" rows="3"></textarea>
                  </div>
                  <div class="row">
                    <div class="col-sm-8 col-md-6 mx-auto">
                      <button class="btn btn-lg btn-block btn-car" type="submit">
                          Enviar
                      </button>
                    </div>

                  </div>

              </form>
          </div>
    </div>
  </div>

  <div class="row background3">

  </div>
</div>




<!-- Latest compiled and minified JavaScript -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>
</body>
</html>
<?php
}
 ?>
