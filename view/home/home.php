<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Proyecto de FixCar" />
	<meta name="keywords" content="taller, comparacion, distancia, precio, cambio de aceite, ruedas, piezas, reparacion, itv" />
    <meta name="Cesur" content="Equipo">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="view/assets/css/custom.css">
	<link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

    <title>FixCar</title>
</head>
<body>
<!-- NAVBAR O MENU-->
<div class="container-fluid p-0">
  <nav class="row navbar navbar-expand-lg navbar-light bg-dark">

      <button class="navbar-toggler element" type="button" data-toggle="collapse" data-target="#navbarNav"
              aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Logo-->
      <a class="navbar-brand" href="">
        <img class="logo" src="view/assets/images/logos/logo.png" alt="Fixcar logo">
      </a>


      <div class="row collapse navbar-collapse" id="navbarNav">
        <div class="col-7">
          <ul class="navbar-nav">
              <li class="nav-item active">
                  <!-- Elemento Inicio -->
                  <a  class="nav-link" href="">Inicio <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                  <!-- Elemento Nosotros -->
                  <a class="nav-link" href="#about">Nosotros</a>
              </li>
              <li class="nav-item">
                  <!-- Elemento Descarga la App -->
                  <a class="nav-link" href="#download">App</a>
              </li>
              <li class="nav-item">
                  <!-- Elemento Contacto -->
                  <a class="nav-link" href="#meet">Contacto</a>
              </li>
                  <!-- Elemento Registro -->
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Registro </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="view/register-options/register-user.php">Registro de usuario</a>
                      <a class="dropdown-item" href="view/register-options/register-workshop.php">Registro de taller</a>
                  </div>
              </li>
              <li class="nav-item dropdown">
                  <!-- Elemento Login -->
                  <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Login </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="view/login/login-user.php">Soy un usuario</a>
                    <a class="dropdown-item" href="view/login/login-workshop.php">Soy un taller</a>
                  </div>
              </li>
          </ul>
        </div>
        <div class="col-md-12 col-lg-5 d-flex">
          <form class="form-inline ml-auto p-2" action="controller/C_Talleres.php" method="post">
            <div class="row d-flex">
              <div class="">
                <button class="btn btn-car form-control-button search-filter dropdown">
                  <!-- Elemento de búsqueda por filtros-->
                    <a class="nav-link dropdown-toggle filter-text" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Filtrar búsqueda </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <div class="col-md-12">
                          <div class="form-check filter-list">
                              <input class="form-check-input" type="checkbox" name="mechanics" value="" id="oil-check">
                              <label class="form-check-label" for="oil-check">
                                  Mecánica
                              </label>
                          </div>
                          <div class="form-check filter-list">
                              <input class="form-check-input move-left" type="checkbox" name="electricity" value="" id="itv-check">
                              <label class="form-check-label" for="itv-check">
                                  Electricidad
                              </label>
                          </div>
                          <div class="form-check filter-list">
                              <input class="form-check-input move-left" type="checkbox" name="repair" value="" id="repair-check">
                              <label class="form-check-label" for="repair-check">
                                  Reparaciones
                              </label>
                          </div>
                          <div class="form-check filter-list">
                              <input class="form-check-input move-left" type="checkbox" name="overhaul" value="" id="overhaul-check">
                              <label class="form-check-label" for="overhaul-check">
                                  Revisión
                              </label>
                          </div>
                          <div class="form-check filter-list">
                              <input class="form-check-input move-left" type="checkbox" name="bodywork" value="" id="clean-check">
                              <label class="form-check-label" for="clean-check">
                                  Chapa y pintura
                              </label>
                          </div>
                          <div class="form-check filter-list">
                              <input class="form-check-input move-left" type="checkbox" name="creditcard" value="" id="credit-check">
                              <label class="form-check-label" for="credit-check">
                                  Pago con tarjeta
                              </label>
                          </div>
                          <div class="form-check filter-list">
                              <input class="form-check-input move-left" type="checkbox" name="premium" value="" id="credit-check">
                              <label class="form-check-label" for="credit-check">
                                  PREMIUM
                              </label>
                          </div>
                          <!-- Aquí acaba el chechbox de filtros de búsqueda -->
                      </div>
                    </div>
                </button>
              </div>
              <div class="ajust-l">
                <button class="btn btn-car btn-block my-2 my-sm-0 filter" style="width: 45px;" type="submit" name="filtersearch">
                    <i class="fas fa-car-side"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- Elemento de Búsqueda -->

  </nav>


  <div class="row back-gradient">
      <div class="offset-md-1 col-md-10 offset-lg-2 col-lg-8 offset-xl-3 col-xl-6 mt-5">
          <div class="shadow mb-5 bg-white rounded">
              <div class="row">
                  <div class="col-md-10 col-sm-12 mx-auto box">
                      <h2 class="title-box text-uppercase font-weight-bold">Filtra talleres a partir de tu calle</h2>
                      <h4 class="description-box"> Encuentra tu taller entre los cercanos a ti, descubre tu alrededor y
                          compara ofertas </h4>
                  </div>
              </div>
              <form class="row" action="controller/C_Talleres.php" method="post">
                  <div class="col-md-9 col-xs-2 ajust-r">
                      <div class="form-group">
                        <input type="text" name="search" class="form-control form-control-button" placeholder="Introduce tu calle o ciudad">
                      </div>
                  </div>
                  <div class="col-md-3 col-sm-1 ajust-l">
                    <a href="view/search/search.php">
                      <button class="btn btn-car btn-block my-2 my-sm-0 filter" type="submit">
                          <i class="fas fa-car-side"></i>
                      </button>
                    </a>
                  </div>
                </form>
          </div>
      </div>
  </div>

  <div class="row background2">

      <div class="col-md-6 col-sm-12 p-5">
          <h2 class="about text-uppercase font-weight-bold " id="about">¡Conócenos!</h2>
          <p class="text-about">
              Somos una empresa familiar de informática en Alcalá de Henares, con una experiencia de más de 15 años en el sector, nuestra calidad nos avala.
          </p>
          <p class="text-about">
              Nuestro deseo es facilitarte el mantenimiento integral de tu vehículo para garantizar una mayor vida útil del mismo y que pueda circular con total seguridad.
          </p>
          <h2 class="about text-uppercase font-weight-bold ">¿Qué hacemos?</h2>
          <p class="text-about">
              Te ofrecemos un servicio con el cual podrás gestionar tus vehículos, llevar un control sobre ellos y su documentación,
              y también podrás establecer recordatorios para que te avisemos y no se te pase la fecha de ninguna revisión, y así puedas
              estar siempre tranquilo, ¡sin que debas preocuparte de nada!
          </p>
          <p class="text-about">
            Además tenemos un buscador de talleres donde podrás ver los talleres cercanos, y encontrar los mejor valorados por nuestros usuarios.
            Así podrás encontrar los mejores precios y ofertas con las que ahorrar en la gestión de tus vehículos.
          </p>
      </div>
      <div class="col-md-6 col-sm-12 text-center align-middle p-2">
          <img class="opacity-image image-fluid mx-auto align-middle" width="60%" src="view/assets/images/background/car-portal.png" alt="IMAGE">
      </div>
  </div>

  <div class="row shadow bg-white rounded download">
          <div class="row" id="download">
              <div class="row">
                  <div class="col-md-3 mx-auto" style="display: flex; align-items: center;">
                    <img width="75%" src="view/assets/images/icons/smartphone-icon.png" alt="smartphone-icon">
                  </div>
                  <div class="col-md-6">
                      <h2 class=" text-uppercase font-weight-bold title-download">
                          ¡Descarga nuestra App!
                      </h2>
                      <p class="text-download">
                        ¿Quieres encontrar los mejores talleres a tu alcance de la forma más sencilla posible? ¿te gustaría poder compararlos y recibir ofertas para ahorrar en tu vehículo? Si descargas nuestra app, disfrutarás de un montón de ventajas, como por ejemplo:
                      </p>
                      <br>
                          <p><span class="app-check"><i class="fas fa-check"></i></span> Los datos de tus vehículos siempre disponibles, aún sin conexión</p>
                          <p><span class="app-check"><i class="fas fa-check"></i></span> Recibe notificaciones sobre tus gestiones directamente en tu móvil</p>
                          <p><span class="app-check"><i class="fas fa-check"></i></span> Descarga tus documentos desde cualquier lugar</p>
                          <p><span class="app-check"><i class="fas fa-check"></i></span> Encuentra los talleres más cercanos, estés donde estés</p>
                          <div class="row col-md-12 m-3 justify-content-around">
                            <div class="col-8 col-sm-5 col-md-6 col-lg-5 col-xl-4 justify-content-center">
                              <img width="90%" class="m-3" src="view/assets/images/icons/googleplay.png" alt="Google Play">
                            </div>
                            <div class="col-8 col-sm-5 col-md-6 col-lg-5 col-xl-4 justify-content-center">
                              <img width="90%" class="m-3" src="view/assets/images/icons/appstore.png" alt="Apple Store">
                            </div>
                          </div>
                  </div>
              </div>
          </div>
  </div>

  <div class="row background3 align-items-center">
      <div class="col-lg-12 col-xl-6 mb-5 p-1">
          <div class="shadow bg-white rounded contact-box" id="meet">
              <h2 class="title-contact text-uppercase font-weight-bold ">¿Necesitas ayuda o tienes alguna sugerencia?</h2>
              <p class="text-about">
                Si tienes algún problema con nuestro servicio, ya sea con el buscador, tu perfil, o alguno de tus vehículos, o por otra parte quieres enviarnos
                alguna sugerencia de mejora de nuestro servicio, no dudes en ponerte en contacto con nosotros a través de nuestro email. Y como queremos facilitarte las cosas,
                ¡escríbenos desde aquí mismo!
              </p>
              <form class="form-custom row">
                  <div class="form-group col-md-12">
                      <input type="text" readonly class="form-control-plaintext border-none" id="staticEmail"
                             value="fixcarproject@gmail.com">
                  </div>
                  <div class="form-group col-md-12">
                      <label for="textarea">Tu sugerecia</label>
                      <textarea class="form-control border-muted " id="textarea" rows="8"></textarea>
                       <div class="col-md-11 mx-auto">
                            <button class="mx-auto mt-2 btn btn-block btn-car" type="submit">
                                Enviar
                            </button>
                        </div>
                  </div>
              </form>
          </div>
      </div>
      <div class="col-md-12 col-sm-12 col-lg-6 mx-auto">
        <div class="mail-icon col-md-12 col-sm-12 col-lg-8 mx-auto">
          <img width="100%" src="view/assets/images/icons/mail-icon.png" alt="mail-icon">
        </div>
      </div>
  </div>
</div>
<!-- Latest compiled and minified JavaScript -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>
</body>
</html>
