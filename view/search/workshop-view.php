<?php
require "../../controller/C_Datos.php";
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Proyecto de FixCar" />
    <meta name="keywords" content="taller, comparacion, distancia, precio, cambio de aceite, ruedas, piezas, reparacion, itv" />
    <meta name="Cesur" content="Equipo">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../assets/css/custom.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

    <title>FixCar</title>
</head>
<body>
<!-- NAVBAR O MENU-->
<div class="container-fluid">
  <nav class="row navbar navbar-expand-lg navbar-light bg-dark">

      <button class="navbar-toggler element" type="button" data-toggle="collapse" data-target="#navbarNav"
              aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Logo-->
      <a class="navbar-brand" href="../home-in/home-in.php">
        <img class="logo" src="../assets/images/logos/logo.png" alt="Fixcar logo">
      </a>


      <div class="row collapse navbar-collapse" id="navbarNav">
        <div class="col-7">
          <ul class="navbar-nav">
              <li class="nav-item">
                  <!-- Elemento Inicio -->
                  <a  class="nav-link" href="../home-in/home-in.php">Inicio <span class="sr-only"></span></a>
              </li>
              <li class="nav-item">
                  <!-- Elemento Nosotros -->
                  <a class="nav-link" href="../home-in/home-in.php#about">Nosotros</a>
              </li>
              <li class="nav-item">
                  <!-- Elemento Descarga la App -->
                  <a class="nav-link" href="../home-in/home-in.php#download">App</a>
              </li>
              <li class="nav-item">
                  <!-- Elemento Contacto -->
                  <a class="nav-link" href="../home-in/home-in.php#meet">Contacto</a>
              </li>
                  <!-- Elemento Registro -->
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Registro </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="view/register-options/register-user.php">Registro de usuario</a>
                      <a class="dropdown-item" href="view/register-options/register-workshop.php">Registro de taller</a>
                  </div>
              </li>
              <li class="nav-item dropdown">
                  <!-- Elemento Login -->
                  <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Login </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="view/login/login-user.php">Soy un usuario</a>
                    <a class="dropdown-item" href="view/login/login-workshop.php">Soy un taller</a>
                  </div>
              </li>
          </ul>
        </div>
        <div class="col-md-12 col-lg-5 d-flex">
            <form class="form-inline ml-auto p-2">
              <div class="row d-flex">
                <div class="">
                  <button class="btn btn-car form-control-button search-filter dropdown">
                      <!-- Elemento Login -->
                      <a class="nav-link dropdown-toggle filter-text" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Filtrar búsqueda
                      </a>
                      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <div class="col-md-12">
                            <div class="form-check filter-list">
                                <input class="form-check-input" type="checkbox" name="oil" value="" id="oil-check">
                                <label class="form-check-label" for="oil-check">
                                    Mecánica
                                </label>
                            </div>
                            <div class="form-check filter-list">
                                <input class="form-check-input move-left" type="checkbox" name="itv" value="" id="itv-check">
                                <label class="form-check-label" for="itv-check">
                                    Electricidad
                                </label>
                            </div>
                            <div class="form-check filter-list">
                                <input class="form-check-input move-left" type="checkbox" name="repair" value="" id="repair-check">
                                <label class="form-check-label" for="repair-check">
                                    Reparaciones
                                </label>
                            </div>
                            <div class="form-check filter-list">
                                <input class="form-check-input move-left" type="checkbox" name="overhaul" value="" id="overhaul-check">
                                <label class="form-check-label" for="overhaul-check">
                                    Revisión
                                </label>
                            </div>
                            <div class="form-check filter-list">
                                <input class="form-check-input move-left" type="checkbox" name="clean" value="" id="clean-check">
                                <label class="form-check-label" for="clean-check">
                                    Chapa y pintura
                                </label>
                            </div>
                            <div class="form-check filter-list">
                                <input class="form-check-input move-left" type="checkbox" name="credit" value="" id="credit-check">
                                <label class="form-check-label" for="credit-check">
                                    Pago con tarjeta
                                </label>
                            </div>
                            <div class="form-check filter-list">
                                <input class="form-check-input move-left" type="checkbox" name="credit" value="" id="premium">
                                <label class="form-check-label" for="premium">
                                    PREMIUM
                                </label>
                            </div>
                            <!-- Aquí acaba el chechbox de filtros de búsqueda -->
                        </div>
                      </div>
                  </button>
                </div>
                <div class="ajust-l">
                  <a class="btn btn-car filter" href="./inline.php">
                    <i class="fas fa-car-side"></i>
                  </a>
                </div>
              </div>
            </form>
        </div>
      </div>
      <!-- Elemento de Búsqueda -->

  </nav>
  <div class="row">
      <div class="col-md-8 mx-auto shadow-lg p-3 mt-5 bg-white rounded">
        <div class="row">
          <img class="workshop-cover" width="100%" height="300px" src="../<?php echo $rowdt['image']; ?>" alt="portada">
        </div>
        <div class="row d-flex">
            <div class="col-xl-6 mt-3">
              <div class="d-flex">
                <div>
                  <h1 class="title-box mt-3"><?php echo $rowdt['name']; ?>
                      <?php if ($rowdt['state'] == 'Premium') {?>
                          <i class="far fa-heart heart-color"></i>
                      <?php } else {?>
                          <i class="fas fa-heart"></i>
                      <?php }?>
                      <!--FIN FAVORITOS -->
                      <?php if ($rowdt['state'] == 'Premium') {?>
                          <i class="fas fa-crown premium-sign pl-2"></i>
                      <?php } ?>
                  </h1>
                </div>
              </div>
              <div class="row d-flex">
                <?php if ($rowtt['mechanics'] == '1') { ?>
                  <div class="m-1 ">
                      <span class="app-check"><i class="fas fa-check"></i></span> Mecánica
                  </div>
                <?php } ?>
                <?php if ($rowtt['repairs'] == '1') { ?>
                  <div class="m-1 ">
                      <span class="app-check"><i class="fas fa-check"></i></span> Reparaciones
                  </div>
                <?php } ?>
                <?php if ($rowtt['review'] == '1') { ?>
                  <div class="m-1 ">
                      <span class="app-check"><i class="fas fa-check"></i></span> Revisión
                  </div>
                <?php } ?>
                <?php if ($rowtt['bodywork'] == '1') { ?>
                  <div class="m-1 ">
                      <span class="app-check"><i class="fas fa-check"></i></span> Chapa y pintura
                  </div>
                <?php } ?>
                <?php if ($rowtt['electricity'] == '1') { ?>
                  <div class="m-1 ">
                      <span class="app-check"><i class="fas fa-check"></i></span> Electricidad
                  </div>
                <?php } ?>
                <?php if ($rowtt['creditcard'] == '1') { ?>
                  <div class="m-1 ">
                      <span class="app-check"><i class="fas fa-check"></i></span> Pago con Tarjeta
                  </div>
                <?php } ?>
              </div>
              <div class="row mt-3 mx-auto">
                <div class="col-md-10">
                 <div class="address">
                   <input id="address" 2z2class="address" type="text" size="50" value="<?php if ($rowdt['adress'] == '') { ?>Dirección del taller <?php }else{ echo $rowdt['adress']; } ?>, <?php if ($rowdt['location'] == '') { ?> Ciudad <?php }else{ echo $rowdt['location']; } ?>">
                 </div>
                </div>
                <div class="col-md-10">
                  <?php if ($rowdt['phone'] == '') { ?>Teléfono de tu taller <?php }else{ echo $rowdt['phone']; } ?>
                </div>
                <div class="col-md-10">
                  <?php echo $rowdt['email']; ?>
                </div>
                <div class="col-md-10 mt-4 mb-4">
                  <?php echo $rowdt['description']; ?>
                </div>
            </div>
          </div>
          <div class="col-xl-6 col-md-12 mt-3" style="align-items: center;display: flex;">
              <?php if ($rowdt['state'] == 'Premium') { ?>
              <!-- 4:3 aspect ratio -->
              <div class="embed-responsive embed-responsive-4by3" width="100%">
                  <iframe width="560" height="315" src="<?php echo 'https://www.youtube.com/embed/'.$rowdt['video']; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
              <?php } ?>
          </div>
          <?php if ($rowdt['state'] == 'Premium') { ?>
              <div class="col-md-12 col-xl-12 col-lg-12 mx-auto mapa mt-3" width="100%">
                  <div class="container" id="map3">
                      <!--<a href=""><img src="../assets/images/workshop-images/map-example.jpg" alt=""></a>-->
                  </div>
              </div>
          <?php } ?>
        </div>
      </div>
  </div>
  <div class="row">
    <div class="col-md-8 mx-auto shadow-lg p-3 mt-5 bg-white">
      <div class="row p-3" id="comments">
          <h1 class="title-box ml-5 mt-3">Reseñas</h1>
          <?php foreach ($consultacw as $rowcw) : ?>
              <?php
              $NuevaFecha = strtotime('+2 hour', strtotime($rowcw['create_date']));
              $comdate = date('Y-m-d H:i:s', $NuevaFecha);
              $NuevaFechaEd = strtotime('+2 hour', strtotime($rowcw['update_date']));
              $edcomdate = date('Y-m-d H:i:s', $NuevaFechaEd);
              ?>
              <div class="col-12">
                  <div class="row">
                      <div class="col-12 card card-white post p-1 mb-3">
                          <div class="post-heading">
                              <div class="float-left image">
                                  <img src="<?php
                                  //Condiciones para mostrar avatar
                                  foreach ($consultauc as $rowuc) {
                                      if ($rowuc['iduser'] == $rowcw['idusers']) {
                                          if ($rowuc['image'] != '') {
                                              echo '../' . $rowuc['image'];
                                          }
                                          if ($rowuc['image'] == '') {
                                              echo '../assets/images/icons/user-icon.png';
                                          }
                                      }
                                  }
                                  ?>" class="img-circle avatar" alt="Avatar">
                              </div>
                              <div class="float-left meta">
                                  <div class="title h5">
                                      <a href="#"><b>
                                              <?php
                                              foreach ($consultauc as $rowuc) {
                                                  if ($rowuc['iduser'] == $rowcw['idusers']) {
                                                      echo $rowuc['name'];
                                                  }
                                                  if ($rowwc['idworkshop'] == $rowcw['idusers'] && $rowcw['workshopname'] != '') {
                                                      echo $rowwc['name'];
                                                  }
                                              } ?>
                                          </b></a>
                                  </div>
                                  <h6 class="text-muted time">Publicado: <?php echo $comdate ?></h6>
                              </div>
                          </div>
                          <div class="post-description">
                              <p> <?php echo $rowcw['commentary']; ?></p>
                          </div>

                          <!-- respuesta -->

                          <?php
                          $countrw = $comentario2->workshopid_reply($rowcw['idcommentary']);
                          $replycount = mysqli_num_rows($countrw);
                          if ($replycount != '0') : ?>
                              <?php $consultarw = $comentario2->workshopid_reply($rowcw['idcommentary']);
                              while ($rowrw = mysqli_fetch_array($consultarw)) : ?>
                                  <?php $ResFecha = strtotime('+2 hour', strtotime($rowrw['create_date']));
                                  $repdate = date('Y-m-d H:i:s', $ResFecha);
                                  $EdResFecha = strtotime('+2 hour', strtotime($rowrw['update_date']));
                                  $edrepdate = date('Y-m-d H:i:s', $EdResFecha);
                                  ?>
                                  <div class="row" style="padding: 30px;">
                                      <div class="col-12 card card-white post p-1 mb-3"
                                           style="background: #E3E3E3;">
                                          <div class="post-heading">
                                              <div class="float-left image">
                                                  <?php
                                                  //Condiciones para mostrar avatar, ya sea de usuario o taller
                                                  foreach ($consultauc as $rowuc) {
                                                      if ($rowuc['iduser'] == $rowrw['idusers']) {
                                                          if ($rowuc['image'] != '') {
                                                              $imageSrc = '../' . $rowuc['image'];
                                                          }
                                                          if ($rowuc['image'] == '') {
                                                              $imageSrc = '../assets/images/icons/user-icon.png';
                                                          }
                                                      }
                                                  }
                                                  foreach ($consultawc as $rowwc) {
                                                      if ($rowwc['name'] == $rowrw['workshopname']) {
                                                          if ($rowwc['image'] != '') {
                                                              $imageSrc = '../' . $rowwc['image'];
                                                          }
                                                          if ($rowwc['image'] == '') {
                                                              $imageSrc = '../assets/images/background/workshop.jpg';
                                                          }
                                                      }
                                                  }
                                                  ?>
                                                  <img src="<?= $imageSrc ?>"
                                                       class="img-circle avatar" alt="Avatar">
                                              </div>
                                              <div class="float-left meta">
                                                  <div class="title h5">
                                                      <a href="#"><b>
                                                              <?php foreach ($consultauc as $rowuc) {
                                                                  if ($rowuc['iduser'] == $rowrw['idusers']) {
                                                                      echo $rowuc['name'];
                                                                  }
                                                              } ?>
                                                              <?php if ($rowrw['workshopname'] != '') {
                                                                  echo 'Comentario del taller';
                                                              } ?>
                                                              <?php foreach ($consultawc as $rowwc) {
                                                                  if ($rowwc['name'] == $rowrw['workshopname']) {
                                                                      echo $rowwc['name'];
                                                                  }
                                                              } ?>
                                                          </b></a>
                                                  </div>
                                                  <h6 class="text-muted time">
                                                      Publicado: <?php echo $comdate ?></h6>
                                              </div>
                                          </div>
                                          <div class="post-description">
                                              <p> <?php echo $rowrw['commentary'] ?></p>
                                          </div>
                                      </div>
                                  </div>
                              <?php endwhile; ?>
                          <?php endif; ?>
                      </div>
                  </div>
              </div>
          <?php endforeach; ?>
          </div>
      </div>
  </div>
</div>

<!-- Latest compiled and minified JavaScript -->
<script>
function initMap() {
    var address= document.getElementById('address');
  var marcadores = address;


  var map = new google.maps.Map(document.getElementById('map3'), {
    zoom: 18,
    center: new google.maps.LatLng( 40.0000000, -4.0000000),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  var infowindow = new google.maps.InfoWindow();
  var marker, i;
  for (i = 0; i < marcadores.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(marcadores[i][1], marcadores[i][2]),
      map: map
    });
    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent(marcadores[i][0]);
        infowindow.open(map, marker);
      }
    })(marker, i));
  }

var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
    this.setZoom(18);
    google.maps.event.removeListener(boundsListener);
    geocodeAddress(geocoder, map);
});
var geocoder = new google.maps.Geocoder();

}
function geocodeAddress(geocoder, resultsMap) {
    var address = document.getElementById('address').value;
    geocoder.geocode({'address': address}, function(results, status) {
      if (status === 'OK') {
        resultsMap.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
          map: resultsMap,
          position: results[0].geometry.location
        });
      } else {
        alert('Geocode  sin exito: ' + status);
      }
    });

  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACXVmPCf6pnuzVYfn4hm9KdVM4rS6_23U&callback=initMap" async defer></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>
</body>
</html>
