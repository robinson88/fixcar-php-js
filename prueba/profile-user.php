<?php
session_start();
if (!isset($_SESSION['usuario'])) {
?>
<script type="text/javascript">
    window.location.href="../../";
</script>
<?php
}else {
require "../../controller/Datos_Usuarios.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Proyecto de FixCar" />
    <meta name="keywords" content="taller, comparacion, distancia, precio, cambio de aceite, ruedas, piezas, reparacion, itv" />
    <meta name="Cesur" content="Equipo">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../assets/css/custom.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

    <title>FixCar</title>
</head>
<body>
<!-- NAVBAR O MENU-->
<div class="container-fluid">

  <nav class="row navbar navbar-expand-lg navbar-light bg-dark">
      <!-- Logo-->
      <a class="navbar-brand" href="../home-in/home-in.php">
        <img class="logo" src="../assets/images/logos/logo.png" alt="Fixcar logo">
      </a>
      <button class="navbar-toggler element" type="button" data-toggle="collapse" data-target="#navbarNav"
              aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
              <li class="nav-item">
                  <!-- Elemento Inicio -->
                  <a  class="nav-link" href="../home-in/home-in.php"><?php echo $_SESSION['usuario']; ?> - Inicio<span class="sr-only"></span></a>
              </li>
              <li class="nav-item">
                  <!-- Elemento vehiculos -->
                  <a class="nav-link" href="../profiles/profile-vehicle.php">Mis vehículos</a>
              </li>
              <li class="nav-item">
                  <!-- Elemento recordatorio -->
                  <a class="nav-link" href="../home-in/home-in.php#memo">Recordatorios</a>
              </li>
              <li class="nav-item">
                  <!-- Elemento recordatorio -->
                  <a class="nav-link" href="../profilew/user-documents.php">Documentos</a>
              </li>
              <li class="nav-item">
                  <!-- Elemento Contacto -->
                  <a class="nav-link" href="../home-in/home-in.php#meet">Contacto</a>
              </li>
              <li class="nav-item dropdown active">
                <!-- Elemento Configuración -->
                  <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Configuración </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="../profiles/profile-user.php">Configurar perfil</a>
                      <a class="dropdown-item" href="../profiles/edit-reminders.php">Configurar recordatorios</a>
                  </div>
              </li>
              <li class="nav-item">
                  <!-- Elemento log-out -->
                  <a class="nav-link" href="../../controller/logout.php"><i class="fas fa-power-off"></i></a>
              </li>
          </ul>
      </div>
      <!-- Elemento de Búsqueda -->
      <form class="form-inline">
          <input class="form-control form-control-button" type="search" placeholder="Filtrar búsqueda" aria-label="Search">
          <button class="btn btn-car my-2 my-sm-0 filter" type="submit">
              <i class="fas fa-car-side"></i>
          </button>
      </form>
  </nav>
    <div class="row">
        <div class="col-md-9 mx-auto">
            <!-- user-profile -->
            <div class="">
                <div id="user" class=""><br>
                    <h3 class="register-heading">Edición de mi perfil</h3>
                    <form class="row mt-5 register-form" action="../../controller/C_Usuarios.php" method="post">
                        <div class="col-md-12">
                            <div class="form-group ">
                                <input type="image" class="circle row mx-auto" name="user-image" src="../assets/images/icons/user-icon.png" alt="profile">
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="nombre" placeholder="<?php if ($consulta1['nombre'] == '') { ?>Nombre <?php }else{ echo $consulta1['nombre']; } ?>" />
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4">
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="<?php if ($consulta1['email'] == '') { ?>Email <?php }else{ echo $consulta1['email']; } ?>" />
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="direccion" placeholder="<?php if ($consulta1['direccion'] == '') { ?>Dirección <?php }else{ echo $consulta1['direccion']; } ?>"/>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="localidad" placeholder="<?php if ($consulta1['localidad'] == '') { ?>Localidad <?php }else{ echo $consulta1['localidad']; } ?>"/>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="telefono" placeholder="<?php if ($consulta1['telefono'] == '') { ?>Teléfono <?php }else{ echo $consulta1['telefono']; } ?>"/>
                            </div>
                        </div>
                        <div class="col-md-6  col-xl-4 mb-5">
                            <div class="form-group">
                                <input type="text" class="form-control" name="fecha" placeholder="<?php if ($consulta1['fecha'] == '') { ?>Fecha de nacimiento <?php }else{ echo $consulta1['fecha']; } ?>"/>
                            </div>
                        </div>
                        <div class="col-md-5 mx-auto mt-5">
                            <button type="submit" class="btn btn-dark btn-lg btn-block" name="save">Guardar cambios</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Latest compiled and minified JavaScript -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>
</body>
</html>
<?php
}
 ?>
